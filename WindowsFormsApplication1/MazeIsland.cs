﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2Randomizer
{
    class MazeIsland : World
    {
        private readonly SortedDictionary<int, terrain> terrains = new SortedDictionary<int, terrain>
        {
            { 0xA131, terrain.road },
                { 0xA132, terrain.road },
                { 0xA133, terrain.road },
                { 0xA134, terrain.bridge },
                { 0xA140, terrain.palace },
                { 0xA143, terrain.road },
                { 0xA145, terrain.road },
                { 0xA146, terrain.road },
                { 0xA147, terrain.road },
                { 0xA148, terrain.road },
                { 0xA149, terrain.road }
        };

        public Location kid;
        public Location magic;
        public Location palace4;
        public Location start;

        public MazeIsland(Hyrule hy)
            : base(hy)
        {
            loadLocations(0xA131, 4, terrains);
            loadLocations(0xA140, 1, terrains);
            loadLocations(0xA143, 1, terrains);
            loadLocations(0xA145, 5, terrains);

            enemyAddr = 0x88B0;
            enemies = new List<int> { 03, 04, 05, 0x11, 0x12, 0x14, 0x16, 0x18, 0x19, 0x1A, 0x1B, 0x1C };
            flyingEnemies = new List<int> { 0x06, 0x07, 0x0A, 0x0D, 0x0E, 0x15 };
            generators = new List<int> { 0x0B, 0x0F, 0x17 };
            shorties = new List<int> { 0x03, 0x04, 0x05, 0x11, 0x12, 0x16 };
            tallGuys = new List<int> { 0x14, 0x18, 0x19, 0x1A, 0x1B, 0x1C };
            enemyPtr = 0xA08E;
            overworldMaps = new List<int>();

            kid = getLocationByMem(0xA143);
            magic = getLocationByMem(0xA133);
            palace4 = getLocationByMem(0xA140);
            palace4.PalNum = 4;
            start = getLocationByMem(0xA134);
            MAP_ROWS = 20;
            MAP_COLS = 64;
    }


        public void setStart()
        {
            v[start.Ypos - 30, start.Xpos] = true;
        }

        public void terraform()
        {
            bcount = 900;
            while (bcount > 801)
            {
                map = new terrain[MAP_ROWS, MAP_COLS];
                bool[,] visited = new bool[MAP_ROWS, 21];
                for (int i = 0; i < MAP_ROWS; i += 2)
                {
                    for (int j = 0; j < 21; j++)
                    {
                        map[i, j] = terrain.mountain;
                    }
                }

                for(int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 21; j < MAP_COLS; j++)
                    {
                        map[i, j] = terrain.water;
                    }
                }

                for (int j = 0; j < 21; j += 2)
                {
                    for (int i = 0; i < MAP_ROWS; i++)
                    {
                        map[i, j] = terrain.mountain;
                    }
                }

                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < 21; j++)
                    {
                        if (map[i, j] != terrain.mountain && map[i, j] != terrain.water)
                        {
                            map[i, j] = terrain.road;
                            visited[i, j] = false;
                        }
                        else
                        {
                            visited[i, j] = true;
                        }
                    }
                }
                //choose starting position
                int starty = hy.R.Next(MAP_ROWS);
                if (starty == 0)
                {
                    starty++;
                }
                else if (starty % 2 == 0)
                {
                    starty--;
                }

                map[starty, 0] = terrain.bridge;
                start.Xpos = 0;
                start.Ypos = starty + 30;
                start.PassThrough = 0;

                

                //generate maze
                int currx = 1;
                int curry = starty;
                Stack<Tuple<int, int>> s = new Stack<Tuple<int, int>>();
                while (moreToVisit(visited))
                {
                    List<Tuple<int, int>> n = getListOfNeighbors(currx, curry, visited);
                    if (n.Count > 0)
                    {
                        Tuple<int, int> next = n[hy.R.Next(n.Count)];
                        s.Push(next);
                        if (next.Item1 > currx)
                        {
                            map[curry, currx + 1] = terrain.road;
                        }
                        else if (next.Item1 < currx)
                        {
                            map[curry, currx - 1] = terrain.road;
                        }
                        else if (next.Item2 > curry)
                        {
                            map[curry + 1, currx] = terrain.road;
                        }
                        else
                        {
                            map[curry - 1, currx] = terrain.road;
                        }
                        currx = next.Item1;
                        curry = next.Item2;
                        visited[curry, currx] = true;
                    }
                    else if (s.Count > 0)
                    {
                        Tuple<int, int> n2 = s.Pop();
                        currx = n2.Item1;
                        curry = n2.Item2;
                    }
                }

                //place palace 4


                    int p4x = hy.R.Next(15) + 3;
                    int p4y = hy.R.Next(MAP_ROWS - 6) + 3;
                
                palace4.Xpos = p4x;
                palace4.Ypos = p4y + 30;
                map[p4y, p4x] = terrain.palace;
                map[p4y + 1, p4x] = terrain.road;
                map[p4y - 1, p4x] = terrain.road;
                map[p4y, p4x + 1] = terrain.road;
                map[p4y, p4x - 1] = terrain.road;

                //draw a river
                int riverstart = starty;
                while (riverstart == starty)
                {
                    riverstart = hy.R.Next(10) * 2;
                }

                int riverend = hy.R.Next(10) * 2;

                Location rs = new Location();
                rs.Xpos = 0;
                rs.Ypos = riverstart + 30;

                Location re = new Location();
                re.Xpos = 20;
                re.Ypos = riverend + 30;

                drawLine(rs, re, terrain.walkablewater);

                foreach (Location l in AllLocations)
                {
                    if (l.TerrainType == terrain.road)
                    {
                        int x = 0;
                        int y = 0;
                        if (l != magic && l != kid)
                        {
                            do
                            {
                                x = hy.R.Next(19) + 2;
                                y = hy.R.Next(MAP_ROWS - 4) + 2;
                            } while (map[y, x] != terrain.road || !((map[y, x + 1] == terrain.mountain && map[y, x - 1] == terrain.mountain) || (map[y+1, x] == terrain.mountain && map[y - 1,x] == terrain.mountain)) || getLocationByCoords(new Tuple<int, int>(y + 30, x + 1)) != null || getLocationByCoords(new Tuple<int, int>(y + 30, x - 1)) != null || getLocationByCoords(new Tuple<int, int>(y + 31, x)) != null || getLocationByCoords(new Tuple<int, int>(y + 29, x)) != null || getLocationByCoords(new Tuple<int, int>(y + 30, x)) != null);
                        }
                        else
                        {
                            do
                            {
                                x = hy.R.Next(19) + 2;
                                y = hy.R.Next(MAP_ROWS - 4) + 2;
                            } while (map[y, x] != terrain.road || getLocationByCoords(new Tuple<int, int>(y + 30, x + 1)) != null || getLocationByCoords(new Tuple<int, int>(y + 30, x - 1)) != null || getLocationByCoords(new Tuple<int, int>(y + 31, x)) != null || getLocationByCoords(new Tuple<int, int>(y + 29, x)) != null || getLocationByCoords(new Tuple<int, int>(y + 30, x)) != null);
                        }

                        l.Xpos = x;
                        l.Ypos = y + 30;
                    }
                }

                //check bytes and adjust
                 writeBytes(false, 0xA65C, 801, 0, 0);
            }
            writeBytes(true, 0xA65C, 801, 0, 0);

            int loc3 = 0x7C00 + bcount;
            int high = (loc3 & 0xFF00) >> 8;
            int low = loc3 & 0xFF;


            hy.ROMData.put(0x87A5, (Byte)low);
            hy.ROMData.put(0x87A6, (Byte)high);

            for (int i = 0xA10C; i < 0xA149; i++)
            {
                if(!terrains.Keys.Contains(i))
                {
                    hy.ROMData.put(i, 0x00);
                }
            }

            v = new bool[MAP_ROWS, MAP_COLS];
            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    v[i, j] = false;
                }
            }

        }

        private bool moreToVisit(bool[,] v)
        {
            for (int i = 0; i < v.GetLength(0); i++)
            {
                for (int j = 0; j < v.GetLength(1); j++)
                {
                    if (v[i, j] == false)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private List<Tuple<int, int>> getListOfNeighbors(int currx, int curry, bool[,] v)
        {
            List<Tuple<int, int>> x = new List<Tuple<int, int>>();

            if (currx - 2 > 0 && v[curry, currx - 2] == false)
            {
                x.Add(new Tuple<int, int>(currx - 2, curry));
            }

            if (currx + 2 < 21 && v[curry, currx + 2] == false)
            {
                x.Add(new Tuple<int, int>(currx + 2, curry));
            }

            if (curry - 2 > 0 && v[curry - 2, currx] == false)
            {
                x.Add(new Tuple<int, int>(currx, curry - 2));
            }

            if (curry + 2 < MAP_ROWS && v[curry + 2, currx] == false)
            {
                x.Add(new Tuple<int, int>(currx, curry + 2));
            }
            return x;
        }

        private void drawLine(Location to, Location from, terrain t)
        {
            int x = from.Xpos;
            int y = from.Ypos - 30;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = (hy.R.Next(Math.Abs(diff / 2)) + 1) * 2;

     
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            if ((x != to.Xpos || y != (to.Ypos - 30)) && getLocationByCoords(new Tuple<int, int>(y + 30, x)) == null)
                            {
                                if(map[y, x] == terrain.mountain)
                                {
                                    map[y, x] = t;
                                }
                                else if (map[y, x] == terrain.road && (diff > 0 && (map[y, x + 1] == terrain.mountain)) || (diff < 0 && map[y, x - 1] == terrain.mountain))
                                {
                                    map[y, x] = terrain.bridge;
                                }
                                else if (map[y, x] != terrain.palace && (x != start.Xpos || y != start.Ypos - 30))
                                {
                                    map[y, x] = t;
                                }

                            }
                            if (diff > 0 && x < MAP_COLS - 1)
                            {
                                x++;
                            }
                            else if (x > 0)
                            {
                                x--;
                            }

                            move--;
                        }
                    }
                }
                else if(y != to.Ypos - 30)
                {
                    int diff = to.Ypos - 30 - y;
                    int move = (hy.R.Next(Math.Abs(diff / 2)) + 1) * 2;
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            if ((x != to.Xpos || y != (to.Ypos - 30)) && getLocationByCoords(new Tuple<int, int>(y + 30, x)) == null)
                            {
                                if (map[y, x] == terrain.mountain)
                                {
                                    map[y, x] = t;
                                }
                                else if(map[y, x] == terrain.road && (diff > 0 && (map[y + 1, x] == terrain.mountain)) || (diff < 0 && map[y - 1, x] == terrain.mountain))
                                {
                                    map[y, x] = terrain.bridge;
                                }
                                else if (map[y, x] != terrain.palace && (x != start.Xpos || y != start.Ypos - 30))
                                {
                                    map[y, x] = t;
                                }
                            }
                            if (diff > 0 && y < MAP_ROWS - 1)
                            {
                                y++;
                            }
                            else if (y > 0)
                            {
                                y--;
                            }
                            move--;
                        }
                    }
                }
            }
        }
        public void updateVisit()
        {
            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < MAP_COLS; j++)
                    {
                        if (!v[i,j] && ((map[i, j] == terrain.walkablewater && hy.itemGet[(int)items.boots]) || map[i,j] == terrain.road || map[i,j] == terrain.palace || map[i, j] == terrain.bridge))
                        {
                            if (i - 1 >= 0)
                            {
                                if (v[i - 1, j])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }

                            }

                            if (i + 1 < MAP_ROWS)
                            {
                                if (v[i + 1, j])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }

                            if (j - 1 >= 0)
                            {
                                if (v[i, j - 1])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }

                            if (j + 1 < MAP_COLS)
                            {
                                if (v[i, j + 1])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }
                        }
                    }
                }
            }

            foreach (Location l in AllLocations)
            {
                if (v[l.Ypos - 30, l.Xpos])
                {
                    l.Reachable = true;
                }
            }
        }
    }
}
