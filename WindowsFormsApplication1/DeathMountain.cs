﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2Randomizer
{
    class DeathMountain : World
    {
        private readonly new terrain[] walkable = { terrain.desert, terrain.forest, terrain.grave };
        private readonly new terrain[] randomTerrains = { terrain.desert, terrain.forest, terrain.grave, terrain.mountain, terrain.walkablewater };

        private readonly SortedDictionary<int, terrain> terrains = new SortedDictionary<int, terrain>
            {
                { 0x610C, terrain.cave },
                { 0x610D, terrain.cave },
                { 0x610E, terrain.cave },
                { 0x610F, terrain.cave },
                { 0x6110, terrain.cave },
                { 0x6111, terrain.cave },
                { 0x6112, terrain.cave },
                { 0x6113, terrain.cave },
                { 0x6114, terrain.cave },
                { 0x6115, terrain.cave },
                { 0x6116, terrain.cave },
                { 0x6117, terrain.cave },
                { 0x6118, terrain.cave },
                { 0x6119, terrain.cave },
                { 0x611A, terrain.cave },
                { 0x611B, terrain.cave },
                { 0x611C, terrain.cave },
                { 0x611D, terrain.cave },
                { 0x611E, terrain.cave },
                { 0x611F, terrain.cave },
                { 0x6120, terrain.cave },
                { 0x6121, terrain.cave },
                { 0x6122, terrain.cave },
                { 0x6123, terrain.cave },
                { 0x6124, terrain.cave },
                { 0x6125, terrain.cave },
                { 0x6126, terrain.cave },
                { 0x6127, terrain.cave },
                { 0x6128, terrain.cave },
                { 0x6129, terrain.cave },
                { 0x612A, terrain.cave },
                { 0x612B, terrain.cave },
                { 0x612C, terrain.cave },
                { 0x612D, terrain.cave },
                { 0x612E, terrain.cave },
                { 0x612F, terrain.cave },
                { 0x6130, terrain.cave },
                { 0x6136, terrain.cave },
                { 0x6137, terrain.cave },
                { 0x6144, terrain.cave }
        };

        public Dictionary<Location, List<Location>> connectionsDM;
        public Location hammerCave;
        public Location magicCave;
        public Location hammerEnter;
        public Location hammerExit;

        public DeathMountain(Hyrule hy)
            : base(hy)
        {
            loadLocations(0x610C, 37, terrains);
            loadLocations(0x6136, 2, terrains);
            loadLocations(0x6144, 1, terrains);

            hammerCave = getLocationByMem(0x6128);
            hammerEnter = getLocationByMem(0x6136);
            hammerExit = getLocationByMem(0x6137);
            magicCave = getLocationByMem(0x6144);

            reachableAreas = new HashSet<string>();
            connectionsDM = new Dictionary<Location, List<Location>>();
            connectionsDM.Add(getLocationByMem(0x610C), new List<Location>() { getLocationByMem(0x610D) });
            connectionsDM.Add(getLocationByMem(0x610D), new List<Location>() { getLocationByMem(0x610C) });
            connectionsDM.Add(getLocationByMem(0x610E), new List<Location>() { getLocationByMem(0x610F) });
            connectionsDM.Add(getLocationByMem(0x610F), new List<Location>() { getLocationByMem(0x610E) });
            connectionsDM.Add(getLocationByMem(0x6110), new List<Location>() { getLocationByMem(0x6111) });
            connectionsDM.Add(getLocationByMem(0x6111), new List<Location>() { getLocationByMem(0x6110) });
            connectionsDM.Add(getLocationByMem(0x6112), new List<Location>() { getLocationByMem(0x6113) });
            connectionsDM.Add(getLocationByMem(0x6113), new List<Location>() { getLocationByMem(0x6112) });
            connectionsDM.Add(getLocationByMem(0x6114), new List<Location>() { getLocationByMem(0x6115) });
            connectionsDM.Add(getLocationByMem(0x6115), new List<Location>() { getLocationByMem(0x6114) });
            connectionsDM.Add(getLocationByMem(0x6116), new List<Location>() { getLocationByMem(0x6117) });
            connectionsDM.Add(getLocationByMem(0x6117), new List<Location>() { getLocationByMem(0x6116) });
            connectionsDM.Add(getLocationByMem(0x6118), new List<Location>() { getLocationByMem(0x6119) });
            connectionsDM.Add(getLocationByMem(0x6119), new List<Location>() { getLocationByMem(0x6118) });
            connectionsDM.Add(getLocationByMem(0x611A), new List<Location>() { getLocationByMem(0x611B) });
            connectionsDM.Add(getLocationByMem(0x611B), new List<Location>() { getLocationByMem(0x611A) });
            connectionsDM.Add(getLocationByMem(0x611C), new List<Location>() { getLocationByMem(0x611D) });
            connectionsDM.Add(getLocationByMem(0x611D), new List<Location>() { getLocationByMem(0x611C) });
            connectionsDM.Add(getLocationByMem(0x611E), new List<Location>() { getLocationByMem(0x611F) });
            connectionsDM.Add(getLocationByMem(0x611F), new List<Location>() { getLocationByMem(0x611E) });
            connectionsDM.Add(getLocationByMem(0x6120), new List<Location>() { getLocationByMem(0x6121) });
            connectionsDM.Add(getLocationByMem(0x6121), new List<Location>() { getLocationByMem(0x6120) });
            connectionsDM.Add(getLocationByMem(0x6122), new List<Location>() { getLocationByMem(0x6123) });
            connectionsDM.Add(getLocationByMem(0x6123), new List<Location>() { getLocationByMem(0x6122) });
            connectionsDM.Add(getLocationByMem(0x6124), new List<Location>() { getLocationByMem(0x6125) });
            connectionsDM.Add(getLocationByMem(0x6125), new List<Location>() { getLocationByMem(0x6124) });
            connectionsDM.Add(getLocationByMem(0x6126), new List<Location>() { getLocationByMem(0x6127) });
            connectionsDM.Add(getLocationByMem(0x6127), new List<Location>() { getLocationByMem(0x6126) });
            connectionsDM.Add(getLocationByMem(0x6129), new List<Location>() { getLocationByMem(0x612A), getLocationByMem(0x612B), getLocationByMem(0x612C) });
            connectionsDM.Add(getLocationByMem(0x612D), new List<Location>() { getLocationByMem(0x612E), getLocationByMem(0x612F), getLocationByMem(0x6130) });
            connectionsDM.Add(getLocationByMem(0x612E), new List<Location>() { getLocationByMem(0x612D), getLocationByMem(0x612F), getLocationByMem(0x6130) });
            connectionsDM.Add(getLocationByMem(0x612F), new List<Location>() { getLocationByMem(0x612E), getLocationByMem(0x612D), getLocationByMem(0x6130) });
            connectionsDM.Add(getLocationByMem(0x6130), new List<Location>() { getLocationByMem(0x612E), getLocationByMem(0x612F), getLocationByMem(0x612D) });

            enemies = new List<int> { 3, 4, 5, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32 };
            flyingEnemies = new List<int> { 0x06, 0x07, 0x0A, 0x0D, 0x0E };
            generators = new List<int> { 11, 12, 15, 29 };
            shorties = new List<int> { 3, 4, 5, 17, 18, 0x1C, 0x1F };
            tallGuys = new List<int> { 0x20, 20, 21, 22, 23, 24, 25, 26, 27 };
            enemyAddr = 0x48B0;
            enemyPtr = 0x608E;

            overworldMaps = new List<int>();
            MAP_ROWS = 45;
            MAP_COLS = 64;
    }

        public bool terraform()
        {
            bcount = 900;
            while (bcount > 801)
            {
                map = new terrain[MAP_ROWS, MAP_COLS];

                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < 29; j++)
                    {
                        map[i, j] = terrain.none;
                    }
                    for (int j = 29; j < MAP_COLS; j++)
                    {
                        map[i, j] = terrain.walkablewater;
                    }
                }
                int x = 0;
                int y = 0;
                foreach (Location l in AllLocations)
                {
                    if (l.TerrainType != terrain.bridge && l != magicCave)
                    {
                        do
                        {
                            x = hy.R.Next(MAP_COLS - 2) + 1;
                            y = hy.R.Next(MAP_ROWS - 2) + 1;
                        } while (map[y, x] != terrain.none || map[y - 1, x] != terrain.none || map[y + 1, x] != terrain.none || map[y + 1, x + 1] != terrain.none || map[y, x + 1] != terrain.none || map[y - 1, x + 1] != terrain.none || map[y + 1, x - 1] != terrain.none || map[y, x - 1] != terrain.none || map[y - 1, x - 1] != terrain.none);

                        map[y, x] = l.TerrainType;
                        l.Xpos = x;
                        l.Ypos = y + 30;
                        if (l.TerrainType == terrain.cave)
                        {
                            int dir = hy.R.Next(4);
                            terrain s = walkable[hy.R.Next(walkable.Length)];
                            if (dir == 0) //south
                            {
                                map[y + 1, x] = s;
                                map[y + 1, x + 1] = s;
                                map[y + 1, x - 1] = s;
                                map[y, x - 1] = terrain.mountain;
                                map[y, x + 1] = terrain.mountain;
                                map[y - 1, x - 1] = terrain.mountain;
                                map[y - 1, x] = terrain.mountain;
                                map[y - 1, x + 1] = terrain.mountain;
                            }
                            else if (dir == 1) //west
                            {
                                map[y + 1, x] = terrain.mountain;
                                map[y + 1, x + 1] = terrain.mountain;
                                map[y + 1, x - 1] = s;
                                map[y, x - 1] = s;
                                map[y, x + 1] = terrain.mountain;
                                map[y - 1, x - 1] = s;
                                map[y - 1, x] = terrain.mountain;
                                map[y - 1, x + 1] = terrain.mountain;
                            }
                            else if (dir == 2) //north
                            {
                                map[y + 1, x] = terrain.mountain;
                                map[y + 1, x + 1] = terrain.mountain;
                                map[y + 1, x - 1] = terrain.mountain;
                                map[y, x - 1] = terrain.mountain;
                                map[y, x + 1] = terrain.mountain;
                                map[y - 1, x - 1] = s;
                                map[y - 1, x] = s;
                                map[y - 1, x + 1] = s;
                            }
                            else if (dir == 3) //east
                            {
                                map[y + 1, x] = terrain.mountain;
                                map[y + 1, x + 1] = s;
                                map[y + 1, x - 1] = terrain.mountain;
                                map[y, x - 1] = terrain.mountain;
                                map[y, x + 1] = s;
                                map[y - 1, x - 1] = terrain.mountain;
                                map[y - 1, x] = terrain.mountain;
                                map[y - 1, x + 1] = s;
                            }
                        }
                    }
                }

                if(!growTerrain())
                {
                    return false;
                }
                

                do
                {
                    x = hy.R.Next(MAP_COLS - 2) + 1;
                    y = hy.R.Next(MAP_ROWS - 2) + 1;
                } while (!walkable.Contains(map[y, x]) || map[y + 1, x] == terrain.cave || map[y - 1, x] == terrain.cave || map[y, x + 1] == terrain.cave || map[y, x - 1] == terrain.cave);

                map[y, x] = terrain.rock;
                magicCave.Ypos = y + 30;
                magicCave.Xpos = x;

                //check bytes and adjust
                writeBytes(false, 0x665C, 801, 0, 0);
            }
            writeBytes(true, 0x665C, 801, 0, 0);

            int loc = 0x7C00 + bcount;
            int high = (loc & 0xFF00) >> 8;
            int low = loc & 0xFF;

            hy.ROMData.put(0x47A5, (Byte)low);
            hy.ROMData.put(0x47A6, (Byte)high);

            v = new bool[MAP_ROWS, MAP_COLS];
            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    v[i, j] = false;
                }
            }

            for (int i = 0x610C; i < 0x6149; i++)
            {
                if (!terrains.Keys.Contains(i))
                {
                    hy.ROMData.put(i, 0x00);
                }
            }
            return true;
        }

        public void updateVisit()
        {
            updateReachable();

            foreach (Location l in AllLocations)
            {
                if (v[l.Ypos - 30, l.Xpos])
                {
                    l.Reachable = true;
                    if (connectionsDM.Keys.Contains(l))
                    {
                        List<Location> l2 = connectionsDM[l];

                        foreach(Location l3 in l2)
                        { 
                            l3.Reachable = true;
                            v[l3.Ypos - 30, l3.Xpos] = true;
                        }
                    }
                }
            }
        }

        public void setStart()
        {
            v[hammerEnter.Ypos - 30, hammerEnter.Xpos] = true;
        }

        public void setStart2()
        {
            v[hammerExit.Ypos - 30, hammerExit.Xpos] = true;
        }
    }
}
