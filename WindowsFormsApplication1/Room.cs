﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2Randomizer
{
    class Room
    {
        private int map;
        private Byte[] connections;
        private int leftByte;
        private int rightByte;
        private int upByte;
        private int downByte;
        private Room left;
        private Room right;
        private Room up;
        private Room down;
        private Boolean isRoot;
        private Boolean isReachable;
        private Boolean beforeTbird;
        private int memAddr;
        private Boolean isDeadEnd;
        private Boolean isPlaced;
        private Boolean udRev;

        public int Map
        {
            get
            {
                return map;
            }

            set
            {
                map = value;
            }
        }

        public bool IsRoot
        {
            get
            {
                return isRoot;
            }

            set
            {
                isRoot = value;
            }
        }

        public Room Left
        {
            get
            {
                return left;
            }

            set
            {
                left = value;
            }
        }

        public Room Right
        {
            get
            {
                return right;
            }

            set
            {
                right = value;
            }
        }

        public Room Up
        {
            get
            {
                if(udRev)
                {
                    return down;
                }
                return up;
            }

            set
            {
                if(udRev)
                {
                    down = value;
                    return;
                }
                up = value;
            }
        }

        public Room Down
        {
            get
            {
                if(udRev)
                {
                    return up;
                }
                return down;
            }

            set
            {
                if(udRev)
                {
                    up = value;
                    return;
                }
                down = value;
            }
        }

        public bool IsReachable
        {
            get
            {
                return isReachable;
            }

            set
            {
                isReachable = value;
            }
        }

        public int MemAddr
        {
            get
            {
                return memAddr;
            }

            set
            {
                memAddr = value;
            }
        }

        public byte[] Connections
        {
            get
            {
                return connections;
            }

            set
            {
                connections = value;
            }
        }









        public bool IsDeadEnd
        {
            get
            {
                return isDeadEnd;
            }

            set
            {
                isDeadEnd = value;
            }
        }

        public bool IsPlaced
        {
            get
            {
                return isPlaced;
            }

            set
            {
                isPlaced = value;
            }
        }

        public int LeftByte
        {
            get
            {
                return leftByte;
            }

            set
            {
                leftByte = value;
            }
        }

        public int RightByte
        {
            get
            {
                return rightByte;
            }

            set
            {
                rightByte = value;
            }
        }

        public int UpByte
        {
            get
            {
                if(udRev)
                {
                    return downByte;
                }
                return upByte;
            }

            set
            {
                if(udRev)
                {
                    downByte = value;
                    return;
                }
                upByte = value;
            }
        }

        public int DownByte
        {
            get
            {
                if(udRev)
                {
                    return upByte;
                }
                return downByte;
            }

            set
            {
                if(udRev)
                {
                    upByte = value;
                    return;
                }
                downByte = value;
            }
        }

        public bool BeforeTbird
        {
            get
            {
                return beforeTbird;
            }

            set
            {
                beforeTbird = value;
            }
        }

        public Room(int map, Byte[] conn, int memAddr, bool upDownRev)
        {
            this.map = map;
            connections = conn;
            leftByte = conn[0];
            downByte = conn[1];
            upByte = conn[2];
            rightByte = conn[3];
            isRoot = false;
            isReachable = false;
            MemAddr = memAddr;
            isPlaced = false;
            left = null;
            right = null;
            up = null;
            down = null;
            beforeTbird = false;
            udRev = upDownRev;
        }

        public void updateBytes()
        {
            connections[0] = (Byte)leftByte;
            connections[1] = (Byte)downByte;
            connections[2] = (Byte)upByte;
            connections[3] = (Byte)rightByte;
        }
    }
}
