﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z2Randomizer
{
    
    /*
    Classes Needed:

    * Location - Represents entry from overworld
        * Terrain type
        * Requires Jump
        * Requires Fairy
        * Requires Hammer
        * X Position
        * Y Position
        * Exit Location?
        * Contains Item
        * Enter from right
        * Map Number
    
    * World - Represent a single map
        * Location Tree
            * Root of tree is starting position, children are areas directly accessible from position
        * Lists of locations broken down by terrain
        * Entry / Exit Points

    * Hyrule
        * Does the randomization
        * Checks for Sanity
        * Contains links to all World objects
    
    * Room - Palace only?

    * Palace
        * Pallette
        * Enemies?
        * Rooms
    */
    class ROM
    {
        private byte[] ROMData;

        public ROM(String filename)
        {
            try
            {
                FileStream fs;
                
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

                BinaryReader br = new BinaryReader(fs, new ASCIIEncoding());
                ROMData = br.ReadBytes(257 * 1024);

            }
            catch
            {
                MessageBox.Show("Cannot find or read file to dump.");
            }
        }

        public Byte getByte(int index)
        {
            return ROMData[index];
        }

        public Byte[] getBytes(int start, int end)
        {
            return ROMData.ToArray();
        }

        public void put(int index, Byte data)
        {
            ROMData[index] = data;
        }

        public void dump(String filename)
        {
            File.WriteAllBytes(filename, ROMData);
        }
    }
}
