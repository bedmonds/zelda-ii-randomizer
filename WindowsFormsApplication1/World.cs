﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Z2Randomizer
{
    abstract class World
    {
        private List<Location> caves;
        private List<Location> towns;
        private List<Location> palaces;
        private List<Location> grasses;
        private List<Location> swamps;
        private List<Location> bridges;
        private List<Location> deserts;
        private List<Location> forests;
        private List<Location> graves;
        private List<Location> lavas;
        private List<Location> roads;
        private List<Location>[] locations;
        private List<Location> allLocations;
        public Dictionary<Location, Location> connections;
        protected HashSet<String> reachableAreas;
        protected int enemyAddr;
        protected List<int> enemies;
        protected List<int> flyingEnemies;
        protected List<int> generators;
        protected List<int> shorties;
        protected List<int> tallGuys;
        protected int enemyPtr;
        protected List<int> overworldMaps;
        protected SortedDictionary<Tuple<int, int>, Location> locsByCoords;
        protected Hyrule hy;
        protected terrain[,] map;
        private const int overworldXOff = 0x3F;
        private const int overworldMapOff = 0x7E;
        private const int overworldWorldOff = 0xBD;
        private List<int> visitedEnemies;
        protected int MAP_ROWS;
        protected int MAP_COLS;
        protected int bcount;
        protected readonly terrain[] randomTerrains = { terrain.desert, terrain.grass, terrain.forest, terrain.swamp, terrain.grave, terrain.mountain, terrain.walkablewater };
        protected readonly terrain[] walkable = { terrain.desert, terrain.grass, terrain.forest, terrain.swamp, terrain.grave };
        protected bool[,] v;

        public List<Location> AllLocations
        {
            get
            {
                return allLocations;
            }
        }

        public List<Location> Palaces
        {
            get
            {
                return palaces;
            }

            set
            {
                palaces = value;
            }
        }

        public List<Location> Towns
        {
            get
            {
                return towns;
            }

            set
            {
                towns = value;
            }
        }

        internal List<Location> Caves
        {
            get
            {
                return caves;
            }

            set
            {
                caves = value;
            }
        }

        internal List<Location> Grasses
        {
            get
            {
                return grasses;
            }

            set
            {
                grasses = value;
            }
        }

        internal List<Location> Swamps
        {
            get
            {
                return swamps;
            }

            set
            {
                swamps = value;
            }
        }

        internal List<Location> Bridges
        {
            get
            {
                return bridges;
            }

            set
            {
                bridges = value;
            }
        }

        internal List<Location> Deserts
        {
            get
            {
                return deserts;
            }

            set
            {
                deserts = value;
            }
        }

        internal List<Location> Forests
        {
            get
            {
                return forests;
            }

            set
            {
                forests = value;
            }
        }

        internal List<Location> Graves
        {
            get
            {
                return graves;
            }

            set
            {
                graves = value;
            }
        }

        internal List<Location> Lavas
        {
            get
            {
                return lavas;
            }

            set
            {
                lavas = value;
            }
        }

        internal List<Location> Roads
        {
            get
            {
                return roads;
            }

            set
            {
                roads = value;
            }
        }

        public World(Hyrule parent)
        {
            hy = parent;
            caves = new List<Location>();
            towns = new List<Location>();
            palaces = new List<Location>();
            grasses = new List<Location>();
            swamps = new List<Location>();
            bridges = new List<Location>();
            deserts = new List<Location>();
            forests = new List<Location>();
            graves = new List<Location>();
            lavas = new List<Location>();
            roads = new List<Location>();
            connections = new Dictionary<Location, Location>();
            locations = new List<Location>[11] { towns, caves, palaces, bridges, deserts, grasses, forests, swamps, graves, roads, lavas };
            allLocations = new List<Location>();
            locsByCoords = new SortedDictionary<Tuple<int, int>, Location>();
            reachableAreas = new HashSet<string>();
            visitedEnemies = new List<int>();
        }

        public void addLocation(Location l)
        {
            if (l.TerrainType == terrain.walkablewater)
            {
                locations[(int)terrain.swamp].Add(l);
            }
            else
            {
                locations[(int)l.TerrainType].Add(l);
            }
            allLocations.Add(l);
            locsByCoords.Add(l.Coords, l);
        }

        protected void shuffleLocations(List<Location> westLocs)
        {
            for (int i = 0; i < westLocs.Count; i++)
            {

                int s = hy.R.Next(i, westLocs.Count);
                Location sl = westLocs[s];
                if (sl.CanShuffle && westLocs[i].CanShuffle)
                {
                    swap(westLocs[i], westLocs[s]);
                }
            }
        }


        protected void swap(Location l1, Location l2)
        {
            int tempX = l1.Xpos;
            int tempY = l1.Ypos;
            int tempPass = l1.PassThrough;
            l1.Xpos = l2.Xpos;
            l1.Ypos = l2.Ypos;
            l1.PassThrough = l2.PassThrough;
            l2.Xpos = tempX;
            l2.Ypos = tempY;
            l2.PassThrough = tempPass;

        }

        public void shuffleEnemies(int addr, bool isOver)
        {
            if (isOver)
            {
                addr = addr + hy.ROMData.getByte(addr);
            }
            if (!visitedEnemies.Contains(addr) && addr != 0x95A4)
            {
                int numBytes = hy.ROMData.getByte(addr);
                for (int j = addr + 2; j < addr + numBytes; j = j + 2)
                {
                    int enemy = hy.ROMData.getByte(j) & 0x3F;
                    int highPart = hy.ROMData.getByte(j) & 0xC0;
                    if (hy.Props.mixEnemies)
                    {
                        if (enemies.Contains(enemy))
                        {
                            int swap = enemies[hy.R.Next(0, enemies.Count)];
                            hy.ROMData.put(j, (Byte)(swap + highPart));
                            if ((shorties.Contains(enemy) && tallGuys.Contains(swap) && swap != 0x20))
                            {
                                int ypos = hy.ROMData.getByte(j - 1) & 0xF0;
                                int xpos = hy.ROMData.getByte(j - 1) & 0x0F;
                                ypos = ypos - 32;
                                hy.ROMData.put(j - 1, (Byte)(ypos + xpos));
                            }
                            else if (swap == 0x20 && swap != enemy)
                            {
                                int ypos = hy.ROMData.getByte(j - 1) & 0xF0;
                                int xpos = hy.ROMData.getByte(j - 1) & 0x0F;
                                ypos = ypos - 48;
                                hy.ROMData.put(j - 1, (Byte)(ypos + xpos));
                            }
                            else if (enemy == 0x1F && swap != enemy)
                            {
                                int ypos = hy.ROMData.getByte(j - 1) & 0xF0;
                                int xpos = hy.ROMData.getByte(j - 1) & 0x0F;
                                ypos = ypos - 16;
                                hy.ROMData.put(j - 1, (Byte)(ypos + xpos));
                            }
                        }
                    }
                    else
                    {

                        if (tallGuys.Contains(enemy))
                        {
                            int swap = hy.R.Next(0, tallGuys.Count);
                            if (tallGuys[swap] == 0x20 && tallGuys[swap] != enemy)
                            {
                                int ypos = hy.ROMData.getByte(j - 1) & 0xF0;
                                int xpos = hy.ROMData.getByte(j - 1) & 0x0F;
                                ypos = ypos - 48;
                                hy.ROMData.put(j - 1, (Byte)(ypos + xpos));
                            }
                            hy.ROMData.put(j, (Byte)(tallGuys[swap] + highPart));
                        }

                        if (shorties.Contains(enemy))
                        {
                            int swap = hy.R.Next(0, shorties.Count);
                            hy.ROMData.put(j, (Byte)(shorties[swap] + highPart));
                        }
                    }

                    if (flyingEnemies.Contains(enemy))
                    {
                        int swap = hy.R.Next(0, flyingEnemies.Count);
                        hy.ROMData.put(j, (Byte)(flyingEnemies[swap] + highPart));

                        if (flyingEnemies[swap] == 0x07 || flyingEnemies[swap] == 0x0a || flyingEnemies[swap] == 0x0d || flyingEnemies[swap] == 0x0e)
                        {
                            int ypos = 0x00;
                            int xpos = hy.ROMData.getByte(j - 1) & 0x0F;
                            hy.ROMData.put(j - 1, (Byte)(ypos + xpos));
                        }
                    }

                    if (generators.Contains(enemy))
                    {
                        int swap = hy.R.Next(0, generators.Count);
                        hy.ROMData.put(j, (Byte)(generators[swap] + highPart));
                    }

                    if (enemy == 33)
                    {
                        int swap = hy.R.Next(0, generators.Count + 1);
                        if (swap != generators.Count)
                        {
                            hy.ROMData.put(j, (Byte)(generators[swap] + highPart));
                        }
                    }
                }
                visitedEnemies.Add(addr);
            }
        }
        protected void loadLocations(int startAddr, int locNum, SortedDictionary<int, terrain> terrains)
        {
            for (int i = 0; i < locNum; i++)
            {
                Byte[] bytes = new Byte[4] { hy.ROMData.getByte(startAddr + i), hy.ROMData.getByte(startAddr + overworldXOff + i), hy.ROMData.getByte(startAddr + overworldMapOff + i), hy.ROMData.getByte(startAddr + overworldWorldOff + i) };
                addLocation(new Location(bytes, terrains[startAddr + i], startAddr + i));
            }
        }

        protected Location getLocationByMap(int map, int world)
        {
            Location l = null;
            foreach (Location loc in allLocations)
            {
                if (loc.LocationBytes[2] == map && loc.World == world)
                {
                    l = loc;
                    break;
                }
            }
            return l;
        }

        protected Location getLocationByCoords(Tuple<int, int> coords)
        {
            Location l = null;
            foreach (Location loc in allLocations)
            {
                if (loc.Coords.Equals(coords))
                {
                    l = loc;
                    break;
                }
            }
            if (l == null)
            {
                //Console.Write(coords);
            }
            return l;
        }

        protected Location getLocationByMem(int mem)
        {
            Location l = null;
            foreach (Location loc in allLocations)
            {
                if (loc.MemAddress == mem)
                {
                    l = loc;
                    break;
                }
            }
            return l;
        }

        public void shuffleE()
        {
            for (int i = enemyPtr; i < enemyPtr + 126; i = i + 2)
            {
                int low = hy.ROMData.getByte(i);
                int high = hy.ROMData.getByte(i + 1);
                high = high << 8;
                high = high & 0x0FFF;
                int addr = high + low + enemyAddr;
                shuffleEnemies(high + low + enemyAddr, false);
            }

            foreach (int i in overworldMaps)
            {
                int ptrAddr = enemyPtr + i * 2;
                int low = hy.ROMData.getByte(ptrAddr);
                int high = hy.ROMData.getByte(ptrAddr + 1);
                high = high << 8;
                high = high & 0x0FFF;
                int addr = high + low + enemyAddr;
                shuffleEnemies(high + low + enemyAddr, true);
            }
        }

        protected void placeLocations()
        {
            foreach (Location l in AllLocations)
            {
                if (l.TerrainType != terrain.bridge && l.CanShuffle)
                {
                    int x = 0;
                    int y = 0;
                    do
                    {
                        x = hy.R.Next(MAP_COLS - 2) + 1;
                        y = hy.R.Next(MAP_ROWS - 2) + 1;
                    } while (map[y, x] != terrain.none || map[y - 1, x] != terrain.none || map[y + 1, x] != terrain.none || map[y + 1, x + 1] != terrain.none || map[y, x + 1] != terrain.none || map[y - 1, x + 1] != terrain.none || map[y + 1, x - 1] != terrain.none || map[y, x - 1] != terrain.none || map[y - 1, x - 1] != terrain.none);

                    map[y, x] = l.TerrainType;
                    if (l.TerrainType == terrain.cave)
                    {
                        int dir = hy.R.Next(4);
                        terrain s = walkable[hy.R.Next(walkable.Length)];
                        if (dir == 0) //south
                        {
                            map[y + 1, x] = s;
                            map[y + 1, x + 1] = s;
                            map[y + 1, x - 1] = s;
                            map[y, x - 1] = terrain.mountain;
                            map[y, x + 1] = terrain.mountain;
                            map[y - 1, x - 1] = terrain.mountain;
                            map[y - 1, x] = terrain.mountain;
                            map[y - 1, x + 1] = terrain.mountain;
                        }
                        else if (dir == 1) //west
                        {
                            map[y + 1, x] = terrain.mountain;
                            map[y + 1, x + 1] = terrain.mountain;
                            map[y + 1, x - 1] = s;
                            map[y, x - 1] = s;
                            map[y, x + 1] = terrain.mountain;
                            map[y - 1, x - 1] = s;
                            map[y - 1, x] = terrain.mountain;
                            map[y - 1, x + 1] = terrain.mountain;
                        }
                        else if (dir == 2) //north
                        {
                            map[y + 1, x] = terrain.mountain;
                            map[y + 1, x + 1] = terrain.mountain;
                            map[y + 1, x - 1] = terrain.mountain;
                            map[y, x - 1] = terrain.mountain;
                            map[y, x + 1] = terrain.mountain;
                            map[y - 1, x - 1] = s;
                            map[y - 1, x] = s;
                            map[y - 1, x + 1] = s;
                        }
                        else if (dir == 3) //east
                        {
                            map[y + 1, x] = terrain.mountain;
                            map[y + 1, x + 1] = s;
                            map[y + 1, x - 1] = terrain.mountain;
                            map[y, x - 1] = terrain.mountain;
                            map[y, x + 1] = s;
                            map[y - 1, x - 1] = terrain.mountain;
                            map[y - 1, x] = terrain.mountain;
                            map[y - 1, x + 1] = s;
                        }
                    }
                    else if (l.TerrainType == terrain.palace)
                    {
                        terrain s = walkable[hy.R.Next(walkable.Length)];
                        map[y + 1, x] = s;
                        map[y + 1, x + 1] = s;
                        map[y + 1, x - 1] = s;
                        map[y, x - 1] = s;
                        map[y, x + 1] = s;
                        map[y - 1, x - 1] = s;
                        map[y - 1, x] = s;
                        map[y - 1, x + 1] = s;
                    }
                    else if(l.TerrainType != terrain.town || l.townNum != 9) //don't place newkasuto2
                    {
                        terrain t = terrain.none;
                        do
                        {
                            t = walkable[hy.R.Next(walkable.Length)];
                        } while (t == l.TerrainType);
                        map[y + 1, x] = t;
                        map[y + 1, x + 1] = t;
                        map[y + 1, x - 1] = t;
                        map[y, x - 1] = t;
                        map[y, x + 1] = t;
                        map[y - 1, x - 1] = t;
                        map[y - 1, x] = t;
                        map[y - 1, x + 1] = t;
                    }
                    l.Xpos = x;
                    l.Ypos = y + 30;
                }
            }
        }

        protected bool growTerrain()
        {
            terrain[,] mapCopy = new terrain[MAP_ROWS, MAP_COLS];
            List<Tuple<int, int>> placed = new List<Tuple<int, int>>();
            for(int i = 0; i < MAP_ROWS; i++)
            {
                for(int j = 0; j < MAP_COLS; j++)
                {
                    if (map[i, j] != terrain.none && randomTerrains.Contains(map[i, j]))
                    {
                        placed.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    if (map[i, j] == terrain.none)
                    {
                        List<terrain> choices = new List<terrain>();
                        double mindistance = 9999999999999999999;

                        foreach(Tuple<int, int> t in placed)
                        {
                            double tx = t.Item1 - i;
                            double ty = t.Item2 - j;
                            double distance = Math.Sqrt(tx * tx + ty * ty);
                            if (distance < mindistance)
                            {
                                choices = new List<terrain>();
                                choices.Add(map[t.Item1, t.Item2]);
                                mindistance = distance;
                            }
                            else if(distance == mindistance)
                            {
                                choices.Add(map[t.Item1, t.Item2]);
                            }
                        }
                        mapCopy[i, j] = choices[hy.R.Next(choices.Count)];
                    }
                }
            }

            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    if (map[i, j] != terrain.none)
                    {
                        mapCopy[i, j] = map[i, j];
                    }
                }
            }
            map = (terrain[,])mapCopy.Clone();
            return true;
            //"grow" terrain on map
            //terrain[,] mapCopy = new terrain[MAP_ROWS, MAP_COLS];
            //int tries = 0;
            //int total = 0;
            //for (int i = 0; i < MAP_ROWS; i++)
            //{
            //    for (int j = 0; j < MAP_COLS; j++)
            //    {
            //        mapCopy[i, j] = map[i, j];
            //    }
            //}
            //bool done = false;
            //while (!done && total < 100000)
            //{
            //    while (!done && tries < 10000)
            //    {
            //        done = true;
            //        for (int i = 0; i < MAP_ROWS; i++)
            //        {
            //            for (int j = 0; j < MAP_COLS; j++)
            //            {
            //                if (map[i, j] == terrain.none)
            //                {
            //                    done = false;
            //                    Dictionary<terrain, int> counts = new Dictionary<terrain, int>();
            //                    foreach (terrain t in randomTerrains)
            //                    {
            //                        counts[t] = 0;
            //                    }

            //                    for (int k = i - 1; k <= i + 1; k++)
            //                    {
            //                        for (int l = j - 1; l <= j + 1; l++)
            //                        {
            //                            if (k >= 0 && k < MAP_ROWS && l >= 0 && l < MAP_COLS && (k != i || l != j) && randomTerrains.Contains(map[k, l]))
            //                            {
            //                                counts[map[k, l]]++;
            //                            }
            //                        }
            //                    }
            //                    int max = 0;
            //                    terrain mt = terrain.none;
            //                    foreach (terrain t in randomTerrains)
            //                    {
            //                        if (counts[t] > max)
            //                        {
            //                            max = counts[t];
            //                            mt = t;
            //                        }
            //                    }
            //                    if (max > 0)
            //                    {
            //                        mapCopy[i, j] = mt;
            //                    }


            //                }
            //                else
            //                {
            //                    mapCopy[i, j] = map[i, j];
            //                }
            //            }
            //        }
            //        map = (terrain[,])mapCopy.Clone();
            //        tries++;
            //    }
            //    terrain t2 = walkable[hy.R.Next(walkable.Count())];
            //    for (int i = 0; i < MAP_ROWS; i++)
            //    {
            //        for (int j = 0; j < MAP_COLS; j++)
            //        {
            //            if (map[i, j] == terrain.none)
            //            {
            //                map[i, j] = t2;
            //            }
            //        }
            //    }
            //    total += tries;
            //}
            //return true;
        }

        protected void placeRandomTerrain(int num)
        {
            //randomly place remaining terrain
            int placed = 0;
            while (placed < num)
            {
                int x = 0;
                int y = 0;
                terrain t = randomTerrains[hy.R.Next(randomTerrains.Length)];
                do
                {
                    x = hy.R.Next(MAP_COLS);
                    y = hy.R.Next(MAP_ROWS);
                } while (map[y, x] != terrain.none);
                map[y, x] = t;
                placed++;
            }
        }

        protected void writeBytes(bool doWrite, int loc, int total, int h1, int h2)
        {
            bcount = 0;
            terrain curr2 = map[0, 0];
            int count2 = 0;
            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    if(hy.hiddenPalace && i == h1 && j == h2 && i != 0 && j != 0)
                    {
                        count2--;
                        int b = count2 * 16 + (int)curr2;
                        //Console.WriteLine("Hex: {0:X}", b);
                        if (doWrite)
                        {
                            hy.ROMData.put(loc, (Byte)b);
                            hy.ROMData.put(loc + 1, (byte)curr2);
                        }
                        count2 = 0;
                        loc += 2;
                        bcount += 2;
                        continue;
                    }
                    if (map[i, j] == curr2 && count2 < 16)
                    {
                        count2++;
                    }
                    else
                    {
                        count2--;
                        int b = count2 * 16 + (int)curr2;
                        //Console.WriteLine("Hex: {0:X}", b);
                        if (doWrite)
                        {
                            hy.ROMData.put(loc, (Byte)b);
                        }

                        curr2 = map[i, j];
                        count2 = 1;
                        loc++;
                        bcount++;
                    }
                }
                count2--;
                int b2 = count2 * 16 + (int)curr2;
                //Console.WriteLine("Hex: {0:X}", b2);
                if (doWrite)
                {
                    hy.ROMData.put(loc, (Byte)b2);
                }

                if (i < MAP_ROWS - 1)
                {
                    curr2 = map[i + 1, 0];
                }
                count2 = 0;
                loc++;
                bcount++;
            }

            while (bcount < total)
            {
                hy.ROMData.put(loc, (Byte)0x0B);
                bcount++;
                loc++;
            }
        }

        protected void drawOcean(bool left)
        {
            int x = 0;
            if (!left)
            {
                x = MAP_COLS - 1;
            }
            //draw ocean on right side
            int ostart = hy.R.Next(MAP_ROWS);
            int olength = hy.R.Next(10, 30);
            if (ostart < MAP_ROWS / 2)
            {
                for (int i = 0; i < olength; i++)
                {
                    map[ostart + i, x] = terrain.walkablewater;
                }
            }
            else
            {
                for (int i = 0; i < olength; i++)
                {
                    map[ostart - i, x] = terrain.walkablewater;
                }
            }
        }
        protected void updateReachable()
        {
            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < MAP_COLS; j++)
                    {
                        if (!v[i, j] && (map[i, j] == terrain.lava || map[i, j] == terrain.bridge || map[i, j] == terrain.cave || map[i, j] == terrain.road || map[i, j] == terrain.palace || map[i, j] == terrain.town || (map[i, j] == terrain.walkablewater && hy.itemGet[(int)items.boots]) || walkable.Contains(map[i, j]) || (map[i, j] == terrain.rock && hy.itemGet[(int)items.hammer]) || (map[i, j] == terrain.spider && hy.itemGet[(int)items.horn])))
                        {
                            if (i - 1 >= 0)
                            {
                                if (v[i - 1, j])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }

                            }

                            if (i + 1 < MAP_ROWS)
                            {
                                if (v[i + 1, j])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }

                            if (j - 1 >= 0)
                            {
                                if (v[i, j - 1])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }

                            if (j + 1 < MAP_COLS)
                            {
                                if (v[i, j + 1])
                                {
                                    v[i, j] = true;
                                    changed = true;
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void reset()
        {
            for(int i = 0; i < MAP_ROWS; i++)
            {
                for(int j = 0; j < MAP_COLS; j++)
                {
                    v[i, j] = false;
                }
            }
        }

        public bool allReachable()
        {

            foreach(Location l in allLocations)
            {
                if(!l.Reachable)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
