﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2Randomizer
{
    //6A31 - address in memory of kasuto y coord;
    //6A35 - address in memory of palace 6 y coord
    class EastHyrule : World
    {
        private int bridgeCount;
                private readonly SortedDictionary<int, terrain> terrains = new SortedDictionary<int, terrain>
            {
                { 0x862F, terrain.forest },
                { 0x8630, terrain.forest },
                { 0x8631, terrain.road },
                { 0x8632, terrain.road },
                { 0x8633, terrain.road },
                { 0x8634, terrain.road },
                { 0x8635, terrain.bridge },
                { 0x8636, terrain.bridge },
                { 0x8637, terrain.desert },
                { 0x8638, terrain.desert },
                { 0x8639, terrain.walkablewater },
                { 0x863A, terrain.cave },
                { 0x863B, terrain.cave },
                { 0x863C, terrain.cave },
                { 0x863D, terrain.cave },
                { 0x863E, terrain.cave },
                { 0x863F, terrain.cave },
                { 0x8640, terrain.cave },
                { 0x8641, terrain.cave },
                { 0x8642, terrain.cave },
                { 0x8643, terrain.cave },
                { 0x8644, terrain.swamp },
                { 0x8645, terrain.lava },
                { 0x8646, terrain.desert },
                { 0x8647, terrain.desert },
                { 0x8648, terrain.desert },
                { 0x8649, terrain.desert },
                { 0x864A, terrain.forest },
                { 0x864B, terrain.lava },
                { 0x864C, terrain.lava },
                { 0x864D, terrain.lava },
                { 0x864E, terrain.lava },
                { 0x864F, terrain.lava },
                { 0x8657, terrain.bridge },
                { 0x8658, terrain.bridge },
                { 0x865C, terrain.town },
                { 0x865E, terrain.town },
                { 0x8660, terrain.town },
                { 0x8662, terrain.town },
                { 0x8663, terrain.palace },
                { 0x8664, terrain.palace },
                { 0x8665, terrain.palace },

            };

        public Location start;
        public Location palace4;
        public Location palace5;
        public Location palace6;
        public Location heart1;
        public Location heart2;
        public Location darunia;
        public Location newKasuto;
        public Location newKasuto2;
        public Location fireTown;
        public Location oldKasuto;
        public Location gp;
        public Location pbagCave1;
        public Location pbagCave2;
        public Location hpCallSpot;
        

        public EastHyrule(Hyrule hy)
            : base(hy)
        {
            loadLocations(0x862F, 22, terrains);
            loadLocations(0x8646, 10, terrains);
            loadLocations(0x8657, 2, terrains);
            loadLocations(0x865C, 1, terrains);
            loadLocations(0x865E, 1, terrains);
            loadLocations(0x8660, 1, terrains);
            loadLocations(0x8662, 4, terrains);

            reachableAreas = new HashSet<string>();

            connections.Add(getLocationByMem(0x863A), getLocationByMem(0x863B));
            connections.Add(getLocationByMem(0x863B), getLocationByMem(0x863A));
            connections.Add(getLocationByMem(0x863E), getLocationByMem(0x863F));
            connections.Add(getLocationByMem(0x863F), getLocationByMem(0x863E));
            connections.Add(getLocationByMem(0x8640), getLocationByMem(0x8641));
            connections.Add(getLocationByMem(0x8641), getLocationByMem(0x8640));
            connections.Add(getLocationByMem(0x8642), getLocationByMem(0x8643));
            connections.Add(getLocationByMem(0x8643), getLocationByMem(0x8642));

            palace6 = getLocationByMem(0x8664);
            palace6.PalNum = 6;
            start = getLocationByMem(0x8658);
            darunia = getLocationByMem(0x865E);
            palace5 = getLocationByMap(0x23, 0x0E);
            palace5.PalNum = 5;
            
            newKasuto = getLocationByMem(0x8660);
            newKasuto2 = new Location(newKasuto.LocationBytes, newKasuto.TerrainType, newKasuto.MemAddress);
            heart1 = getLocationByMem(0x8639);
            heart2 = getLocationByMem(0x8649);
            if (palace5 == null)
            {
                palace5 = getLocationByMem(0x8657);
                palace5.PalNum = 5;
            }
            palace4 = getLocationByMap(0x0F, 0x11);
            if (palace4 == null)
            {
                palace4 = getLocationByMem(0x8657);
            }

            hpCallSpot = new Location();
            hpCallSpot.Xpos = 0;
            hpCallSpot.Ypos = 0;

            enemyAddr = 0x88B0;
            enemies = new List<int> { 03, 04, 05, 0x11, 0x12, 0x14, 0x16, 0x18, 0x19, 0x1A, 0x1B, 0x1C };
            flyingEnemies = new List<int> { 0x06, 0x07, 0x0A, 0x0D, 0x0E, 0x15 };
            generators = new List<int> { 0x0B, 0x0F, 0x17 };
            shorties = new List<int> { 0x03, 0x04, 0x05, 0x11, 0x12, 0x16 };
            tallGuys = new List<int> { 0x14, 0x18, 0x19, 0x1A, 0x1B, 0x1C };
            enemyPtr = 0x85B1;
            fireTown = getLocationByMem(0x865C);
            oldKasuto = getLocationByMem(0x8662);
            gp = getLocationByMem(0x8665);
            gp.PalNum = 7;
            gp.item = items.donotuse;
            pbagCave1 = getLocationByMem(0x863C);
            pbagCave2 = getLocationByMem(0x863D);

            overworldMaps = new List<int> { 0x22, 0x1D, 0x27, 0x35, 0x30, 0x1E, 0x28, 0x3C };

            MAP_ROWS = 75;
            MAP_COLS = 64;
    }
        


        public bool terraform()
        {
            bcount = 900;
            while (bcount > 792)
            {
                map = new terrain[MAP_ROWS, MAP_COLS];

                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < MAP_COLS; j++)
                    {
                        map[i, j] = terrain.none;
                    }
                }
                drawRoad();
                daruniaPath();
                drawMountains();
                drawRiver();
                drawVod();
                drawOcean(false);
                drawOcean(true);
                if (hy.hiddenKasuto)
                {
                    drawHiddenKasuto();
                }
                placeLocations();
                if (hy.hiddenPalace)
                {
                    drawHiddenPalace();
                }







                placeRandomTerrain(10);
                if (!growTerrain())
                {
                    return false;
                }
                drawRaft(true);
                drawRaft(false);
                writeBytes(false, 0x9056, 792, palace6.Ypos - 30, palace6.Xpos);
                Console.WriteLine("East:" + bcount);
            }
            if (hy.hiddenPalace)
            {
                updateHPspot();
            }
            if (hy.hiddenKasuto)
            {
                updateKasuto();
            }
            writeBytes(true, 0x9056, 792, palace6.Ypos - 30, palace6.Xpos);


            int loc3 = 0x7C00 + bcount;
            int high = (loc3 & 0xFF00) >> 8;
            int low = loc3 & 0xFF;

            hy.ROMData.put(0x879F, (Byte)low);
            hy.ROMData.put(0x87A0, (Byte)high);
            hy.ROMData.put(0x87A1, (Byte)low);
            hy.ROMData.put(0x87A2, (Byte)high);



            v = new bool[MAP_ROWS, MAP_COLS];
            for (int i = 0; i < MAP_ROWS; i++)
            {
                for (int j = 0; j < MAP_COLS; j++)
                {
                    v[i, j] = false;
                }
            }
            newKasuto.ExternalWorld = 128;
            palace6.ExternalWorld = 128;
            
            return true;
        }

        private void updateKasuto()
        {
            hy.ROMData.put(0x1df79, (byte)(newKasuto.Ypos + 128));
            hy.ROMData.put(0x1dfac, (byte)(newKasuto.Ypos - 30));
            hy.ROMData.put(0x1dfb2, (byte)(newKasuto.Xpos + 1));
            hy.ROMData.put(0x1ccd4, (byte)(newKasuto.Xpos));
            hy.ROMData.put(0x1ccdb, (byte)(newKasuto.Ypos));
            newKasuto.Needhammer = true;
            newKasuto2.Needhammer = true;
        }

        private void drawHiddenKasuto()
        {
            hy.ROMData.put(0x8660, 0);
            newKasuto.TerrainType = terrain.forest;
            newKasuto2.TerrainType = terrain.forest;
        }

        private void drawHiddenPalace()
        {
            bool done = false;
            int xpos = hy.R.Next(6, MAP_COLS - 6);
            int ypos = hy.R.Next(6, MAP_ROWS - 6);
            palace6.NeedRecorder = true;
            while (!done)
            {
                xpos = hy.R.Next(6, MAP_COLS - 6);
                ypos = hy.R.Next(6, MAP_ROWS - 6);
                done = true;
                for (int i = ypos - 3; i < ypos + 4; i++)
                {
                    for (int j = xpos - 3; j < xpos + 4; j++)
                    {
                        if (map[i, j] != terrain.none)
                        {
                            done = false;
                        }
                    }
                }
            }
            terrain t = walkable[hy.R.Next(walkable.Count())];
            while(t == terrain.forest)
            {
                t = walkable[hy.R.Next(walkable.Count())];
            }
            //t = terrain.desert;
            for (int i = ypos - 3; i < ypos + 4; i++)
            {
                for (int j = xpos - 3; j < xpos + 4; j++)
                {
                    if ((i == ypos - 2 && j == xpos) || (i == ypos && j == xpos - 2) || (i == ypos && j == xpos + 2))
                    {
                        map[i, j] = terrain.mountain;
                    }
                    else
                    {
                        map[i, j] = t;
                    }
                }
            }
            map[palace6.Ypos - 30, palace6.Xpos] = map[palace6.Ypos - 29, palace6.Xpos];
            palace6.Xpos = xpos;
            palace6.Ypos = ypos + 2 + 30;
            hpCallSpot.Xpos = xpos;
            hpCallSpot.Ypos = ypos + 30;
            hy.ROMData.put(0x1df70, (byte)t);
        }

        public void updateHPspot() { 
            

            hy.ROMData.put(0x8382, (byte)hpCallSpot.Ypos);
            hy.ROMData.put(0x8388, (byte)hpCallSpot.Xpos);
            int pos = palace6.Ypos;

            hy.ROMData.put(0x1df78, (byte)(pos + 128));
            hy.ROMData.put(0x1df84, 0xff);
            hy.ROMData.put(0x1ccc0, (byte)pos);
            

            int ppu_addr1 = 0x2000 + 2 * (32 * (palace6.Ypos % 15) + (palace6.Xpos % 16)) + 2048 * (palace6.Ypos % 30 / 15);
            int ppu_addr2 = ppu_addr1 + 32;
            int ppu1low = ppu_addr1 & 0x00ff;
            int ppu1high = (ppu_addr1 >> 8) & 0xff;
            int ppu2low = ppu_addr2 & 0x00ff;
            int ppu2high = (ppu_addr2 >> 8) & 0xff;
            hy.ROMData.put(0x1df7a, (byte)ppu1high);
            hy.ROMData.put(0x1df7b, (byte)ppu1low);
            hy.ROMData.put(0x1df7f, (byte)ppu2high);
            hy.ROMData.put(0x1df80, (byte)ppu2low);
            hy.ROMData.put(0x8664, 0);
            
        }

        private void drawRoad()
        {
            //Draw road structure
            int r1x = hy.R.Next(20, MAP_COLS - 20);
            int r1y = hy.R.Next(MAP_ROWS / 3);

            Location rl = new Location();
            rl.Xpos = r1x;
            rl.Ypos = r1y + 30;

            int r2x = hy.R.Next(MAP_COLS - 20, MAP_COLS);
            int r2y = hy.R.Next(MAP_ROWS / 3, MAP_ROWS);

            Location r2 = new Location();
            r2.Xpos = r2x;
            r2.Ypos = r2y + 30;

            map[rl.Ypos - 30, rl.Xpos] = terrain.road;
            map[r2.Ypos - 30, r2.Xpos] = terrain.road;
            drawLine(rl, r2, terrain.road);
            rl = r2;

            if (hy.R.NextDouble() > .5) //left
            {
                r2x = hy.R.Next(MAP_COLS / 2);
                r2y = hy.R.Next(MAP_ROWS / 3, MAP_ROWS);
            }
            else
            {
                r2x = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                r2y = hy.R.Next(MAP_ROWS / 3, MAP_ROWS);
            }
            r2 = new Location();
            r2.Xpos = r2x;
            r2.Ypos = r2y + 30;

            map[rl.Ypos - 30, rl.Xpos] = terrain.road;
            map[r2.Ypos - 30, r2.Xpos] = terrain.road;
            drawLine(rl, r2, terrain.road);
        }

        private void drawLine(Location to, Location from, terrain t)
        {
            int x = from.Xpos;
            int y = from.Ypos - 30;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if ((x != to.Xpos || y != (to.Ypos - 30)) && map[y, x] == terrain.none)
                        {
                            map[y, x] = t;
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                    }
                }
                else
                {
                    int diff = to.Ypos - 30 - y;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if ((x != to.Xpos || y != (to.Ypos - 30)) && map[y, x] == terrain.none)
                        {
                            map[y, x] = t;
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if (y > 0)
                        {
                            y--;
                        }
                        move--;
                    }
                }
            }
        }

        private void drawLine(Location to, Location from, terrain t, terrain fill)
        {
            int x = from.Xpos;
            int y = from.Ypos - 30;
            bridgeCount = 0;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if (t == terrain.walkablewater && (map[y, x] == terrain.road || (map[y, x] == terrain.desert && fill != terrain.desert)))
                        {
                            if (bridgeCount == 0 && move > 1 && (diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x8635);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                                b.CanShuffle = false;
                            }
                            else if (bridgeCount == 1 && move > 1 && (diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x8636);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                                b.CanShuffle = false;
                            }
                            else if ((diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                map[y, x] = terrain.bridge;
                            }
                            else
                            {
                                map[y, x] = t;
                            }
                        }
                        else if(map[y, x] != terrain.bridge)
                        {
                            map[y, x] = t;
                        }


                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = fill;
                                }

                            }
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                    }
                }
                else
                {
                    int diff = to.Ypos - y - 30;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if (t == terrain.walkablewater && (map[y, x] == terrain.road || (map[y, x] == terrain.desert && fill != terrain.desert)))
                        {
                            if (bridgeCount == 0 && move > 1 && (diff > 0 && (map[y + 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x8635);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                b.CanShuffle = false;
                                map[y, x] = terrain.bridge;
                            }
                            else if (bridgeCount == 1 && move > 1 && (diff > 0 && (map[y + 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x8636);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                b.CanShuffle = false;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                            }
                            else if ((diff > 0 && (map[y + 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                map[y, x] = terrain.bridge;
                            }
                            else
                            {
                                map[y, x] = t;
                            }
                        }
                        else
                        {
                            map[y, x] = t;
                        }
                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = fill;
                                }

                            }
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if (y > 0)
                        {
                            y--;
                        }
                        move--;
                    }
                }
            }
        }

        public void setStart()
        {
            v[start.Ypos - 30, start.Xpos] = true;
        }

 

        public void updateVisit()
        {
            updateReachable();

            foreach (Location l in AllLocations)
            {
                if (l.Ypos > 30)
                {
                    if (v[l.Ypos - 30, l.Xpos])
                    {
                        if ((!l.NeedRecorder && !l.Needhammer) || (l.NeedRecorder && hy.itemGet[(int)items.horn]) || (l.Needhammer && hy.itemGet[(int)items.hammer]))
                        {
                            l.Reachable = true;
                            if (connections.Keys.Contains(l))
                            {
                                Location l2 = connections[l];

                                if ((l.NeedBagu && (hy.westHyrule.bagu.Reachable || hy.spellGet[(int)spells.fairy])))
                                {
                                    l2.Reachable = true;
                                    v[l2.Ypos - 30, l2.Xpos] = true;
                                }

                                if (l.NeedFairy && hy.spellGet[(int)spells.fairy])
                                {
                                    l2.Reachable = true;
                                    v[l2.Ypos - 30, l2.Xpos] = true;
                                }

                                if (l.Needjump && hy.spellGet[(int)spells.jump])
                                {
                                    l2.Reachable = true;
                                    v[l2.Ypos - 30, l2.Xpos] = true;
                                }

                                if (!l.NeedFairy && !l.NeedBagu && !l.Needjump)
                                {
                                    l2.Reachable = true;
                                    v[l2.Ypos - 30, l2.Xpos] = true;
                                }
                            }
                        }
                    }
                }
                if(newKasuto.Reachable && newKasuto.townNum == 8)
                {
                    newKasuto2.Reachable = true;
                }
            }
        }

        private double computeDistance(Location l, Location l2)
        {
            return Math.Sqrt(Math.Pow(l.Xpos - l2.Xpos, 2) + Math.Pow(l.Ypos - l2.Ypos, 2));
        }

        private void daruniaPath()
        {
            //darunia path
            int gpx = hy.R.Next(MAP_COLS - 6) + 3;
            int gpy = hy.R.Next(MAP_ROWS / 2 - 2) + 1;


            Location from = new Location();
            Location to = new Location();

            from.Xpos = gpx;
            from.Ypos = gpy + 30;
            double d = 26;
            while (d > 25)
            {
                to.Xpos = hy.R.Next(MAP_COLS - 6) + 3;
                to.Ypos = hy.R.Next(MAP_ROWS / 2 - 2) + 34;


                d = computeDistance(from, to);
            }
            int encounterCount = 0;

            int x = from.Xpos;
            int y = from.Ypos - 30;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    bool first = true;
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        Location curr3 = new Location();
                        curr3.Xpos = x;
                        curr3.Ypos = y + 30;

                        map[y, x] = terrain.desert;
                        if (computeDistance(curr3, to) < .666 * d && move > 1 && encounterCount == 0 && !first)
                        {
                            Location b = getLocationByMem(0x8637);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .333 * d && move > 1 && encounterCount == 1 && !first)
                        {
                            Location b = getLocationByMem(0x8638);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }

                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = terrain.mountain;
                                }

                            }
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                        first = false;
                    }
                }
                else
                {
                    int diff = to.Ypos - y - 30;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    bool first = true;
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        Location curr3 = new Location();
                        curr3.Xpos = x;
                        curr3.Ypos = y + 30;

                        map[y, x] = terrain.desert;
                        if (computeDistance(curr3, to) < .666 * d && move > 1 && encounterCount == 0 && !first)
                        {
                            Location b = getLocationByMem(0x8637);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .333 * d && move > 1 && encounterCount == 1 && !first)
                        {
                            Location b = getLocationByMem(0x8638);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }

                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = terrain.mountain;
                                }

                            }
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if (y > 0)
                        {
                            y--;
                        }
                        move--;
                        first = false;
                    }
                }
            }
            if (encounterCount < 2)
            {
                Location b = getLocationByMem(0x8638);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (encounterCount < 1)
            {
                Location b = getLocationByMem(0x8637);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            map[to.Ypos - 30, to.Xpos] = terrain.desert;
            map[to.Ypos - 30 - 1, to.Xpos] = terrain.desert;
            map[to.Ypos - 30 + 1, to.Xpos] = terrain.desert;
            map[to.Ypos - 30 - 1, to.Xpos + 1] = terrain.desert;
            map[to.Ypos - 30 - 1, to.Xpos - 1] = terrain.desert;
            map[to.Ypos - 30 + 1, to.Xpos + 1] = terrain.desert;
            map[to.Ypos - 30 + 1, to.Xpos - 1] = terrain.desert;
            map[to.Ypos - 30, to.Xpos + 1] = terrain.desert;
            map[to.Ypos - 30, to.Xpos - 1] = terrain.desert;

            map[from.Ypos - 30, from.Xpos] = terrain.desert;
            map[to.Ypos - 30 - 1, from.Xpos] = terrain.desert;
            map[from.Ypos - 30 + 1, from.Xpos] = terrain.desert;
            map[from.Ypos - 30 - 1, from.Xpos + 1] = terrain.desert;
            map[from.Ypos - 30 - 1, from.Xpos - 1] = terrain.desert;
            map[from.Ypos - 30 + 1, from.Xpos + 1] = terrain.desert;
            map[from.Ypos - 30 + 1, from.Xpos - 1] = terrain.desert;
            map[from.Ypos - 30, from.Xpos + 1] = terrain.desert;
            map[from.Ypos - 30, from.Xpos - 1] = terrain.desert;
        }

        private void drawMountains()
        {
            //create some mountains
            int mounty = hy.R.Next(MAP_COLS / 3 - 10, MAP_COLS / 3 + 10);
            map[mounty, 0] = terrain.mountain;
            bool placedSpider = false;


            int endmounty = hy.R.Next(MAP_COLS / 3 - 10, MAP_COLS / 3 + 10);
            int endmountx = hy.R.Next(2, 8);
            int x2 = 0;
            int y2 = mounty;
            int roadEncounters = 0;
            while (x2 != (MAP_COLS - endmountx) || y2 != endmounty)
            {
                if (Math.Abs(x2 - (MAP_COLS - endmountx)) >= Math.Abs(y2 - endmounty))
                {
                    if (x2 > MAP_COLS - endmountx)
                    {
                        x2--;
                    }
                    else
                    {
                        x2++;
                    }
                }
                else
                {
                    if (y2 > endmounty)
                    {
                        y2--;
                    }
                    else
                    {
                        y2++;
                    }
                }
                if (x2 != MAP_COLS - endmountx || y2 != endmounty)
                {
                    if (map[y2, x2] == terrain.none)
                    {
                        map[y2, x2] = terrain.mountain;
                    }
                    else if (map[y2, x2] == terrain.road)
                    {
                        if (!placedSpider)
                        {
                            map[y2, x2] = terrain.spider;
                            placedSpider = true;
                        }
                        else if (map[y2, x2 + 1] == terrain.none && (((y2 > 0 && map[y2 - 1, x2] == terrain.road) && (y2 < MAP_ROWS - 1 && map[y2 + 1, x2] == terrain.road)) || ((x2 > 0 && map[y2, x2 - 0] == terrain.road) && (x2 < MAP_COLS - 1 && map[y2, x2 + 1] == terrain.road))))
                        {
                            if (roadEncounters == 0)
                            {
                                Location roadEnc = getLocationByMem(0x8631);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 1)
                            {
                                Location roadEnc = getLocationByMem(0x8632);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 2)
                            {
                                Location roadEnc = getLocationByMem(0x8633);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 3)
                            {
                                Location roadEnc = getLocationByMem(0x8634);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                        }
                    }
                }
            }

            mounty = hy.R.Next(MAP_COLS * 2 / 3 - 10, MAP_COLS * 2 / 3 + 10);
            map[mounty, 0] = terrain.mountain;

            endmounty = hy.R.Next(MAP_COLS * 2 / 3 - 10, MAP_COLS * 2 / 3 + 10);
            endmountx = hy.R.Next(2, 8);
            x2 = 0;
            y2 = mounty;
            while (x2 != (MAP_COLS - endmountx) || y2 != endmounty)
            {
                if (Math.Abs(x2 - (MAP_COLS - endmountx)) >= Math.Abs(y2 - endmounty))
                {
                    if (x2 > MAP_COLS - endmountx)
                    {
                        x2--;
                    }
                    else
                    {
                        x2++;
                    }
                }
                else
                {
                    if (y2 > endmounty)
                    {
                        y2--;
                    }
                    else
                    {
                        y2++;
                    }
                }
                if (x2 != MAP_COLS - endmountx || y2 != endmounty)
                {
                    if (map[y2, x2] == terrain.none)
                    {
                        map[y2, x2] = terrain.mountain;
                    }
                    else if (map[y2, x2] == terrain.road)
                    {
                        if (!placedSpider)
                        {
                            map[y2, x2] = terrain.spider;
                            placedSpider = true;
                        }
                        else if (map[y2, x2 + 1] == terrain.none && (((y2 > 0 && map[y2 - 1, x2] == terrain.road) && (y2 < MAP_ROWS - 1 && map[y2 + 1, x2] == terrain.road)) || ((x2 > 0 && map[y2, x2 - 0] == terrain.road) && (x2 < MAP_COLS - 1 && map[y2, x2 + 1] == terrain.road))))
                        {
                            if (roadEncounters == 0)
                            {
                                Location roadEnc = getLocationByMem(0x8631);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 1)
                            {
                                Location roadEnc = getLocationByMem(0x8632);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 2)
                            {
                                Location roadEnc = getLocationByMem(0x8633);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                            else if (roadEncounters == 3)
                            {
                                Location roadEnc = getLocationByMem(0x8634);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEncounters++;
                            }
                        }
                    }
                }
            }

            if (roadEncounters < 4)
            {
                Location b = getLocationByMem(0x8634);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (roadEncounters < 3)
            {
                Location b = getLocationByMem(0x8633);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }
            if (roadEncounters < 2)
            {
                Location b = getLocationByMem(0x8632);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (roadEncounters < 1)
            {
                Location b = getLocationByMem(0x8631);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }
        }

        private void drawRiver()
        {
            //draw a river

            int dirr = hy.R.Next(4);
            int dirr2 = dirr;
            while (dirr == dirr2)
            {
                dirr2 = hy.R.Next(4);
            }
            Location lr = null;
            Location lr2 = null;
            if (dirr == 0) //start north
            {
                int startx = hy.R.Next(MAP_COLS);
                lr = new Location();
                lr.Xpos = startx;
                lr.Ypos = 30;
            }
            else if (dirr == 1) //start east
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr = new Location();
                lr.Ypos = startx + 30;
                lr.Xpos = MAP_COLS - 1;
            }
            else if (dirr == 2) //start south
            {
                int startx = hy.R.Next(MAP_COLS);
                lr = new Location();
                lr.Xpos = startx;
                lr.Ypos = MAP_ROWS - 1 + 30;
            }
            else //start west
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr = new Location();
                lr.Ypos = startx + 30;
                lr.Xpos = 0;
            }

            if (dirr2 == 0) //start north
            {
                int startx = hy.R.Next(MAP_COLS);
                lr2 = new Location();
                lr2.Xpos = startx;
                lr2.Ypos = 30;
            }
            else if (dirr2 == 1) //start east
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr2 = new Location();
                lr2.Ypos = startx + 30;
                lr2.Xpos = MAP_COLS - 1;
            }
            else if (dirr2 == 2) //start south
            {
                int startx = hy.R.Next(MAP_COLS);
                lr2 = new Location();
                lr2.Xpos = startx;
                lr2.Ypos = MAP_ROWS - 1 + 30;
            }
            else //start west
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr2 = new Location();
                lr2.Ypos = startx + 30;
                lr2.Xpos = 0;
            }


            map[lr.Ypos - 30, lr.Xpos] = terrain.walkablewater;
            map[lr2.Ypos - 30, lr.Xpos] = terrain.walkablewater;
            drawLine(lr, lr2, terrain.walkablewater, walkable[hy.R.Next(walkable.Length)]);


            if (bridgeCount < 2)
            {
                Location b = getLocationByMem(0x8636);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (bridgeCount < 1)
            {
                Location b = getLocationByMem(0x8635);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }
        }

        private void drawVod()
        {
            //place gp and valley of death
            int gpx = hy.R.Next(MAP_COLS - 6) + 3;
            int gpy = hy.R.Next((MAP_ROWS / 2) + 3, MAP_ROWS - 3);

            map[gpy + 1, gpx] = terrain.lava;
            map[gpy - 1, gpx] = terrain.lava;
            map[gpy + 1, gpx + 1] = terrain.lava;
            map[gpy - 1, gpx - 1] = terrain.lava;
            map[gpy + 1, gpx - 1] = terrain.lava;
            map[gpy - 1, gpx + 1] = terrain.lava;
            map[gpy, gpx + 1] = terrain.lava;
            map[gpy, gpx - 1] = terrain.lava;

            map[gpy + 2, gpx + 2] = terrain.mountain;
            map[gpy + 1, gpx + 2] = terrain.mountain;
            map[gpy, gpx + 2] = terrain.mountain;
            map[gpy - 1, gpx + 2] = terrain.mountain;
            map[gpy - 2, gpx + 2] = terrain.mountain;
            map[gpy - 2, gpx + 1] = terrain.mountain;
            map[gpy - 2, gpx] = terrain.mountain;
            map[gpy - 2, gpx - 1] = terrain.mountain;
            map[gpy - 2, gpx - 2] = terrain.mountain;
            map[gpy - 1, gpx - 2] = terrain.mountain;
            map[gpy, gpx - 2] = terrain.mountain;
            map[gpy + 1, gpx - 2] = terrain.mountain;
            map[gpy + 2, gpx - 2] = terrain.mountain;
            map[gpy + 2, gpx - 1] = terrain.mountain;
            map[gpy + 2, gpx] = terrain.mountain;
            map[gpy + 2, gpx + 1] = terrain.mountain;

            Location from = getLocationByMem(0x8665);
            from.Xpos = gpx;
            from.Ypos = gpy + 30;
            from.CanShuffle = false;
            Location to = new Location();

            double d = 26;
            while (d > 25)
            {
                to.Xpos = hy.R.Next(MAP_COLS - 6) + 3;
                to.Ypos = hy.R.Next((MAP_ROWS / 2) + 5, MAP_ROWS - 5) + 30;
                d = computeDistance(from, to);
            }
            int encounterCount = 0;

            int x = from.Xpos;
            int y = from.Ypos - 30;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    bool first = true;
                    while (Math.Abs(move) > 0 && x != to.Xpos)
                    {
                        Location curr3 = new Location();
                        curr3.Xpos = x;
                        curr3.Ypos = y + 30;

                        map[y, x] = terrain.lava;
                        if (computeDistance(curr3, to) < .75 * d && move > 1 && encounterCount == 0 && !first)
                        {
                            Location b = getLocationByMem(0x864F);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .5 * d && move > 1 && encounterCount == 1 && !first)
                        {
                            Location b = getLocationByMem(0x864E);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .5 * d && move > 1 && encounterCount == 2 && !first)
                        {
                            Location b = getLocationByMem(0x864D);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }

                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] != terrain.lava)
                                {
                                    map[i, j] = terrain.mountain;
                                }

                            }
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                        first = false;
                    }
                }
                else
                {
                    int diff = to.Ypos - y - 30;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    bool first = true;
                    while (Math.Abs(move) > 0 && y != to.Ypos - 30)
                    {
                        Location curr3 = new Location();
                        curr3.Xpos = x;
                        curr3.Ypos = y + 30;

                        map[y, x] = terrain.lava;
                        if (computeDistance(curr3, to) < .75 * d && move > 1 && encounterCount == 0 && !first)
                        {
                            Location b = getLocationByMem(0x864F);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .5 * d && move > 1 && encounterCount == 1 && !first)
                        {
                            Location b = getLocationByMem(0x864E);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        else if (computeDistance(curr3, to) < .25 * d && move > 1 && encounterCount == 2 && !first)
                        {
                            Location b = getLocationByMem(0x864D);
                            b.Xpos = x;
                            b.Ypos = y + 30;
                            encounterCount++;
                            b.CanShuffle = false;
                        }
                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] != terrain.lava)
                                {
                                    map[i, j] = terrain.mountain;
                                }

                            }
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if (y > 0)
                        {
                            y--;
                        }
                        move--;
                        first = false;
                    }
                }
            }
            if (encounterCount < 3)
            {
                Location b = getLocationByMem(0x864D);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (encounterCount < 2)
            {
                Location b = getLocationByMem(0x864E);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (encounterCount < 1)
            {
                Location b = getLocationByMem(0x864F);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            terrain t5 = walkable[hy.R.Next(walkable.Length)];
            map[to.Ypos - 30, to.Xpos] = t5;
            if (map[to.Ypos - 30, to.Xpos + 1] == terrain.lava || map[to.Ypos - 30, to.Xpos - 1] == terrain.lava) //moving left/right
            {
                map[to.Ypos - 30 - 1, to.Xpos] = t5;
                map[to.Ypos - 30 + 1, to.Xpos] = t5;
            }
            else
            {
                map[to.Ypos - 30, to.Xpos - 1] = t5;
                map[to.Ypos - 30, to.Xpos + 1] = t5;
            }

            map[gpy, gpx] = terrain.palace;
        }


        private void drawRaft(bool left)
        {
            Console.WriteLine(left);
            int rafty = hy.R.Next(0, MAP_ROWS);
            int raftx = 0;
            if (!left)
            {
                raftx = MAP_COLS - 1;
                while (map[rafty, raftx] != terrain.walkablewater)
                {
                    rafty = hy.R.Next(0, MAP_ROWS);
                }
                while (map[rafty, raftx] == terrain.walkablewater && raftx < MAP_COLS - 1)
                {
                    raftx++;
                }
            }
            else
            {
                while (map[rafty, raftx] != terrain.walkablewater)
                {
                    rafty = hy.R.Next(0, MAP_ROWS);
                }
                while (map[rafty, raftx] == terrain.walkablewater && raftx > 0)
                {
                    raftx--;
                }
            }

            int tries = 0;
            while (!walkable.Contains(map[rafty, raftx]))
            {
                tries++;
                if(tries > 100)
                {
                    //terraform();
                    palace4.Xpos = 0;
                    palace4.Ypos = 0;
                    return;
                }
                rafty = hy.R.Next(0, MAP_ROWS);
                raftx = 0;
                if (!left)
                {
                    raftx = MAP_COLS - 1;
                    while (map[rafty, raftx] != terrain.walkablewater)
                    {
                        rafty = hy.R.Next(0, MAP_ROWS);
                    }
                    while (map[rafty, raftx] == terrain.walkablewater && raftx > 0)
                    {
                        raftx--;
                    }
                }
                else
                {
                    while (map[rafty, raftx] != terrain.walkablewater)
                    {
                        rafty = hy.R.Next(0, MAP_ROWS);
                    }
                    while (map[rafty, raftx] == terrain.walkablewater && raftx < MAP_COLS - 1)
                    {
                        raftx++;
                    }
                }
            }

            if (left)
            {
                map[rafty, raftx] = terrain.bridge;
                start.Xpos = raftx;
                start.Ypos = rafty + 30;
            }
            else
            {
                map[rafty, raftx] = terrain.bridge;
                palace4.Xpos = raftx;
                palace4.Ypos = rafty + 30;
                palace4.PassThrough = 0;
                for (int i = raftx + 1; i < MAP_COLS; i++)
                {
                    map[rafty, i] = terrain.bridge;
                }
            }
            
        }
    }

    
}
