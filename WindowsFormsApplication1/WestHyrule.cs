﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Z2Randomizer
{
    class WestHyrule : World
    {
        public Location start;
        public Location hammerEnter;
        public Location hammerExit;
        public Location fairy;
        public Location bagu;
        public Location jump;
        public Location medCave;
        public Location trophyCave;
        public Location raftSpot;
        public Location palace1;
        public Location palace2;
        public Location palace3;
        public Location jar;
        public Location heart1;
        public Location heart2;
        public Location lifeNorth;
        public Location lifeSouth;
        public Location shieldTown;
        public Location bridge1;
        public Location bridge2;
        public Location pbagCave;
        private int bridgeCount;

        private Dictionary<Location, Location> bridgeConn;
        private Dictionary<Location, Location> cityConn;
        private Dictionary<Location, Location> caveConn;
        private Dictionary<Location, Location> graveConn;

        private readonly SortedDictionary<int, terrain> terrains = new SortedDictionary<int, terrain>
            {
                { 0x462F, terrain.palace},
                { 0x4630,  terrain.cave },
                { 0x4631, terrain.forest},
                { 0x4632, terrain.cave },
                { 0x4633, terrain.forest },
                { 0x4634, terrain.grass },
                { 0x4635, terrain.forest },
                { 0x4636, terrain.road },
                { 0x4637, terrain.swamp },
                { 0x4638, terrain.grave },
                { 0x4639, terrain.cave },
                { 0x463A, terrain.cave },
                { 0x463B, terrain.cave },
                { 0x463C, terrain.cave },
                { 0x463D, terrain.cave },
                { 0x463E, terrain.cave },
                { 0x463F, terrain.cave },
                { 0x4640, terrain.grave },
                { 0x4641, terrain.cave },
                { 0x4642, terrain.bridge },
                { 0x4643, terrain.bridge },
                { 0x4644, terrain.bridge },
                { 0x4645, terrain.bridge },
                { 0x4646, terrain.forest },
                { 0x4647, terrain.swamp },
                { 0x4648, terrain.forest },
                { 0x4649, terrain.forest },
                { 0x464A, terrain.forest },
                { 0x464B, terrain.forest },
                { 0x464C, terrain.forest },
                { 0x464D, terrain.road },
                //{ 0x464E, terrain.desert },
                { 0x464F, terrain.desert },
                { 0x4658, terrain.bridge },
                { 0x4659, terrain.cave },
                { 0x465A, terrain.cave },
                { 0x465B, terrain.grave },
                { 0x465C, terrain.town },
                { 0x465E, terrain.town },
                { 0x465F, terrain.town },
                { 0x4660, terrain.town },
                { 0x4661, terrain.forest },
                { 0x4662, terrain.town },
                { 0x4663, terrain.palace },
                { 0x4664, terrain.palace },
                { 0x4665, terrain.palace }
        };

        public WestHyrule(Hyrule hy)
            : base(hy)
        {
            loadLocations(0x462F, 31, terrains);
            loadLocations(0x464F, 1, terrains);
            loadLocations(0x4658, 5, terrains);
            loadLocations(0x465E, 8, terrains);
            start = getLocationByMap(0x80, 0x00);
            reachableAreas = new HashSet<string>();
            hammerEnter = getLocationByMap(0x2A, 1);
            hammerExit = getLocationByMap(0x2B, 1);
            Location jumpCave = getLocationByMap(9, 0);
            jumpCave.Needjump = true;
            medCave = getLocationByMap(0x0E, 0);
            Location heartCave = getLocationByMap(0x10, 0);
            Location fairyCave = getLocationByMap(0x12, 0);
            fairyCave.NeedFairy = true;
            jump = getLocationByMap(0xC5, 4);
            bagu = getLocationByMap(0x18, 4);
            fairy = getLocationByMap(0xCB, 4);
            lifeNorth = getLocationByMap(0xC8, 4);
            lifeSouth = getLocationByMap(0x06, 4);
            lifeNorth.NeedBagu = true;
            lifeSouth.NeedBagu = true;
            trophyCave = getLocationByMap(0xE1, 0);
            raftSpot = getLocationByMem(0x4658);
            palace1 = getLocationByMem(0x4663);
            palace1.PalNum = 1;
            palace2 = getLocationByMem(0x4664);
            palace2.PalNum = 2;
            palace3 = getLocationByMem(0x4665);
            palace3.PalNum = 3;
            jar = getLocationByMem(0x4632);
            heart1 = getLocationByMem(0x463F);
            heart2 = getLocationByMem(0x4634);
            shieldTown = getLocationByMem(0x465C);
            pbagCave = getLocationByMem(0x463D);


            Location parapaCave1 = getLocationByMap(07, 0);
            Location parapaCave2 = getLocationByMap(0xC7, 0);
            Location jumpCave2 = getLocationByMap(0xCB, 0);
            Location fairyCave2 = getLocationByMap(0xD3, 0);
            bridge1 = getLocationByMap(0x04, 0);
            bridge2 = getLocationByMap(0xC5, 0);

            caveConn = new Dictionary<Location, Location>();
            bridgeConn = new Dictionary<Location, Location>();
            cityConn = new Dictionary<Location, Location>();
            graveConn = new Dictionary<Location, Location>();

            //connections.Add(hammerEnter, hammerExit);
            //connections.Add(hammerExit, hammerEnter);
            //caveConn.Add(hammerEnter, hammerExit);
            //caveConn.Add(hammerExit, hammerEnter);
            connections.Add(parapaCave1, parapaCave2);
            connections.Add(parapaCave2, parapaCave1);
            caveConn.Add(parapaCave1, parapaCave2);
            caveConn.Add(parapaCave2, parapaCave1);
            connections.Add(jumpCave, jumpCave2);
            connections.Add(jumpCave2, jumpCave);
            caveConn.Add(jumpCave, jumpCave2);
            caveConn.Add(jumpCave2, jumpCave);
            connections.Add(fairyCave, fairyCave2);
            connections.Add(fairyCave2, fairyCave);
            caveConn.Add(fairyCave2, fairyCave);
            graveConn.Add(fairyCave, fairyCave2);
            connections.Add(lifeNorth, lifeSouth);
            connections.Add(lifeSouth, lifeNorth);
            cityConn.Add(lifeSouth, lifeNorth);
            cityConn.Add(lifeNorth, lifeSouth);
            connections.Add(bridge1, bridge2);
            connections.Add(bridge2, bridge1);
            bridgeConn.Add(bridge1, bridge2);
            bridgeConn.Add(bridge2, bridge1);

            enemies = new List<int> { 3, 4, 5, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32 };
            flyingEnemies = new List<int> { 0x06, 0x07, 0x0A, 0x0D, 0x0E };
            generators = new List<int> { 11, 12, 15, 29 };
            shorties = new List<int> { 3, 4, 5, 17, 18, 0x1C, 0x1F };
            tallGuys = new List<int> { 0x20, 20, 21, 22, 23, 24, 25, 26, 27 };
            enemyAddr = 0x48B0;
            enemyPtr = 0x45B1;

            overworldMaps = new List<int>() { 0x22, 0x1D, 0x27, 0x30, 0x23, 0x3A, 0x1E, 0x35, 0x28 };
            MAP_ROWS = 75;
            MAP_COLS = 64;
    }

        public bool terraform()
        {
            bcount = 900;
            while (bcount > 799)
            {
                map = new terrain[MAP_ROWS, MAP_COLS];

                for (int i = 0; i < MAP_ROWS; i++)
                {
                    for (int j = 0; j < MAP_COLS; j++)
                    {
                        map[i, j] = terrain.none;
                    }
                }

                drawRoad();
                drawMountains();
                drawBridge();
                drawRiver();
               
                drawOcean(false);
                placeLocations();
                placeRocks();
                placeRandomTerrain(5);

                if(!growTerrain())
                {
                    return false;
                }

                drawRaft();


                

                //check bytes and adjust
                writeBytes(false, 0x506C, 799, 0, 0);
                Console.WriteLine("West:" + bcount);
            }
            writeBytes(true, 0x506C, 800, 0, 0);

            int loc3 = 0x7C00 + bcount;
            int high = (loc3 & 0xFF00) >> 8;
            int low = loc3 & 0xFF;

            hy.ROMData.put(0x479F, (Byte)low);
            hy.ROMData.put(0x47A0, (Byte)high);
            hy.ROMData.put(0x47A1, (Byte)low);
            hy.ROMData.put(0x47A2, (Byte)high);
            hy.ROMData.put(0x47A3, (Byte)low);
            hy.ROMData.put(0x47A4, (Byte)high);

            v = new bool[MAP_ROWS, MAP_COLS];
            for(int i = 0; i < MAP_ROWS; i++)
            {
                for(int j = 0; j < MAP_COLS; j++)
                {
                    v[i, j] = false;
                }
            }

            v[start.Ypos - 30, start.Xpos] = true;
            return true;
        }

        public void setStart()
        {
            v[start.Ypos - 30, start.Xpos] = true;
        }

        private void placeRocks()
        {
            int rockNum = hy.R.Next(3);
            while (rockNum > 0)
            {
                Location cave = Caves[hy.R.Next(Caves.Count)];
                if(!connections.Keys.Contains(cave) && cave != hammerEnter && cave != hammerExit)
                {
                    if(map[cave.Ypos - 30, cave.Xpos - 1] != terrain.mountain)
                    {
                        map[cave.Ypos - 30, cave.Xpos - 1] = terrain.rock;
                        rockNum--;
                    }
                    else if (map[cave.Ypos - 30, cave.Xpos + 1] != terrain.mountain)
                    {
                        map[cave.Ypos - 30, cave.Xpos + 1] = terrain.rock;
                        rockNum--;
                    }
                    else if (map[cave.Ypos - 29, cave.Xpos] != terrain.mountain)
                    {
                        map[cave.Ypos - 29, cave.Xpos] = terrain.rock;
                        rockNum--;
                    }
                    else if (map[cave.Ypos - 31, cave.Xpos] != terrain.mountain)
                    {
                        map[cave.Ypos - 31, cave.Xpos] = terrain.rock;
                        rockNum--;
                    }
                }
            }
        }

        private void drawRiver()
        {
            //draw a river

            int dirr = hy.R.Next(4);
            int dirr2 = dirr;
            while (dirr == dirr2)
            {
                dirr2 = hy.R.Next(4);
            }
            Location lr = null;
            Location lr2 = null;
            if (dirr == 0) //start north
            {
                int startx = hy.R.Next(MAP_COLS);
                lr = new Location();
                lr.Xpos = startx;
                lr.Ypos = 30;
            }
            else if (dirr == 1) //start east
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr = new Location();
                lr.Ypos = startx + 30;
                lr.Xpos = MAP_COLS - 1;
            }
            else if (dirr == 2) //start south
            {
                int startx = hy.R.Next(MAP_COLS);
                lr = new Location();
                lr.Xpos = startx;
                lr.Ypos = MAP_ROWS - 1 + 30;
            }
            else //start west
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr = new Location();
                lr.Ypos = startx + 30;
                lr.Xpos = 0;
            }

            if (dirr2 == 0) //start north
            {
                int startx = hy.R.Next(MAP_COLS);
                lr2 = new Location();
                lr2.Xpos = startx;
                lr2.Ypos = 30;
            }
            else if (dirr2 == 1) //start east
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr2 = new Location();
                lr2.Ypos = startx + 30;
                lr2.Xpos = MAP_COLS - 1;
            }
            else if (dirr2 == 2) //start south
            {
                int startx = hy.R.Next(MAP_COLS);
                lr2 = new Location();
                lr2.Xpos = startx;
                lr2.Ypos = MAP_ROWS - 1 + 30;
            }
            else //start west
            {
                int startx = hy.R.Next(MAP_ROWS);
                lr2 = new Location();
                lr2.Ypos = startx + 30;
                lr2.Xpos = 0;
            }

            map[lr.Ypos - 30, lr.Xpos] = terrain.walkablewater;
            map[lr2.Ypos - 30, lr.Xpos] = terrain.walkablewater;
            drawLine(lr, lr2, terrain.walkablewater, walkable[hy.R.Next(walkable.Length)]);

            if (bridgeCount < 2)
            {
                Location b = getLocationByMem(0x4643);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }

            if (bridgeCount < 1)
            {
                Location b = getLocationByMem(0x4642);
                b.Xpos = 0;
                b.Ypos = 0;
                b.Reachable = true;
                b.CanShuffle = false;
            }
        }

        private void drawBridge()
        {
            int bridgex = hy.R.Next(1, MAP_COLS - 10);
            int bridgey = hy.R.Next(1, MAP_ROWS - 10) + 30;

            bridge1.Xpos = bridgex;
            bridge1.Ypos = bridgey;

            int bdiff = 0;
            terrain wt = walkable[hy.R.Next(walkable.Length)];
            terrain wt2 = walkable[hy.R.Next(walkable.Length)];
            if (hy.R.NextDouble() > .5)
            {

                bdiff = hy.R.Next(6, 9);
                map[bridge1.Ypos - 1 - 30, bridge1.Xpos] = wt;
                map[bridge1.Ypos - 1 - 30, bridge1.Xpos - 1] = wt;
                map[bridge1.Ypos - 1 - 30, bridge1.Xpos + 1] = wt;
                map[bridge1.Ypos + 1 + bdiff - 30, bridge1.Xpos] = wt2;
                map[bridge1.Ypos + 1 + bdiff - 30, bridge1.Xpos - 1] = wt2;
                map[bridge1.Ypos + 1 + bdiff - 30, bridge1.Xpos + 1] = wt2;

                bridge2.Xpos = bridge1.Xpos;
                bridge2.Ypos = bridge1.Ypos + bdiff;
            }
            else
            {

                bdiff = hy.R.Next(6, 9);
                map[bridge1.Ypos - 30, bridge1.Xpos - 1] = wt;
                map[bridge1.Ypos - 30 - 1, bridge1.Xpos - 1] = wt;
                map[bridge1.Ypos - 30 + 1, bridge1.Xpos - 1] = wt;
                map[bridge1.Ypos - 30, bridge1.Xpos + bdiff + 1] = wt2;
                map[bridge1.Ypos - 30 - 1, bridge1.Xpos + bdiff + 1] = wt2;
                map[bridge1.Ypos - 30 + 1, bridge1.Xpos + bdiff + 1] = wt2;

                bridge2.Xpos = bridge1.Xpos + bdiff;
                bridge2.Ypos = bridge1.Ypos;
            }
            map[bridge1.Ypos - 30, bridge1.Xpos] = terrain.bridge;
            map[bridge2.Ypos - 30, bridge2.Xpos] = terrain.bridge;
            drawLine(bridge2, bridge1, terrain.bridge, terrain.walkablewater);
            bridge2.CanShuffle = false;
            bridge1.CanShuffle = false;
        }

        private void drawMountains()
        {
            //create some mountains
            int mounty = hy.R.Next(22, 42);
            map[mounty, 0] = terrain.mountain;
            bool placedRoad = false;


            int endmounty = hy.R.Next(22, 42);
            int endmountx = hy.R.Next(2, 8);
            int x2 = 0;
            int y2 = mounty;
            int placedRocks = 0;
            while (x2 != (MAP_COLS - endmountx) || y2 != endmounty)
            {
                if (Math.Abs(x2 - (MAP_COLS - endmountx)) >= Math.Abs(y2 - endmounty))
                {
                    if (x2 > MAP_COLS - endmountx && x2 > 0)
                    {
                        x2--;
                    }
                    else if (x2 < MAP_COLS - 1)
                    {
                        x2++;
                    }
                }
                else
                {
                    if (y2 > endmounty && y2 > 0)
                    {
                        y2--;
                    }
                    else if (y2 < MAP_ROWS - 1)
                    {
                        y2++;
                    }
                }
                if (x2 != MAP_COLS - endmountx || y2 != endmounty)
                {
                    if (map[y2, x2] == terrain.none)
                    {
                        map[y2, x2] = terrain.mountain;
                    }
                    else
                    {
                        if (!placedRoad && map[y2, x2 + 1] != terrain.road)
                        {
                            if (hy.R.NextDouble() > .5 && (x2 > 0 && map[y2, x2 - 1] != terrain.rock) && (x2 < MAP_COLS - 1 && map[y2, x2 + 1] != terrain.rock) && (((y2 > 0 && map[y2 - 1, x2] == terrain.road) && (y2 < MAP_ROWS - 1 && map[y2 + 1, x2] == terrain.road)) || ((x2 > 0 && map[y2, x2 - 0] == terrain.road) && (x2 < MAP_COLS - 1 && map[y2, x2 + 1] == terrain.road))))
                            {
                                Location roadEnc = getLocationByMem(0x4636);
                                roadEnc.Xpos = x2;
                                roadEnc.Ypos = y2 + 30;
                                roadEnc.CanShuffle = false;
                                roadEnc.Reachable = true;
                                placedRoad = true;
                            }
                            else if (placedRocks < 1)
                            {
                                Location roadEnc = getLocationByMem(0x4636);
                                if ((roadEnc.Ypos - 30 != y2 && roadEnc.Xpos - 1 != x2) && (roadEnc.Ypos - 30 + 1 != y2 && roadEnc.Xpos != x2) && (roadEnc.Ypos - 30 - 1 != y2 && roadEnc.Xpos != x2) && (roadEnc.Ypos - 30 != y2 && roadEnc.Xpos + 1 != x2))
                                {
                                    map[y2, x2] = terrain.rock;
                                    placedRocks++;
                                }
                            }
                        }
                        else if (placedRocks < 1)
                        {

                            map[y2, x2] = terrain.rock;
                            placedRocks++;
                        }
                    }
                }
            }

            if (!placedRoad)
            {
                Location roadEnc = getLocationByMem(0x4636);
                roadEnc.Xpos = 0;
                roadEnc.Ypos = 0;
                roadEnc.CanShuffle = false;
            }
        }

        private void drawRoad()
        {
            int roadType = hy.R.Next(3);
            if(roadType == 0) //two vertical
            {
                Location rl = new Location();
                rl.Xpos = hy.R.Next(MAP_COLS / 2);
                rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                Location r2 = new Location();
                r2.Xpos = hy.R.Next(MAP_COLS / 2);
                r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                drawLine(rl, r2, terrain.road);
                rl.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                drawLine(rl, r2, terrain.road);
            }
            else if(roadType == 1)//two horizontal
            {
                Location rl = new Location();
                rl.Xpos = hy.R.Next(MAP_COLS / 2);
                rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                Location r2 = new Location();
                r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                r2.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                drawLine(rl, r2, terrain.road);
                rl.Xpos = hy.R.Next(MAP_COLS / 2);
                rl.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                drawLine(rl, r2, terrain.road);
            }
            else //one of each
            {
                if(hy.R.Next() > .5) //north horizontal
                {
                    Location rl = new Location();
                    rl.Xpos = hy.R.Next(MAP_COLS / 2);
                    rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                    map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                    Location r2 = new Location();
                    r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                    r2.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                    map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                    drawLine(rl, r2, terrain.road);
                }
                else
                {
                    Location rl = new Location();
                    Location r2 = new Location();
                    map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                    drawLine(rl, r2, terrain.road);
                    rl.Xpos = hy.R.Next(MAP_COLS / 2);
                    rl.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                    r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
                    r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                    map[rl.Ypos - 30, rl.Xpos] = terrain.road;
                    map[r2.Ypos - 30, r2.Xpos] = terrain.road;
                    drawLine(rl, r2, terrain.road);
                }
                Location r3 = new Location();
                Location r4 = new Location();
                r3.Xpos = hy.R.Next(MAP_COLS);
                r3.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
                r4.Xpos = hy.R.Next(MAP_COLS);
                r4.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
                map[r3.Ypos - 30, r3.Xpos] = terrain.road;
                map[r4.Ypos - 30, r4.Xpos] = terrain.road;
                drawLine(r3, r4, terrain.road);

            }
            ////Draw road structure
            //int startRoad = hy.R.Next(4);
            //Location rl = new Location();
            //if (startRoad == 0) //northwest
            //{
            //    rl.Xpos = hy.R.Next(MAP_COLS / 2);
            //    rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
            //}
            //else if (startRoad == 1) //northeast
            //{
            //    rl.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
            //    rl.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
            //}
            //else if (startRoad == 2) //southeast
            //{
            //    rl.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
            //    rl.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
            //}
            //else //southwest
            //{
            //    rl.Xpos = hy.R.Next(MAP_COLS / 2);
            //    rl.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
            //}
            //map[rl.Ypos - 30, rl.Xpos] = terrain.road;
            //bool clockwise = hy.R.NextDouble() > .5;

            //for (int i = 0; i < 3; i++)
            //{
            //    if (clockwise)
            //    {
            //        startRoad++;
            //    }
            //    else
            //    {
            //        startRoad--;
            //    }
            //    if (startRoad > 3)
            //    {
            //        startRoad = 0;
            //    }
            //    if (startRoad < 0)
            //    {
            //        startRoad = 3;
            //    }
            //    Location r2 = new Location();
            //    if (startRoad == 0) //northwest
            //    {
            //        r2.Xpos = hy.R.Next(MAP_COLS / 2);
            //        r2.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
            //    }
            //    else if (startRoad == 1) //northeast
            //    {
            //        r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
            //        r2.Ypos = hy.R.Next(MAP_ROWS / 2) + 30;
            //    }
            //    else if (startRoad == 2) //southeast
            //    {
            //        r2.Xpos = hy.R.Next(MAP_COLS / 2, MAP_COLS);
            //        r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
            //    }
            //    else //southwest
            //    {
            //        r2.Xpos = hy.R.Next(MAP_COLS / 2);
            //        r2.Ypos = hy.R.Next(MAP_ROWS / 2, MAP_ROWS) + 30;
            //    }
            //    map[rl.Ypos - 30, rl.Xpos] = terrain.road;
            //    map[r2.Ypos - 30, r2.Xpos] = terrain.road;
            //    drawLine(rl, r2, terrain.road);
            //    rl = r2;
            //}
        }

        public void updateVisit()
        {
            updateReachable();

            foreach(Location l in AllLocations)
            {
                if (l.Ypos > 30)
                {
                    if (v[l.Ypos - 30, l.Xpos])
                    {
                        l.Reachable = true;
                        if (connections.Keys.Contains(l))
                        {
                            Location l2 = connections[l];
                            if ((l.NeedBagu && (bagu.Reachable || hy.spellGet[(int)spells.fairy])))
                            {
                                l2.Reachable = true;
                                v[l2.Ypos - 30, l2.Xpos] = true;
                            }

                            if (l.NeedFairy && hy.spellGet[(int)spells.fairy])
                            {
                                l2.Reachable = true;
                                v[l2.Ypos - 30, l2.Xpos] = true;
                            }

                            if (l.Needjump && hy.spellGet[(int)spells.jump])
                            {
                                l2.Reachable = true;
                                v[l2.Ypos - 30, l2.Xpos] = true;
                            }

                            if (!l.NeedFairy && !l.NeedBagu && !l.Needjump)
                            {
                                l2.Reachable = true;
                                v[l2.Ypos - 30, l2.Xpos] = true;
                            }
                        }
                    }
                }
            }
            if (lifeNorth.Reachable && lifeNorth.townNum == 8)
            {
                lifeSouth.Reachable = true;
            }
        }

        private void drawLine(Location to, Location from, terrain t)
        {
            int x = from.Xpos;
            int y = from.Ypos - 30;
            while(x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if ((x != to.Xpos || y != (to.Ypos - 30)))
                        {
                            map[y, x] = t;
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                    }
                }
                else
                {
                    int diff = to.Ypos - 30 - y;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if ((x != to.Xpos || y != (to.Ypos - 30)))
                        {
                            map[y, x] = t;
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if (y > 0)
                        {
                            y--;
                        }
                        move--;
                    }
                }
            }
        }

        private void drawLine(Location to, Location from, terrain t, terrain fill)
        {
            int x = from.Xpos;
            int y = from.Ypos - 30;
            bridgeCount = 0;
            while (x != to.Xpos || y != (to.Ypos - 30))
            {
                if (hy.R.NextDouble() > .5 && x != to.Xpos)
                {
                    int diff = to.Xpos - x;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while(Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if (t == terrain.walkablewater && map[y, x] == terrain.road)
                        {
                            if (bridgeCount == 0 && move > 1 && (diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x4642);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                                b.CanShuffle = false;
                            }
                            else if (bridgeCount == 1 && move > 1 && (diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x4643);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                                b.CanShuffle = false;
                            }
                            else if((diff > 0 && (map[y, x + 1] == terrain.none || map[y, x + 1] == terrain.mountain)) || (diff < 0 && (map[y, x - 1] == terrain.none || map[y, x - 1] == terrain.mountain)))
                            {
                                map[y, x] = terrain.bridge;
                            }
                            else 
                            {
                                map[y, x] = t;
                            }
                        }
                        else if(map[y, x] != terrain.bridge)
                        {
                            map[y, x] = t;
                        }
                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = fill;
                                }

                            }
                        }
                        if (diff > 0 && x < MAP_COLS - 1)
                        {
                            x++;
                        }
                        else if (x > 0)
                        {
                            x--;
                        }
                        move--;
                    }
                }
                else
                {
                    int diff = to.Ypos - y - 30;
                    int move = hy.R.Next(Math.Abs(diff) + 1);
                    while (Math.Abs(move) > 0 && !(x == to.Xpos && y == to.Ypos - 30))
                    {
                        if (t == terrain.walkablewater && map[y, x] == terrain.road)
                        {
                            if (bridgeCount == 0 && move > 1 && (diff > 0 && (map[y+ 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x4642);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                bridgeCount++;
                                b.CanShuffle = false;
                                map[y, x] = terrain.bridge;
                            }
                            else if (bridgeCount == 1 && move > 1 && (diff > 0 && (map[y + 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                Location b = getLocationByMem(0x4643);
                                b.Xpos = x;
                                b.Ypos = y + 30;
                                b.CanShuffle = false;
                                bridgeCount++;
                                map[y, x] = terrain.bridge;
                            }
                            else if ((diff > 0 && (map[y + 1, x] == terrain.none || map[y + 1, x] == terrain.mountain)) || (diff < 0 && (map[y - 1, x] == terrain.none || map[y - 1, x] == terrain.mountain)))
                            {
                                map[y, x] = terrain.bridge;
                            }
                            else
                            {
                                map[y, x] = t;
                            }
                        }
                        else
                        {
                            map[y, x] = t;
                        }
                        for (int i = y - 1; i <= y + 1; i++)
                        {
                            for (int j = x - 1; j <= x + 1; j++)
                            {
                                if ((i != y || j != x) && i >= 0 && i < MAP_ROWS && j >= 0 && j < MAP_COLS && map[i, j] == terrain.none)
                                {
                                    map[i, j] = fill;
                                }

                            }
                        }
                        if (diff > 0 && y < MAP_ROWS - 1)
                        {
                            y++;
                        }
                        else if(y > 0)
                        {
                            y--;
                        }
                        move--;
                    }
                }
            }
        }

        private void drawRaft()
        {
            int rafty = hy.R.Next(0, MAP_ROWS);
            int raftx = MAP_COLS - 1;
            while (map[rafty, raftx] != terrain.walkablewater)
            {
                rafty = hy.R.Next(0, MAP_ROWS);
            }
            while (map[rafty, raftx] == terrain.walkablewater && raftx > 0)
            {
                raftx--;
            }

            int tries = 0;
            while (!walkable.Contains(map[rafty, raftx]))
            {
                tries++;
                if(tries > 100)
                {
                    terraform();
                    return;
                }
                rafty = hy.R.Next(0, MAP_ROWS);
                raftx = MAP_COLS - 1;
                while (map[rafty, raftx] != terrain.walkablewater)
                {
                    rafty = hy.R.Next(0, MAP_ROWS);
                }
                while (map[rafty, raftx] == terrain.walkablewater && raftx > 0)
                {
                    raftx--;
                }
            }
            map[rafty, raftx] = terrain.bridge;
            raftSpot.Xpos = raftx;
            raftSpot.Ypos = rafty + 30;
        }
    }
}
