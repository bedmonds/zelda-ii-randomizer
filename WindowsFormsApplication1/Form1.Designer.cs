﻿namespace Z2Randomizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.communityBox = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.maxHeartsBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.hintBox = new System.Windows.Forms.ComboBox();
            this.livesBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.techCmbo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.heartCmbo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.thunderBox = new System.Windows.Forms.CheckBox();
            this.spellBox = new System.Windows.Forms.CheckBox();
            this.reflectBox = new System.Windows.Forms.CheckBox();
            this.fireBox = new System.Windows.Forms.CheckBox();
            this.fairyBox = new System.Windows.Forms.CheckBox();
            this.lifeBox = new System.Windows.Forms.CheckBox();
            this.jumpBox = new System.Windows.Forms.CheckBox();
            this.shieldBox = new System.Windows.Forms.CheckBox();
            this.spellShuffleBox = new System.Windows.Forms.CheckBox();
            this.itemGrp = new System.Windows.Forms.GroupBox();
            this.keyBox = new System.Windows.Forms.CheckBox();
            this.hammerBox = new System.Windows.Forms.CheckBox();
            this.crossBox = new System.Windows.Forms.CheckBox();
            this.fluteBox = new System.Windows.Forms.CheckBox();
            this.bootsBox = new System.Windows.Forms.CheckBox();
            this.raftBox = new System.Windows.Forms.CheckBox();
            this.gloveBox = new System.Windows.Forms.CheckBox();
            this.candleBox = new System.Windows.Forms.CheckBox();
            this.shuffleItemBox = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.hideKasutoBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.hpCmbo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.p7Shuffle = new System.Windows.Forms.CheckBox();
            this.palaceSwapBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.allowPathEnemies = new System.Windows.Forms.CheckBox();
            this.shuffleEncounters = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.removeTbird = new System.Windows.Forms.CheckBox();
            this.gpBox = new System.Windows.Forms.CheckBox();
            this.upaBox = new System.Windows.Forms.CheckBox();
            this.palacePalette = new System.Windows.Forms.CheckBox();
            this.tbirdBox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numGemsCbo = new System.Windows.Forms.ComboBox();
            this.palaceRoomBox = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.combineFireBox = new System.Windows.Forms.CheckBox();
            this.lifeLabel = new System.Windows.Forms.Label();
            this.magLabel = new System.Windows.Forms.Label();
            this.atkLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lifeBar = new System.Windows.Forms.TrackBar();
            this.magicBar = new System.Windows.Forms.TrackBar();
            this.attackBar = new System.Windows.Forms.TrackBar();
            this.disableJarBox = new System.Windows.Forms.CheckBox();
            this.shuffleSpellLocationsBox = new System.Windows.Forms.CheckBox();
            this.lifeRefilBox = new System.Windows.Forms.CheckBox();
            this.expBox = new System.Windows.Forms.GroupBox();
            this.lifeExpNeeded = new System.Windows.Forms.CheckBox();
            this.magicExpNeeded = new System.Windows.Forms.CheckBox();
            this.shuffleAtkExp = new System.Windows.Forms.CheckBox();
            this.shuffleAllExp = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.shuffleDripper = new System.Windows.Forms.CheckBox();
            this.mixEnemies = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.shufflePalaceEnemies = new System.Windows.Forms.CheckBox();
            this.shuffleOverworldEnemies = new System.Windows.Forms.CheckBox();
            this.shuffleBossExp = new System.Windows.Forms.CheckBox();
            this.shuffleEnemyExp = new System.Windows.Forms.CheckBox();
            this.swordImmuneBox = new System.Windows.Forms.CheckBox();
            this.stealExpAmt = new System.Windows.Forms.CheckBox();
            this.stealExpBox = new System.Windows.Forms.CheckBox();
            this.shuffleEnemyHPBox = new System.Windows.Forms.CheckBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.shufflePbagExp = new System.Windows.Forms.CheckBox();
            this.spellItemBox = new System.Windows.Forms.CheckBox();
            this.pbagItemShuffleBox = new System.Windows.Forms.CheckBox();
            this.kasutoBox = new System.Windows.Forms.CheckBox();
            this.palaceKeys = new System.Windows.Forms.CheckBox();
            this.shuffleSmallItemsBox = new System.Windows.Forms.CheckBox();
            this.mixItemBox = new System.Windows.Forms.CheckBox();
            this.overworldItemBox = new System.Windows.Forms.CheckBox();
            this.palaceItemBox = new System.Windows.Forms.CheckBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.randoDrops = new System.Windows.Forms.CheckBox();
            this.standardDrops = new System.Windows.Forms.CheckBox();
            this.largeKey = new System.Windows.Forms.CheckBox();
            this.large1up = new System.Windows.Forms.CheckBox();
            this.large500 = new System.Windows.Forms.CheckBox();
            this.large200 = new System.Windows.Forms.CheckBox();
            this.large100 = new System.Windows.Forms.CheckBox();
            this.large50 = new System.Windows.Forms.CheckBox();
            this.largeRedJar = new System.Windows.Forms.CheckBox();
            this.largeBlueJar = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.smallKey = new System.Windows.Forms.CheckBox();
            this.small1up = new System.Windows.Forms.CheckBox();
            this.small500 = new System.Windows.Forms.CheckBox();
            this.small200 = new System.Windows.Forms.CheckBox();
            this.small100 = new System.Windows.Forms.CheckBox();
            this.small50 = new System.Windows.Forms.CheckBox();
            this.smallRedJar = new System.Windows.Forms.CheckBox();
            this.smallBlueJar = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.enemyDropBox = new System.Windows.Forms.CheckBox();
            this.pbagDrop = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.beamCmbo = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.shieldColor = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tunicColor = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.spriteCmbo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.disableMusicBox = new System.Windows.Forms.CheckBox();
            this.enemyPalette = new System.Windows.Forms.CheckBox();
            this.beamBox = new System.Windows.Forms.CheckBox();
            this.fastSpellBox = new System.Windows.Forms.CheckBox();
            this.jumpNormalbox = new System.Windows.Forms.CheckBox();
            this.disableLowHealthBeep = new System.Windows.Forms.CheckBox();
            this.fileTextBox = new System.Windows.Forms.TextBox();
            this.seedTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.seedBtn = new System.Windows.Forms.Button();
            this.fileBtn = new System.Windows.Forms.Button();
            this.generateBtn = new System.Windows.Forms.Button();
            this.flagBox = new System.Windows.Forms.TextBox();
            this.updateBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.wikiBtn = new System.Windows.Forms.Button();
            this.botBtn = new System.Windows.Forms.Button();
            this.megmetBtn = new System.Windows.Forms.Button();
            this.zoraBtn = new System.Windows.Forms.Button();
            this.wizzBtn = new System.Windows.Forms.Button();
            this.paraBtn = new System.Windows.Forms.Button();
            this.lizBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.customBox1 = new System.Windows.Forms.TextBox();
            this.customBox2 = new System.Windows.Forms.TextBox();
            this.customSave1 = new System.Windows.Forms.Button();
            this.customLoad1 = new System.Windows.Forms.Button();
            this.customBox3 = new System.Windows.Forms.TextBox();
            this.customSave2 = new System.Windows.Forms.Button();
            this.customLoad2 = new System.Windows.Forms.Button();
            this.customSave3 = new System.Windows.Forms.Button();
            this.customLoad3 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.itemGrp.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lifeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magicBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBar)).BeginInit();
            this.expBox.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(24, 173);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1024, 461);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.communityBox);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.maxHeartsBox);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.hintBox);
            this.tabPage4.Controls.Add(this.livesBox);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.techCmbo);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.heartCmbo);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Controls.Add(this.itemGrp);
            this.tabPage4.Location = new System.Drawing.Point(8, 39);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1008, 414);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Start Configuration";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // communityBox
            // 
            this.communityBox.AutoSize = true;
            this.communityBox.Location = new System.Drawing.Point(670, 333);
            this.communityBox.Name = "communityBox";
            this.communityBox.Size = new System.Drawing.Size(206, 29);
            this.communityBox.TabIndex = 20;
            this.communityBox.Text = "Community Hints";
            this.toolTip1.SetToolTip(this.communityBox, "When selected, will replace some text with hints submitted by the community");
            this.communityBox.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(664, 85);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(221, 25);
            this.label7.TabIndex = 19;
            this.label7.Text = "Max Heart Containers";
            // 
            // maxHeartsBox
            // 
            this.maxHeartsBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.maxHeartsBox.FormattingEnabled = true;
            this.maxHeartsBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "Random"});
            this.maxHeartsBox.Location = new System.Drawing.Point(670, 115);
            this.maxHeartsBox.Margin = new System.Windows.Forms.Padding(6);
            this.maxHeartsBox.Name = "maxHeartsBox";
            this.maxHeartsBox.Size = new System.Drawing.Size(238, 33);
            this.maxHeartsBox.TabIndex = 18;
            this.toolTip1.SetToolTip(this.maxHeartsBox, "The number of heart containers you start with");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(664, 251);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 25);
            this.label13.TabIndex = 17;
            this.label13.Text = "Hint Type";
            // 
            // hintBox
            // 
            this.hintBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hintBox.FormattingEnabled = true;
            this.hintBox.Items.AddRange(new object[] {
            "Normal",
            "Spell Item",
            "Helpful",
            "Spell + Helpful"});
            this.hintBox.Location = new System.Drawing.Point(670, 281);
            this.hintBox.Margin = new System.Windows.Forms.Padding(6);
            this.hintBox.Name = "hintBox";
            this.hintBox.Size = new System.Drawing.Size(238, 33);
            this.hintBox.TabIndex = 16;
            this.toolTip1.SetToolTip(this.hintBox, "Select what types of helpful hints are available");
            this.hintBox.SelectedIndexChanged += new System.EventHandler(this.hintBox_SelectedIndexChanged);
            // 
            // livesBox
            // 
            this.livesBox.AutoSize = true;
            this.livesBox.Location = new System.Drawing.Point(670, 379);
            this.livesBox.Margin = new System.Windows.Forms.Padding(6);
            this.livesBox.Name = "livesBox";
            this.livesBox.Size = new System.Drawing.Size(314, 29);
            this.livesBox.TabIndex = 15;
            this.livesBox.Text = "Randomize Number of Lives";
            this.toolTip1.SetToolTip(this.livesBox, "Start with anywhere from 2-5 lives");
            this.livesBox.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(664, 169);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 25);
            this.label5.TabIndex = 14;
            this.label5.Text = "Starting Techs";
            // 
            // techCmbo
            // 
            this.techCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.techCmbo.FormattingEnabled = true;
            this.techCmbo.Items.AddRange(new object[] {
            "None",
            "Downstab",
            "Upstab",
            "Both",
            "Random"});
            this.techCmbo.Location = new System.Drawing.Point(670, 199);
            this.techCmbo.Margin = new System.Windows.Forms.Padding(6);
            this.techCmbo.Name = "techCmbo";
            this.techCmbo.Size = new System.Drawing.Size(238, 33);
            this.techCmbo.TabIndex = 13;
            this.toolTip1.SetToolTip(this.techCmbo, "The sword techniques you start with");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(664, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(254, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Starting Heart Containers";
            // 
            // heartCmbo
            // 
            this.heartCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heartCmbo.FormattingEnabled = true;
            this.heartCmbo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "Random"});
            this.heartCmbo.Location = new System.Drawing.Point(672, 35);
            this.heartCmbo.Margin = new System.Windows.Forms.Padding(6);
            this.heartCmbo.Name = "heartCmbo";
            this.heartCmbo.Size = new System.Drawing.Size(238, 33);
            this.heartCmbo.TabIndex = 9;
            this.toolTip1.SetToolTip(this.heartCmbo, "The number of heart containers you start with");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.thunderBox);
            this.groupBox1.Controls.Add(this.spellBox);
            this.groupBox1.Controls.Add(this.reflectBox);
            this.groupBox1.Controls.Add(this.fireBox);
            this.groupBox1.Controls.Add(this.fairyBox);
            this.groupBox1.Controls.Add(this.lifeBox);
            this.groupBox1.Controls.Add(this.jumpBox);
            this.groupBox1.Controls.Add(this.shieldBox);
            this.groupBox1.Controls.Add(this.spellShuffleBox);
            this.groupBox1.Location = new System.Drawing.Point(338, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox1.Size = new System.Drawing.Size(320, 394);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                                        ";
            // 
            // thunderBox
            // 
            this.thunderBox.AutoSize = true;
            this.thunderBox.Location = new System.Drawing.Point(46, 346);
            this.thunderBox.Margin = new System.Windows.Forms.Padding(6);
            this.thunderBox.Name = "thunderBox";
            this.thunderBox.Size = new System.Drawing.Size(224, 29);
            this.thunderBox.TabIndex = 7;
            this.thunderBox.Text = "Start With Thunder";
            this.toolTip1.SetToolTip(this.thunderBox, "Start with thunder spell");
            this.thunderBox.UseVisualStyleBackColor = true;
            // 
            // spellBox
            // 
            this.spellBox.AutoSize = true;
            this.spellBox.Location = new System.Drawing.Point(46, 302);
            this.spellBox.Margin = new System.Windows.Forms.Padding(6);
            this.spellBox.Name = "spellBox";
            this.spellBox.Size = new System.Drawing.Size(192, 29);
            this.spellBox.TabIndex = 6;
            this.spellBox.Text = "Start With Spell";
            this.toolTip1.SetToolTip(this.spellBox, "Start with spell spell");
            this.spellBox.UseVisualStyleBackColor = true;
            // 
            // reflectBox
            // 
            this.reflectBox.AutoSize = true;
            this.reflectBox.Location = new System.Drawing.Point(46, 258);
            this.reflectBox.Margin = new System.Windows.Forms.Padding(6);
            this.reflectBox.Name = "reflectBox";
            this.reflectBox.Size = new System.Drawing.Size(211, 29);
            this.reflectBox.TabIndex = 5;
            this.reflectBox.Text = "Start With Reflect";
            this.toolTip1.SetToolTip(this.reflectBox, "Start with reflect spell");
            this.reflectBox.UseVisualStyleBackColor = true;
            // 
            // fireBox
            // 
            this.fireBox.AutoSize = true;
            this.fireBox.Location = new System.Drawing.Point(46, 213);
            this.fireBox.Margin = new System.Windows.Forms.Padding(6);
            this.fireBox.Name = "fireBox";
            this.fireBox.Size = new System.Drawing.Size(181, 29);
            this.fireBox.TabIndex = 4;
            this.fireBox.Text = "Start With Fire";
            this.toolTip1.SetToolTip(this.fireBox, "Start with fire spell");
            this.fireBox.UseVisualStyleBackColor = true;
            // 
            // fairyBox
            // 
            this.fairyBox.AutoSize = true;
            this.fairyBox.Location = new System.Drawing.Point(46, 169);
            this.fairyBox.Margin = new System.Windows.Forms.Padding(6);
            this.fairyBox.Name = "fairyBox";
            this.fairyBox.Size = new System.Drawing.Size(192, 29);
            this.fairyBox.TabIndex = 3;
            this.fairyBox.Text = "Start With Fairy";
            this.toolTip1.SetToolTip(this.fairyBox, "Start with fairy spell");
            this.fairyBox.UseVisualStyleBackColor = true;
            // 
            // lifeBox
            // 
            this.lifeBox.AutoSize = true;
            this.lifeBox.Location = new System.Drawing.Point(46, 125);
            this.lifeBox.Margin = new System.Windows.Forms.Padding(6);
            this.lifeBox.Name = "lifeBox";
            this.lifeBox.Size = new System.Drawing.Size(179, 29);
            this.lifeBox.TabIndex = 2;
            this.lifeBox.Text = "Start With Life";
            this.toolTip1.SetToolTip(this.lifeBox, "Start with life spell");
            this.lifeBox.UseVisualStyleBackColor = true;
            // 
            // jumpBox
            // 
            this.jumpBox.AutoSize = true;
            this.jumpBox.Location = new System.Drawing.Point(46, 81);
            this.jumpBox.Margin = new System.Windows.Forms.Padding(6);
            this.jumpBox.Name = "jumpBox";
            this.jumpBox.Size = new System.Drawing.Size(196, 29);
            this.jumpBox.TabIndex = 1;
            this.jumpBox.Text = "Start With Jump";
            this.toolTip1.SetToolTip(this.jumpBox, "Start with jump spell");
            this.jumpBox.UseVisualStyleBackColor = true;
            // 
            // shieldBox
            // 
            this.shieldBox.AutoSize = true;
            this.shieldBox.Location = new System.Drawing.Point(46, 37);
            this.shieldBox.Margin = new System.Windows.Forms.Padding(6);
            this.shieldBox.Name = "shieldBox";
            this.shieldBox.Size = new System.Drawing.Size(204, 29);
            this.shieldBox.TabIndex = 1;
            this.shieldBox.Text = "Start With Shield";
            this.toolTip1.SetToolTip(this.shieldBox, "Start with shield spell");
            this.shieldBox.UseVisualStyleBackColor = true;
            // 
            // spellShuffleBox
            // 
            this.spellShuffleBox.AutoSize = true;
            this.spellShuffleBox.Location = new System.Drawing.Point(12, 0);
            this.spellShuffleBox.Margin = new System.Windows.Forms.Padding(6);
            this.spellShuffleBox.Name = "spellShuffleBox";
            this.spellShuffleBox.Size = new System.Drawing.Size(256, 29);
            this.spellShuffleBox.TabIndex = 1;
            this.spellShuffleBox.Text = "Shuffle Starting Spells";
            this.toolTip1.SetToolTip(this.spellShuffleBox, "Each spell has a 25% chance of being known");
            this.spellShuffleBox.UseVisualStyleBackColor = true;
            this.spellShuffleBox.CheckedChanged += new System.EventHandler(this.spellShuffleBox_CheckedChanged);
            // 
            // itemGrp
            // 
            this.itemGrp.Controls.Add(this.keyBox);
            this.itemGrp.Controls.Add(this.hammerBox);
            this.itemGrp.Controls.Add(this.crossBox);
            this.itemGrp.Controls.Add(this.fluteBox);
            this.itemGrp.Controls.Add(this.bootsBox);
            this.itemGrp.Controls.Add(this.raftBox);
            this.itemGrp.Controls.Add(this.gloveBox);
            this.itemGrp.Controls.Add(this.candleBox);
            this.itemGrp.Controls.Add(this.shuffleItemBox);
            this.itemGrp.Location = new System.Drawing.Point(6, 6);
            this.itemGrp.Margin = new System.Windows.Forms.Padding(6);
            this.itemGrp.Name = "itemGrp";
            this.itemGrp.Padding = new System.Windows.Forms.Padding(6);
            this.itemGrp.Size = new System.Drawing.Size(320, 394);
            this.itemGrp.TabIndex = 0;
            this.itemGrp.TabStop = false;
            this.itemGrp.Text = "                                        ";
            // 
            // keyBox
            // 
            this.keyBox.AutoSize = true;
            this.keyBox.Location = new System.Drawing.Point(46, 346);
            this.keyBox.Margin = new System.Windows.Forms.Padding(6);
            this.keyBox.Name = "keyBox";
            this.keyBox.Size = new System.Drawing.Size(245, 29);
            this.keyBox.TabIndex = 7;
            this.keyBox.Text = "Start With Magic Key";
            this.toolTip1.SetToolTip(this.keyBox, "Start with the magic key");
            this.keyBox.UseVisualStyleBackColor = true;
            // 
            // hammerBox
            // 
            this.hammerBox.AutoSize = true;
            this.hammerBox.Location = new System.Drawing.Point(46, 302);
            this.hammerBox.Margin = new System.Windows.Forms.Padding(6);
            this.hammerBox.Name = "hammerBox";
            this.hammerBox.Size = new System.Drawing.Size(224, 29);
            this.hammerBox.TabIndex = 6;
            this.hammerBox.Text = "Start With Hammer";
            this.toolTip1.SetToolTip(this.hammerBox, "Start with the hammer");
            this.hammerBox.UseVisualStyleBackColor = true;
            // 
            // crossBox
            // 
            this.crossBox.AutoSize = true;
            this.crossBox.Location = new System.Drawing.Point(46, 258);
            this.crossBox.Margin = new System.Windows.Forms.Padding(6);
            this.crossBox.Name = "crossBox";
            this.crossBox.Size = new System.Drawing.Size(200, 29);
            this.crossBox.TabIndex = 5;
            this.crossBox.Text = "Start With Cross";
            this.toolTip1.SetToolTip(this.crossBox, "Start with the cross");
            this.crossBox.UseVisualStyleBackColor = true;
            // 
            // fluteBox
            // 
            this.fluteBox.AutoSize = true;
            this.fluteBox.Location = new System.Drawing.Point(46, 213);
            this.fluteBox.Margin = new System.Windows.Forms.Padding(6);
            this.fluteBox.Name = "fluteBox";
            this.fluteBox.Size = new System.Drawing.Size(192, 29);
            this.fluteBox.TabIndex = 4;
            this.fluteBox.Text = "Start With Flute";
            this.toolTip1.SetToolTip(this.fluteBox, "Start with the flute");
            this.fluteBox.UseVisualStyleBackColor = true;
            // 
            // bootsBox
            // 
            this.bootsBox.AutoSize = true;
            this.bootsBox.Location = new System.Drawing.Point(46, 169);
            this.bootsBox.Margin = new System.Windows.Forms.Padding(6);
            this.bootsBox.Name = "bootsBox";
            this.bootsBox.Size = new System.Drawing.Size(199, 29);
            this.bootsBox.TabIndex = 3;
            this.bootsBox.Text = "Start With Boots";
            this.toolTip1.SetToolTip(this.bootsBox, "Start with the boots");
            this.bootsBox.UseVisualStyleBackColor = true;
            // 
            // raftBox
            // 
            this.raftBox.AutoSize = true;
            this.raftBox.Location = new System.Drawing.Point(46, 125);
            this.raftBox.Margin = new System.Windows.Forms.Padding(6);
            this.raftBox.Name = "raftBox";
            this.raftBox.Size = new System.Drawing.Size(183, 29);
            this.raftBox.TabIndex = 2;
            this.raftBox.Text = "Start With Raft";
            this.toolTip1.SetToolTip(this.raftBox, "Start with the raft");
            this.raftBox.UseVisualStyleBackColor = true;
            // 
            // gloveBox
            // 
            this.gloveBox.AutoSize = true;
            this.gloveBox.Location = new System.Drawing.Point(46, 81);
            this.gloveBox.Margin = new System.Windows.Forms.Padding(6);
            this.gloveBox.Name = "gloveBox";
            this.gloveBox.Size = new System.Drawing.Size(200, 29);
            this.gloveBox.TabIndex = 1;
            this.gloveBox.Text = "Start With Glove";
            this.toolTip1.SetToolTip(this.gloveBox, "Start with the glove");
            this.gloveBox.UseVisualStyleBackColor = true;
            // 
            // candleBox
            // 
            this.candleBox.AutoSize = true;
            this.candleBox.Location = new System.Drawing.Point(46, 37);
            this.candleBox.Margin = new System.Windows.Forms.Padding(6);
            this.candleBox.Name = "candleBox";
            this.candleBox.Size = new System.Drawing.Size(212, 29);
            this.candleBox.TabIndex = 1;
            this.candleBox.Text = "Start With Candle";
            this.toolTip1.SetToolTip(this.candleBox, "Start with candle");
            this.candleBox.UseVisualStyleBackColor = true;
            // 
            // shuffleItemBox
            // 
            this.shuffleItemBox.AccessibleDescription = "";
            this.shuffleItemBox.AutoSize = true;
            this.shuffleItemBox.Location = new System.Drawing.Point(12, 0);
            this.shuffleItemBox.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleItemBox.Name = "shuffleItemBox";
            this.shuffleItemBox.Size = new System.Drawing.Size(248, 29);
            this.shuffleItemBox.TabIndex = 1;
            this.shuffleItemBox.Text = "Shuffle Starting Items";
            this.toolTip1.SetToolTip(this.shuffleItemBox, "Each item has a 25% chance of being in your inventory");
            this.shuffleItemBox.UseVisualStyleBackColor = true;
            this.shuffleItemBox.CheckedChanged += new System.EventHandler(this.shuffleItemBox_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.hideKasutoBox);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.hpCmbo);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.p7Shuffle);
            this.tabPage1.Controls.Add(this.palaceSwapBox);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.allowPathEnemies);
            this.tabPage1.Controls.Add(this.shuffleEncounters);
            this.tabPage1.Location = new System.Drawing.Point(8, 39);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage1.Size = new System.Drawing.Size(1008, 414);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Overworld";
            this.tabPage1.ToolTipText = "When selected, will hide Kasuto behind a forest tile";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // hideKasutoBox
            // 
            this.hideKasutoBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hideKasutoBox.FormattingEnabled = true;
            this.hideKasutoBox.Items.AddRange(new object[] {
            "Off",
            "On",
            "Random"});
            this.hideKasutoBox.Location = new System.Drawing.Point(708, 56);
            this.hideKasutoBox.Margin = new System.Windows.Forms.Padding(6);
            this.hideKasutoBox.Name = "hideKasutoBox";
            this.hideKasutoBox.Size = new System.Drawing.Size(238, 33);
            this.hideKasutoBox.TabIndex = 25;
            this.toolTip1.SetToolTip(this.hideKasutoBox, "When selected, will hide Kasuto behind a forest tile");
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(536, 59);
            this.label18.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(159, 25);
            this.label18.TabIndex = 24;
            this.label18.Text = "Hidden Kasuto:";
            // 
            // hpCmbo
            // 
            this.hpCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hpCmbo.FormattingEnabled = true;
            this.hpCmbo.Items.AddRange(new object[] {
            "Off",
            "On",
            "Random"});
            this.hpCmbo.Location = new System.Drawing.Point(708, 8);
            this.hpCmbo.Margin = new System.Windows.Forms.Padding(6);
            this.hpCmbo.Name = "hpCmbo";
            this.hpCmbo.Size = new System.Drawing.Size(238, 33);
            this.hpCmbo.TabIndex = 23;
            this.toolTip1.SetToolTip(this.hpCmbo, "When selected, will include three eye rock on the overworld");
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(536, 13);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(158, 25);
            this.label14.TabIndex = 22;
            this.label14.Text = "Hidden Palace:";
            // 
            // p7Shuffle
            // 
            this.p7Shuffle.AutoSize = true;
            this.p7Shuffle.Location = new System.Drawing.Point(12, 53);
            this.p7Shuffle.Margin = new System.Windows.Forms.Padding(6);
            this.p7Shuffle.Name = "p7Shuffle";
            this.p7Shuffle.Size = new System.Drawing.Size(340, 29);
            this.p7Shuffle.TabIndex = 21;
            this.p7Shuffle.Text = "Include Great Palace in Shuffle";
            this.toolTip1.SetToolTip(this.p7Shuffle, "When selected, palace 7 does not have to be in the valley of death.");
            this.p7Shuffle.UseVisualStyleBackColor = true;
            // 
            // palaceSwapBox
            // 
            this.palaceSwapBox.AutoSize = true;
            this.palaceSwapBox.Location = new System.Drawing.Point(12, 12);
            this.palaceSwapBox.Margin = new System.Windows.Forms.Padding(6);
            this.palaceSwapBox.Name = "palaceSwapBox";
            this.palaceSwapBox.Size = new System.Drawing.Size(370, 29);
            this.palaceSwapBox.TabIndex = 20;
            this.palaceSwapBox.Text = "Allow Palaces to Swap Continents";
            this.toolTip1.SetToolTip(this.palaceSwapBox, "When selected, palaces can move from their normal continents. Palace 1 could be f" +
        "ound on Maze Island or East Hyrule, for example.");
            this.palaceSwapBox.UseVisualStyleBackColor = true;
            this.palaceSwapBox.CheckedChanged += new System.EventHandler(this.palaceSwapBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(12, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(416, 2);
            this.label4.TabIndex = 18;
            // 
            // allowPathEnemies
            // 
            this.allowPathEnemies.AutoSize = true;
            this.allowPathEnemies.Location = new System.Drawing.Point(12, 174);
            this.allowPathEnemies.Margin = new System.Windows.Forms.Padding(6);
            this.allowPathEnemies.Name = "allowPathEnemies";
            this.allowPathEnemies.Size = new System.Drawing.Size(334, 29);
            this.allowPathEnemies.TabIndex = 15;
            this.allowPathEnemies.Text = "Allow Unsafe Path Encounters";
            this.toolTip1.SetToolTip(this.allowPathEnemies, "If checked, you may have enemies in path encounters");
            this.allowPathEnemies.UseVisualStyleBackColor = true;
            // 
            // shuffleEncounters
            // 
            this.shuffleEncounters.AutoSize = true;
            this.shuffleEncounters.Location = new System.Drawing.Point(12, 130);
            this.shuffleEncounters.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleEncounters.Name = "shuffleEncounters";
            this.shuffleEncounters.Size = new System.Drawing.Size(226, 29);
            this.shuffleEncounters.TabIndex = 14;
            this.shuffleEncounters.Text = "Shuffle Encounters";
            this.toolTip1.SetToolTip(this.shuffleEncounters, "Shuffle which overworld encounters occur on different terrain types");
            this.shuffleEncounters.UseVisualStyleBackColor = true;
            this.shuffleEncounters.CheckedChanged += new System.EventHandler(this.shuffleEncounters_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.removeTbird);
            this.tabPage2.Controls.Add(this.gpBox);
            this.tabPage2.Controls.Add(this.upaBox);
            this.tabPage2.Controls.Add(this.palacePalette);
            this.tabPage2.Controls.Add(this.tbirdBox);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.numGemsCbo);
            this.tabPage2.Controls.Add(this.palaceRoomBox);
            this.tabPage2.Location = new System.Drawing.Point(8, 39);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage2.Size = new System.Drawing.Size(1008, 414);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Palaces";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // removeTbird
            // 
            this.removeTbird.AutoSize = true;
            this.removeTbird.Location = new System.Drawing.Point(12, 144);
            this.removeTbird.Margin = new System.Windows.Forms.Padding(6);
            this.removeTbird.Name = "removeTbird";
            this.removeTbird.Size = new System.Drawing.Size(245, 29);
            this.removeTbird.TabIndex = 7;
            this.removeTbird.Text = "Remove Thunderbird";
            this.toolTip1.SetToolTip(this.removeTbird, "If checked, you must defeat thunderbird");
            this.removeTbird.UseVisualStyleBackColor = true;
            this.removeTbird.CheckedChanged += new System.EventHandler(this.removeTbird_CheckedChanged);
            // 
            // gpBox
            // 
            this.gpBox.AutoSize = true;
            this.gpBox.Location = new System.Drawing.Point(12, 56);
            this.gpBox.Margin = new System.Windows.Forms.Padding(6);
            this.gpBox.Name = "gpBox";
            this.gpBox.Size = new System.Drawing.Size(250, 29);
            this.gpBox.TabIndex = 6;
            this.gpBox.Text = "Shorten Great Palace";
            this.toolTip1.SetToolTip(this.gpBox, "When selected, the Great Palace will have fewer rooms than normal");
            this.gpBox.UseVisualStyleBackColor = true;
            // 
            // upaBox
            // 
            this.upaBox.AutoSize = true;
            this.upaBox.Location = new System.Drawing.Point(482, 12);
            this.upaBox.Margin = new System.Windows.Forms.Padding(6);
            this.upaBox.Name = "upaBox";
            this.upaBox.Size = new System.Drawing.Size(355, 29);
            this.upaBox.TabIndex = 5;
            this.upaBox.Text = "Restart at palaces on game over";
            this.toolTip1.SetToolTip(this.upaBox, "When selected, if you game over in a palace, you will restart at that palace inst" +
        "ead of the normal starting spot");
            this.upaBox.UseVisualStyleBackColor = true;
            // 
            // palacePalette
            // 
            this.palacePalette.AutoSize = true;
            this.palacePalette.Location = new System.Drawing.Point(12, 188);
            this.palacePalette.Margin = new System.Windows.Forms.Padding(6);
            this.palacePalette.Name = "palacePalette";
            this.palacePalette.Size = new System.Drawing.Size(275, 29);
            this.palacePalette.TabIndex = 4;
            this.palacePalette.Text = "Change Palace Palettes";
            this.toolTip1.SetToolTip(this.palacePalette, "This option changes the colors and tileset of palaces");
            this.palacePalette.UseVisualStyleBackColor = true;
            // 
            // tbirdBox
            // 
            this.tbirdBox.AutoSize = true;
            this.tbirdBox.Location = new System.Drawing.Point(12, 100);
            this.tbirdBox.Margin = new System.Windows.Forms.Padding(6);
            this.tbirdBox.Name = "tbirdBox";
            this.tbirdBox.Size = new System.Drawing.Size(253, 29);
            this.tbirdBox.TabIndex = 3;
            this.tbirdBox.Text = "Thunderbird Required";
            this.toolTip1.SetToolTip(this.tbirdBox, "If checked, you must defeat thunderbird");
            this.tbirdBox.UseVisualStyleBackColor = true;
            this.tbirdBox.CheckedChanged += new System.EventHandler(this.tbirdBox_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 290);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(315, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Number of Palaces to Complete";
            // 
            // numGemsCbo
            // 
            this.numGemsCbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numGemsCbo.FormattingEnabled = true;
            this.numGemsCbo.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "Random"});
            this.numGemsCbo.Location = new System.Drawing.Point(12, 321);
            this.numGemsCbo.Margin = new System.Windows.Forms.Padding(6);
            this.numGemsCbo.Name = "numGemsCbo";
            this.numGemsCbo.Size = new System.Drawing.Size(238, 33);
            this.numGemsCbo.TabIndex = 1;
            this.toolTip1.SetToolTip(this.numGemsCbo, "How many gems need to be placed before entering palace 7");
            // 
            // palaceRoomBox
            // 
            this.palaceRoomBox.AutoSize = true;
            this.palaceRoomBox.Location = new System.Drawing.Point(12, 12);
            this.palaceRoomBox.Margin = new System.Windows.Forms.Padding(6);
            this.palaceRoomBox.Name = "palaceRoomBox";
            this.palaceRoomBox.Size = new System.Drawing.Size(256, 29);
            this.palaceRoomBox.TabIndex = 0;
            this.palaceRoomBox.Text = "Shuffle Palace Rooms";
            this.toolTip1.SetToolTip(this.palaceRoomBox, "Shuffles the palace layouts");
            this.palaceRoomBox.UseVisualStyleBackColor = true;
            this.palaceRoomBox.CheckedChanged += new System.EventHandler(this.palaceRoomBox_CheckedChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.combineFireBox);
            this.tabPage5.Controls.Add(this.lifeLabel);
            this.tabPage5.Controls.Add(this.magLabel);
            this.tabPage5.Controls.Add(this.atkLabel);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.lifeBar);
            this.tabPage5.Controls.Add(this.magicBar);
            this.tabPage5.Controls.Add(this.attackBar);
            this.tabPage5.Controls.Add(this.disableJarBox);
            this.tabPage5.Controls.Add(this.shuffleSpellLocationsBox);
            this.tabPage5.Controls.Add(this.lifeRefilBox);
            this.tabPage5.Controls.Add(this.expBox);
            this.tabPage5.Location = new System.Drawing.Point(8, 39);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1008, 414);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Levels and Spells";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // combineFireBox
            // 
            this.combineFireBox.AutoSize = true;
            this.combineFireBox.Location = new System.Drawing.Point(18, 315);
            this.combineFireBox.Margin = new System.Windows.Forms.Padding(6);
            this.combineFireBox.Name = "combineFireBox";
            this.combineFireBox.Size = new System.Drawing.Size(374, 29);
            this.combineFireBox.TabIndex = 15;
            this.combineFireBox.Text = "Combine Fire with a Random Spell";
            this.combineFireBox.UseVisualStyleBackColor = true;
            // 
            // lifeLabel
            // 
            this.lifeLabel.Location = new System.Drawing.Point(694, 288);
            this.lifeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lifeLabel.Name = "lifeLabel";
            this.lifeLabel.Size = new System.Drawing.Size(200, 25);
            this.lifeLabel.TabIndex = 14;
            this.lifeLabel.Text = "Random";
            this.lifeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // magLabel
            // 
            this.magLabel.Location = new System.Drawing.Point(694, 190);
            this.magLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.magLabel.Name = "magLabel";
            this.magLabel.Size = new System.Drawing.Size(200, 25);
            this.magLabel.TabIndex = 13;
            this.magLabel.Text = "Random";
            this.magLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // atkLabel
            // 
            this.atkLabel.Location = new System.Drawing.Point(694, 88);
            this.atkLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.atkLabel.Name = "atkLabel";
            this.atkLabel.Size = new System.Drawing.Size(200, 25);
            this.atkLabel.TabIndex = 12;
            this.atkLabel.Text = "Random";
            this.atkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(448, 235);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(182, 25);
            this.label12.TabIndex = 11;
            this.label12.Text = "Life Effectiveness";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(448, 138);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(205, 25);
            this.label11.TabIndex = 10;
            this.label11.Text = "Magic Effectiveness";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(448, 44);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(207, 25);
            this.label10.TabIndex = 9;
            this.label10.Text = "Attack Effectiveness";
            // 
            // lifeBar
            // 
            this.lifeBar.BackColor = System.Drawing.SystemColors.Window;
            this.lifeBar.Location = new System.Drawing.Point(670, 227);
            this.lifeBar.Margin = new System.Windows.Forms.Padding(6);
            this.lifeBar.Maximum = 4;
            this.lifeBar.Name = "lifeBar";
            this.lifeBar.Size = new System.Drawing.Size(242, 90);
            this.lifeBar.TabIndex = 8;
            this.toolTip1.SetToolTip(this.lifeBar, "Moving this slider will affect how much damage Link takes.");
            this.lifeBar.Scroll += new System.EventHandler(this.lifeBar_Scroll);
            this.lifeBar.ValueChanged += new System.EventHandler(this.lifeBar_Scroll);
            // 
            // magicBar
            // 
            this.magicBar.BackColor = System.Drawing.SystemColors.Window;
            this.magicBar.Location = new System.Drawing.Point(670, 129);
            this.magicBar.Margin = new System.Windows.Forms.Padding(6);
            this.magicBar.Maximum = 4;
            this.magicBar.Name = "magicBar";
            this.magicBar.Size = new System.Drawing.Size(242, 90);
            this.magicBar.TabIndex = 7;
            this.toolTip1.SetToolTip(this.magicBar, "Moving this slider will affect how much magic spells cost.");
            this.magicBar.Scroll += new System.EventHandler(this.magicBar_Scroll);
            this.magicBar.ValueChanged += new System.EventHandler(this.magicBar_Scroll);
            // 
            // attackBar
            // 
            this.attackBar.BackColor = System.Drawing.SystemColors.Window;
            this.attackBar.Location = new System.Drawing.Point(670, 33);
            this.attackBar.Margin = new System.Windows.Forms.Padding(6);
            this.attackBar.Maximum = 4;
            this.attackBar.Name = "attackBar";
            this.attackBar.Size = new System.Drawing.Size(242, 90);
            this.attackBar.TabIndex = 6;
            this.toolTip1.SetToolTip(this.attackBar, "How powerful are Link\'s attacks?");
            this.attackBar.Scroll += new System.EventHandler(this.attackBar_Scroll);
            this.attackBar.ValueChanged += new System.EventHandler(this.attackBar_Scroll);
            // 
            // disableJarBox
            // 
            this.disableJarBox.AutoSize = true;
            this.disableJarBox.Location = new System.Drawing.Point(18, 271);
            this.disableJarBox.Margin = new System.Windows.Forms.Padding(6);
            this.disableJarBox.Name = "disableJarBox";
            this.disableJarBox.Size = new System.Drawing.Size(418, 29);
            this.disableJarBox.TabIndex = 5;
            this.disableJarBox.Text = "Disable Magic Container Requirements";
            this.toolTip1.SetToolTip(this.disableJarBox, "When checked, you can get spells without having the necessary magic containers");
            this.disableJarBox.UseVisualStyleBackColor = true;
            // 
            // shuffleSpellLocationsBox
            // 
            this.shuffleSpellLocationsBox.AutoSize = true;
            this.shuffleSpellLocationsBox.Location = new System.Drawing.Point(18, 227);
            this.shuffleSpellLocationsBox.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleSpellLocationsBox.Name = "shuffleSpellLocationsBox";
            this.shuffleSpellLocationsBox.Size = new System.Drawing.Size(264, 29);
            this.shuffleSpellLocationsBox.TabIndex = 4;
            this.shuffleSpellLocationsBox.Text = "Shuffle Spell Locations";
            this.toolTip1.SetToolTip(this.shuffleSpellLocationsBox, "This option shuffles which towns you find the spells in");
            this.shuffleSpellLocationsBox.UseVisualStyleBackColor = true;
            // 
            // lifeRefilBox
            // 
            this.lifeRefilBox.AutoSize = true;
            this.lifeRefilBox.Location = new System.Drawing.Point(18, 183);
            this.lifeRefilBox.Margin = new System.Windows.Forms.Padding(6);
            this.lifeRefilBox.Name = "lifeRefilBox";
            this.lifeRefilBox.Size = new System.Drawing.Size(285, 29);
            this.lifeRefilBox.TabIndex = 3;
            this.lifeRefilBox.Text = "Shuffle Life Refill Amount";
            this.toolTip1.SetToolTip(this.lifeRefilBox, "Shuffles how much health is restored when the life spell is used");
            this.lifeRefilBox.UseVisualStyleBackColor = true;
            // 
            // expBox
            // 
            this.expBox.Controls.Add(this.lifeExpNeeded);
            this.expBox.Controls.Add(this.magicExpNeeded);
            this.expBox.Controls.Add(this.shuffleAtkExp);
            this.expBox.Controls.Add(this.shuffleAllExp);
            this.expBox.Location = new System.Drawing.Point(6, 6);
            this.expBox.Margin = new System.Windows.Forms.Padding(6);
            this.expBox.Name = "expBox";
            this.expBox.Padding = new System.Windows.Forms.Padding(6);
            this.expBox.Size = new System.Drawing.Size(422, 165);
            this.expBox.TabIndex = 0;
            this.expBox.TabStop = false;
            this.expBox.Text = "                                                      ";
            // 
            // lifeExpNeeded
            // 
            this.lifeExpNeeded.AutoSize = true;
            this.lifeExpNeeded.Location = new System.Drawing.Point(36, 125);
            this.lifeExpNeeded.Margin = new System.Windows.Forms.Padding(6);
            this.lifeExpNeeded.Name = "lifeExpNeeded";
            this.lifeExpNeeded.Size = new System.Drawing.Size(347, 29);
            this.lifeExpNeeded.TabIndex = 3;
            this.lifeExpNeeded.Text = "Shuffle Life Experience Needed";
            this.toolTip1.SetToolTip(this.lifeExpNeeded, "Shuffles experience needed for life levels");
            this.lifeExpNeeded.UseVisualStyleBackColor = true;
            // 
            // magicExpNeeded
            // 
            this.magicExpNeeded.AutoSize = true;
            this.magicExpNeeded.Location = new System.Drawing.Point(36, 81);
            this.magicExpNeeded.Margin = new System.Windows.Forms.Padding(6);
            this.magicExpNeeded.Name = "magicExpNeeded";
            this.magicExpNeeded.Size = new System.Drawing.Size(370, 29);
            this.magicExpNeeded.TabIndex = 2;
            this.magicExpNeeded.Text = "Shuffle Magic Experience Needed";
            this.toolTip1.SetToolTip(this.magicExpNeeded, "Shuffles experience needed for magic levels");
            this.magicExpNeeded.UseVisualStyleBackColor = true;
            // 
            // shuffleAtkExp
            // 
            this.shuffleAtkExp.AutoSize = true;
            this.shuffleAtkExp.Location = new System.Drawing.Point(36, 37);
            this.shuffleAtkExp.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleAtkExp.Name = "shuffleAtkExp";
            this.shuffleAtkExp.Size = new System.Drawing.Size(372, 29);
            this.shuffleAtkExp.TabIndex = 1;
            this.shuffleAtkExp.Text = "Shuffle Attack Experience Needed";
            this.toolTip1.SetToolTip(this.shuffleAtkExp, "Shuffles experience needed for attack levels");
            this.shuffleAtkExp.UseVisualStyleBackColor = true;
            // 
            // shuffleAllExp
            // 
            this.shuffleAllExp.AutoSize = true;
            this.shuffleAllExp.Location = new System.Drawing.Point(12, 0);
            this.shuffleAllExp.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleAllExp.Name = "shuffleAllExp";
            this.shuffleAllExp.Size = new System.Drawing.Size(336, 29);
            this.shuffleAllExp.TabIndex = 1;
            this.shuffleAllExp.Text = "Shuffle All Experience Needed";
            this.toolTip1.SetToolTip(this.shuffleAllExp, "Shuffles experience needed for all levels");
            this.shuffleAllExp.UseVisualStyleBackColor = true;
            this.shuffleAllExp.CheckedChanged += new System.EventHandler(this.shuffleAllExp_CheckedChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.shuffleDripper);
            this.tabPage6.Controls.Add(this.mixEnemies);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.shufflePalaceEnemies);
            this.tabPage6.Controls.Add(this.shuffleOverworldEnemies);
            this.tabPage6.Controls.Add(this.shuffleBossExp);
            this.tabPage6.Controls.Add(this.shuffleEnemyExp);
            this.tabPage6.Controls.Add(this.swordImmuneBox);
            this.tabPage6.Controls.Add(this.stealExpAmt);
            this.tabPage6.Controls.Add(this.stealExpBox);
            this.tabPage6.Controls.Add(this.shuffleEnemyHPBox);
            this.tabPage6.Location = new System.Drawing.Point(8, 39);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1008, 414);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Enemies";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // shuffleDripper
            // 
            this.shuffleDripper.AutoSize = true;
            this.shuffleDripper.Location = new System.Drawing.Point(6, 98);
            this.shuffleDripper.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleDripper.Name = "shuffleDripper";
            this.shuffleDripper.Size = new System.Drawing.Size(259, 29);
            this.shuffleDripper.TabIndex = 20;
            this.shuffleDripper.Text = "Shuffle Dripper Enemy";
            this.toolTip1.SetToolTip(this.shuffleDripper, "When selected, the enemy spawned by the dripper will be randomized");
            this.shuffleDripper.UseVisualStyleBackColor = true;
            // 
            // mixEnemies
            // 
            this.mixEnemies.AutoSize = true;
            this.mixEnemies.Location = new System.Drawing.Point(6, 142);
            this.mixEnemies.Margin = new System.Windows.Forms.Padding(6);
            this.mixEnemies.Name = "mixEnemies";
            this.mixEnemies.Size = new System.Drawing.Size(329, 29);
            this.mixEnemies.TabIndex = 18;
            this.mixEnemies.Text = "Mix Large and Small Enemies";
            this.toolTip1.SetToolTip(this.mixEnemies, "Allows large enemies to spawn where small enemies normally spawn and vice versa");
            this.mixEnemies.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(346, 10);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(4, 375);
            this.label8.TabIndex = 17;
            // 
            // shufflePalaceEnemies
            // 
            this.shufflePalaceEnemies.AutoSize = true;
            this.shufflePalaceEnemies.Location = new System.Drawing.Point(6, 54);
            this.shufflePalaceEnemies.Margin = new System.Windows.Forms.Padding(6);
            this.shufflePalaceEnemies.Name = "shufflePalaceEnemies";
            this.shufflePalaceEnemies.Size = new System.Drawing.Size(272, 29);
            this.shufflePalaceEnemies.TabIndex = 8;
            this.shufflePalaceEnemies.Text = "Shuffle Palace Enemies";
            this.toolTip1.SetToolTip(this.shufflePalaceEnemies, "Shuffles enemies in the palaces");
            this.shufflePalaceEnemies.UseVisualStyleBackColor = true;
            this.shufflePalaceEnemies.CheckedChanged += new System.EventHandler(this.shufflePalaceEnemies_CheckedChanged);
            // 
            // shuffleOverworldEnemies
            // 
            this.shuffleOverworldEnemies.AutoSize = true;
            this.shuffleOverworldEnemies.Location = new System.Drawing.Point(6, 10);
            this.shuffleOverworldEnemies.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleOverworldEnemies.Name = "shuffleOverworldEnemies";
            this.shuffleOverworldEnemies.Size = new System.Drawing.Size(303, 29);
            this.shuffleOverworldEnemies.TabIndex = 7;
            this.shuffleOverworldEnemies.Text = "Shuffle Overworld Enemies";
            this.toolTip1.SetToolTip(this.shuffleOverworldEnemies, "Shuffles enemies on the overworld");
            this.shuffleOverworldEnemies.UseVisualStyleBackColor = true;
            this.shuffleOverworldEnemies.CheckedChanged += new System.EventHandler(this.shuffleOverworldEnemies_CheckedChanged);
            // 
            // shuffleBossExp
            // 
            this.shuffleBossExp.AutoSize = true;
            this.shuffleBossExp.Location = new System.Drawing.Point(388, 98);
            this.shuffleBossExp.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleBossExp.Name = "shuffleBossExp";
            this.shuffleBossExp.Size = new System.Drawing.Size(208, 29);
            this.shuffleBossExp.TabIndex = 6;
            this.shuffleBossExp.Text = "Shuffle Boss Exp";
            this.toolTip1.SetToolTip(this.shuffleBossExp, "Shuffle the amount of experience dropped by bosses");
            this.shuffleBossExp.UseVisualStyleBackColor = true;
            // 
            // shuffleEnemyExp
            // 
            this.shuffleEnemyExp.AutoSize = true;
            this.shuffleEnemyExp.Location = new System.Drawing.Point(388, 54);
            this.shuffleEnemyExp.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleEnemyExp.Name = "shuffleEnemyExp";
            this.shuffleEnemyExp.Size = new System.Drawing.Size(226, 29);
            this.shuffleEnemyExp.TabIndex = 5;
            this.shuffleEnemyExp.Text = "Shuffle Enemy Exp";
            this.toolTip1.SetToolTip(this.shuffleEnemyExp, "Shuffle the amount of experience dropped by enemies");
            this.shuffleEnemyExp.UseVisualStyleBackColor = true;
            // 
            // swordImmuneBox
            // 
            this.swordImmuneBox.AutoSize = true;
            this.swordImmuneBox.Location = new System.Drawing.Point(388, 231);
            this.swordImmuneBox.Margin = new System.Windows.Forms.Padding(6);
            this.swordImmuneBox.Name = "swordImmuneBox";
            this.swordImmuneBox.Size = new System.Drawing.Size(268, 29);
            this.swordImmuneBox.TabIndex = 4;
            this.swordImmuneBox.Text = "Shuffle Sword Immunity";
            this.toolTip1.SetToolTip(this.swordImmuneBox, "Shuffle which enemies require fire to kill");
            this.swordImmuneBox.UseVisualStyleBackColor = true;
            // 
            // stealExpAmt
            // 
            this.stealExpAmt.AutoSize = true;
            this.stealExpAmt.Location = new System.Drawing.Point(388, 187);
            this.stealExpAmt.Margin = new System.Windows.Forms.Padding(6);
            this.stealExpAmt.Name = "stealExpAmt";
            this.stealExpAmt.Size = new System.Drawing.Size(324, 29);
            this.stealExpAmt.TabIndex = 3;
            this.stealExpAmt.Text = "Shuffle Amount of Exp Stolen";
            this.toolTip1.SetToolTip(this.stealExpAmt, "Shuffle how much experience is stolen from the player when taking damage from cer" +
        "tain enemies");
            this.stealExpAmt.UseVisualStyleBackColor = true;
            // 
            // stealExpBox
            // 
            this.stealExpBox.AutoSize = true;
            this.stealExpBox.Location = new System.Drawing.Point(388, 142);
            this.stealExpBox.Margin = new System.Windows.Forms.Padding(6);
            this.stealExpBox.Name = "stealExpBox";
            this.stealExpBox.Size = new System.Drawing.Size(364, 29);
            this.stealExpBox.TabIndex = 2;
            this.stealExpBox.Text = "Shuffle Which Enemies Steal Exp";
            this.toolTip1.SetToolTip(this.stealExpBox, "Shuffle which enemies steal experience when doing damage to the player");
            this.stealExpBox.UseVisualStyleBackColor = true;
            // 
            // shuffleEnemyHPBox
            // 
            this.shuffleEnemyHPBox.AutoSize = true;
            this.shuffleEnemyHPBox.Location = new System.Drawing.Point(388, 10);
            this.shuffleEnemyHPBox.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleEnemyHPBox.Name = "shuffleEnemyHPBox";
            this.shuffleEnemyHPBox.Size = new System.Drawing.Size(218, 29);
            this.shuffleEnemyHPBox.TabIndex = 0;
            this.shuffleEnemyHPBox.Text = "Shuffle Enemy HP";
            this.toolTip1.SetToolTip(this.shuffleEnemyHPBox, "Each enemy will have +/- 50% of its normal HP");
            this.shuffleEnemyHPBox.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.shufflePbagExp);
            this.tabPage7.Controls.Add(this.spellItemBox);
            this.tabPage7.Controls.Add(this.pbagItemShuffleBox);
            this.tabPage7.Controls.Add(this.kasutoBox);
            this.tabPage7.Controls.Add(this.palaceKeys);
            this.tabPage7.Controls.Add(this.shuffleSmallItemsBox);
            this.tabPage7.Controls.Add(this.mixItemBox);
            this.tabPage7.Controls.Add(this.overworldItemBox);
            this.tabPage7.Controls.Add(this.palaceItemBox);
            this.tabPage7.Location = new System.Drawing.Point(8, 39);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1008, 414);
            this.tabPage7.TabIndex = 7;
            this.tabPage7.Text = "Items";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // shufflePbagExp
            // 
            this.shufflePbagExp.AutoSize = true;
            this.shufflePbagExp.Location = new System.Drawing.Point(6, 357);
            this.shufflePbagExp.Name = "shufflePbagExp";
            this.shufflePbagExp.Size = new System.Drawing.Size(257, 29);
            this.shufflePbagExp.TabIndex = 19;
            this.shufflePbagExp.Text = "Shuffle Pbag Amounts";
            this.shufflePbagExp.UseVisualStyleBackColor = true;
            // 
            // spellItemBox
            // 
            this.spellItemBox.AutoSize = true;
            this.spellItemBox.Location = new System.Drawing.Point(6, 314);
            this.spellItemBox.Name = "spellItemBox";
            this.spellItemBox.Size = new System.Drawing.Size(234, 29);
            this.spellItemBox.TabIndex = 18;
            this.spellItemBox.Text = "Remove Spell Items";
            this.toolTip1.SetToolTip(this.spellItemBox, "When checked, you no longer need the trophy, medicine, or kid to access the respe" +
        "ctive spells");
            this.spellItemBox.UseVisualStyleBackColor = true;
            // 
            // pbagItemShuffleBox
            // 
            this.pbagItemShuffleBox.AutoSize = true;
            this.pbagItemShuffleBox.Location = new System.Drawing.Point(6, 138);
            this.pbagItemShuffleBox.Margin = new System.Windows.Forms.Padding(6);
            this.pbagItemShuffleBox.Name = "pbagItemShuffleBox";
            this.pbagItemShuffleBox.Size = new System.Drawing.Size(378, 29);
            this.pbagItemShuffleBox.TabIndex = 17;
            this.pbagItemShuffleBox.Text = "Include Pbag Caves in Item Shuffle";
            this.toolTip1.SetToolTip(this.pbagItemShuffleBox, "Will include the 3 pbag caves as item locations");
            this.pbagItemShuffleBox.UseVisualStyleBackColor = true;
            // 
            // kasutoBox
            // 
            this.kasutoBox.AutoSize = true;
            this.kasutoBox.Location = new System.Drawing.Point(6, 271);
            this.kasutoBox.Margin = new System.Windows.Forms.Padding(6);
            this.kasutoBox.Name = "kasutoBox";
            this.kasutoBox.Size = new System.Drawing.Size(448, 29);
            this.kasutoBox.TabIndex = 16;
            this.kasutoBox.Text = "Randomize New Kasuto Jar Requirements";
            this.toolTip1.SetToolTip(this.kasutoBox, "When selected, the number of jars required to get the item in New Kasuto will be " +
        "randomized between 5 and 7.");
            this.kasutoBox.UseVisualStyleBackColor = true;
            // 
            // palaceKeys
            // 
            this.palaceKeys.AutoSize = true;
            this.palaceKeys.Location = new System.Drawing.Point(6, 227);
            this.palaceKeys.Margin = new System.Windows.Forms.Padding(6);
            this.palaceKeys.Name = "palaceKeys";
            this.palaceKeys.Size = new System.Drawing.Size(311, 29);
            this.palaceKeys.TabIndex = 4;
            this.palaceKeys.Text = "Palaces Contain Extra Keys";
            this.toolTip1.SetToolTip(this.palaceKeys, "Inserts a lot of extra keys into the palaces");
            this.palaceKeys.UseVisualStyleBackColor = true;
            // 
            // shuffleSmallItemsBox
            // 
            this.shuffleSmallItemsBox.AutoSize = true;
            this.shuffleSmallItemsBox.Location = new System.Drawing.Point(6, 183);
            this.shuffleSmallItemsBox.Margin = new System.Windows.Forms.Padding(6);
            this.shuffleSmallItemsBox.Name = "shuffleSmallItemsBox";
            this.shuffleSmallItemsBox.Size = new System.Drawing.Size(227, 29);
            this.shuffleSmallItemsBox.TabIndex = 3;
            this.shuffleSmallItemsBox.Text = "Shuffle Small Items";
            this.toolTip1.SetToolTip(this.shuffleSmallItemsBox, "Shuffles pbags, jars, and 1ups");
            this.shuffleSmallItemsBox.UseVisualStyleBackColor = true;
            // 
            // mixItemBox
            // 
            this.mixItemBox.AutoSize = true;
            this.mixItemBox.Location = new System.Drawing.Point(6, 94);
            this.mixItemBox.Margin = new System.Windows.Forms.Padding(6);
            this.mixItemBox.Name = "mixItemBox";
            this.mixItemBox.Size = new System.Drawing.Size(352, 29);
            this.mixItemBox.TabIndex = 2;
            this.mixItemBox.Text = "Mix Overworld and Palace Items";
            this.toolTip1.SetToolTip(this.mixItemBox, "Allows palace items to be found in the overworld, and vice versa");
            this.mixItemBox.UseVisualStyleBackColor = true;
            // 
            // overworldItemBox
            // 
            this.overworldItemBox.AutoSize = true;
            this.overworldItemBox.Location = new System.Drawing.Point(6, 50);
            this.overworldItemBox.Margin = new System.Windows.Forms.Padding(6);
            this.overworldItemBox.Name = "overworldItemBox";
            this.overworldItemBox.Size = new System.Drawing.Size(271, 29);
            this.overworldItemBox.TabIndex = 1;
            this.overworldItemBox.Text = "Shuffle Overworld Items";
            this.toolTip1.SetToolTip(this.overworldItemBox, "Shuffles the items that are found in the overworld");
            this.overworldItemBox.UseVisualStyleBackColor = true;
            this.overworldItemBox.CheckedChanged += new System.EventHandler(this.overworldItemBox_CheckedChanged);
            // 
            // palaceItemBox
            // 
            this.palaceItemBox.AutoSize = true;
            this.palaceItemBox.Location = new System.Drawing.Point(6, 6);
            this.palaceItemBox.Margin = new System.Windows.Forms.Padding(6);
            this.palaceItemBox.Name = "palaceItemBox";
            this.palaceItemBox.Size = new System.Drawing.Size(240, 29);
            this.palaceItemBox.TabIndex = 0;
            this.palaceItemBox.Text = "Shuffle Palace Items";
            this.toolTip1.SetToolTip(this.palaceItemBox, "Shuffles the items that are found in palaces");
            this.palaceItemBox.UseVisualStyleBackColor = true;
            this.palaceItemBox.CheckedChanged += new System.EventHandler(this.palaceItemBox_CheckedChanged);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.randoDrops);
            this.tabPage8.Controls.Add(this.standardDrops);
            this.tabPage8.Controls.Add(this.largeKey);
            this.tabPage8.Controls.Add(this.large1up);
            this.tabPage8.Controls.Add(this.large500);
            this.tabPage8.Controls.Add(this.large200);
            this.tabPage8.Controls.Add(this.large100);
            this.tabPage8.Controls.Add(this.large50);
            this.tabPage8.Controls.Add(this.largeRedJar);
            this.tabPage8.Controls.Add(this.largeBlueJar);
            this.tabPage8.Controls.Add(this.label21);
            this.tabPage8.Controls.Add(this.smallKey);
            this.tabPage8.Controls.Add(this.small1up);
            this.tabPage8.Controls.Add(this.small500);
            this.tabPage8.Controls.Add(this.small200);
            this.tabPage8.Controls.Add(this.small100);
            this.tabPage8.Controls.Add(this.small50);
            this.tabPage8.Controls.Add(this.smallRedJar);
            this.tabPage8.Controls.Add(this.smallBlueJar);
            this.tabPage8.Controls.Add(this.label20);
            this.tabPage8.Controls.Add(this.label19);
            this.tabPage8.Controls.Add(this.enemyDropBox);
            this.tabPage8.Controls.Add(this.pbagDrop);
            this.tabPage8.Location = new System.Drawing.Point(8, 39);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1008, 414);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "Drops";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // randoDrops
            // 
            this.randoDrops.AutoSize = true;
            this.randoDrops.Location = new System.Drawing.Point(15, 100);
            this.randoDrops.Name = "randoDrops";
            this.randoDrops.Size = new System.Drawing.Size(215, 29);
            this.randoDrops.TabIndex = 45;
            this.randoDrops.Text = "Randomize Drops";
            this.toolTip1.SetToolTip(this.randoDrops, "When selected, the items in the drop pool will be randomized");
            this.randoDrops.UseVisualStyleBackColor = true;
            this.randoDrops.CheckedChanged += new System.EventHandler(this.randoDrops_CheckedChanged);
            // 
            // standardDrops
            // 
            this.standardDrops.AutoSize = true;
            this.standardDrops.Location = new System.Drawing.Point(15, 140);
            this.standardDrops.Name = "standardDrops";
            this.standardDrops.Size = new System.Drawing.Size(222, 29);
            this.standardDrops.TabIndex = 44;
            this.standardDrops.Text = "Standardize Drops";
            this.toolTip1.SetToolTip(this.standardDrops, "When selected, all runners playing the same seed will get the same drops in the s" +
        "ame order");
            this.standardDrops.UseVisualStyleBackColor = true;
            // 
            // largeKey
            // 
            this.largeKey.AutoSize = true;
            this.largeKey.Enabled = false;
            this.largeKey.Location = new System.Drawing.Point(692, 340);
            this.largeKey.Name = "largeKey";
            this.largeKey.Size = new System.Drawing.Size(81, 29);
            this.largeKey.TabIndex = 43;
            this.largeKey.Text = "Key";
            this.toolTip1.SetToolTip(this.largeKey, "Add small keys to the large enemy pool");
            this.largeKey.UseVisualStyleBackColor = true;
            // 
            // large1up
            // 
            this.large1up.AutoSize = true;
            this.large1up.Enabled = false;
            this.large1up.Location = new System.Drawing.Point(693, 300);
            this.large1up.Name = "large1up";
            this.large1up.Size = new System.Drawing.Size(80, 29);
            this.large1up.TabIndex = 42;
            this.large1up.Text = "1up";
            this.toolTip1.SetToolTip(this.large1up, "Add 1ups to the large enemy pool");
            this.large1up.UseVisualStyleBackColor = true;
            // 
            // large500
            // 
            this.large500.AutoSize = true;
            this.large500.Enabled = false;
            this.large500.Location = new System.Drawing.Point(693, 260);
            this.large500.Name = "large500";
            this.large500.Size = new System.Drawing.Size(134, 29);
            this.large500.TabIndex = 41;
            this.large500.Text = "500 pbag";
            this.toolTip1.SetToolTip(this.large500, "Add 500 bags to the large enemy pool");
            this.large500.UseVisualStyleBackColor = true;
            // 
            // large200
            // 
            this.large200.AutoSize = true;
            this.large200.Checked = true;
            this.large200.CheckState = System.Windows.Forms.CheckState.Checked;
            this.large200.Enabled = false;
            this.large200.Location = new System.Drawing.Point(693, 220);
            this.large200.Name = "large200";
            this.large200.Size = new System.Drawing.Size(134, 29);
            this.large200.TabIndex = 40;
            this.large200.Text = "200 pbag";
            this.toolTip1.SetToolTip(this.large200, "Add 200 bags to the large enemy pool");
            this.large200.UseVisualStyleBackColor = true;
            // 
            // large100
            // 
            this.large100.AutoSize = true;
            this.large100.Enabled = false;
            this.large100.Location = new System.Drawing.Point(693, 180);
            this.large100.Name = "large100";
            this.large100.Size = new System.Drawing.Size(134, 29);
            this.large100.TabIndex = 39;
            this.large100.Text = "100 pbag";
            this.toolTip1.SetToolTip(this.large100, "Add 100 bags to the large enemy pool");
            this.large100.UseVisualStyleBackColor = true;
            // 
            // large50
            // 
            this.large50.AutoSize = true;
            this.large50.Enabled = false;
            this.large50.Location = new System.Drawing.Point(693, 140);
            this.large50.Name = "large50";
            this.large50.Size = new System.Drawing.Size(122, 29);
            this.large50.TabIndex = 38;
            this.large50.Text = "50 pbag";
            this.toolTip1.SetToolTip(this.large50, "Add 50 bags to the large enemy pool");
            this.large50.UseVisualStyleBackColor = true;
            // 
            // largeRedJar
            // 
            this.largeRedJar.AutoSize = true;
            this.largeRedJar.Checked = true;
            this.largeRedJar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.largeRedJar.Enabled = false;
            this.largeRedJar.Location = new System.Drawing.Point(693, 100);
            this.largeRedJar.Name = "largeRedJar";
            this.largeRedJar.Size = new System.Drawing.Size(119, 29);
            this.largeRedJar.TabIndex = 37;
            this.largeRedJar.Text = "Red Jar";
            this.toolTip1.SetToolTip(this.largeRedJar, "Add red jars to the large enemy pool");
            this.largeRedJar.UseVisualStyleBackColor = true;
            // 
            // largeBlueJar
            // 
            this.largeBlueJar.AutoSize = true;
            this.largeBlueJar.Enabled = false;
            this.largeBlueJar.Location = new System.Drawing.Point(693, 60);
            this.largeBlueJar.Name = "largeBlueJar";
            this.largeBlueJar.Size = new System.Drawing.Size(123, 29);
            this.largeBlueJar.TabIndex = 36;
            this.largeBlueJar.Text = "Blue Jar";
            this.toolTip1.SetToolTip(this.largeBlueJar, "Add blue jars to the large enemy pool");
            this.largeBlueJar.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(671, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(188, 25);
            this.label21.TabIndex = 35;
            this.label21.Text = "Large Enemy Pool";
            // 
            // smallKey
            // 
            this.smallKey.AutoSize = true;
            this.smallKey.Enabled = false;
            this.smallKey.Location = new System.Drawing.Point(437, 340);
            this.smallKey.Name = "smallKey";
            this.smallKey.Size = new System.Drawing.Size(81, 29);
            this.smallKey.TabIndex = 34;
            this.smallKey.Text = "Key";
            this.toolTip1.SetToolTip(this.smallKey, "Add small keys to the small enemy pool");
            this.smallKey.UseVisualStyleBackColor = true;
            // 
            // small1up
            // 
            this.small1up.AutoSize = true;
            this.small1up.Enabled = false;
            this.small1up.Location = new System.Drawing.Point(438, 300);
            this.small1up.Name = "small1up";
            this.small1up.Size = new System.Drawing.Size(80, 29);
            this.small1up.TabIndex = 33;
            this.small1up.Text = "1up";
            this.toolTip1.SetToolTip(this.small1up, "Add 1ups to the small enemy pool");
            this.small1up.UseVisualStyleBackColor = true;
            // 
            // small500
            // 
            this.small500.AutoSize = true;
            this.small500.Enabled = false;
            this.small500.Location = new System.Drawing.Point(438, 260);
            this.small500.Name = "small500";
            this.small500.Size = new System.Drawing.Size(134, 29);
            this.small500.TabIndex = 32;
            this.small500.Text = "500 pbag";
            this.toolTip1.SetToolTip(this.small500, "Add 500 bags to the small enemy pool");
            this.small500.UseVisualStyleBackColor = true;
            // 
            // small200
            // 
            this.small200.AutoSize = true;
            this.small200.Enabled = false;
            this.small200.Location = new System.Drawing.Point(438, 220);
            this.small200.Name = "small200";
            this.small200.Size = new System.Drawing.Size(134, 29);
            this.small200.TabIndex = 31;
            this.small200.Text = "200 pbag";
            this.toolTip1.SetToolTip(this.small200, "Add 200 bags to the small enemy pool");
            this.small200.UseVisualStyleBackColor = true;
            // 
            // small100
            // 
            this.small100.AutoSize = true;
            this.small100.Enabled = false;
            this.small100.Location = new System.Drawing.Point(438, 180);
            this.small100.Name = "small100";
            this.small100.Size = new System.Drawing.Size(134, 29);
            this.small100.TabIndex = 30;
            this.small100.Text = "100 pbag";
            this.toolTip1.SetToolTip(this.small100, "Add 100 bags to the small enemy pool");
            this.small100.UseVisualStyleBackColor = true;
            // 
            // small50
            // 
            this.small50.AutoSize = true;
            this.small50.Checked = true;
            this.small50.CheckState = System.Windows.Forms.CheckState.Checked;
            this.small50.Enabled = false;
            this.small50.Location = new System.Drawing.Point(438, 140);
            this.small50.Name = "small50";
            this.small50.Size = new System.Drawing.Size(122, 29);
            this.small50.TabIndex = 29;
            this.small50.Text = "50 pbag";
            this.toolTip1.SetToolTip(this.small50, "Add 50 bags to the small enemy pool");
            this.small50.UseVisualStyleBackColor = true;
            // 
            // smallRedJar
            // 
            this.smallRedJar.AutoSize = true;
            this.smallRedJar.Enabled = false;
            this.smallRedJar.Location = new System.Drawing.Point(438, 100);
            this.smallRedJar.Name = "smallRedJar";
            this.smallRedJar.Size = new System.Drawing.Size(119, 29);
            this.smallRedJar.TabIndex = 28;
            this.smallRedJar.Text = "Red Jar";
            this.toolTip1.SetToolTip(this.smallRedJar, "Add red jars to the small enemy pool");
            this.smallRedJar.UseVisualStyleBackColor = true;
            // 
            // smallBlueJar
            // 
            this.smallBlueJar.AutoSize = true;
            this.smallBlueJar.Checked = true;
            this.smallBlueJar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.smallBlueJar.Enabled = false;
            this.smallBlueJar.Location = new System.Drawing.Point(438, 60);
            this.smallBlueJar.Name = "smallBlueJar";
            this.smallBlueJar.Size = new System.Drawing.Size(123, 29);
            this.smallBlueJar.TabIndex = 27;
            this.smallBlueJar.Text = "Blue Jar";
            this.toolTip1.SetToolTip(this.smallBlueJar, "Add blue jars to the small enemy pool");
            this.smallBlueJar.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(416, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(186, 25);
            this.label20.TabIndex = 25;
            this.label20.Text = "Small Enemy Pool";
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Location = new System.Drawing.Point(358, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(4, 375);
            this.label19.TabIndex = 24;
            // 
            // enemyDropBox
            // 
            this.enemyDropBox.AutoSize = true;
            this.enemyDropBox.Location = new System.Drawing.Point(15, 60);
            this.enemyDropBox.Name = "enemyDropBox";
            this.enemyDropBox.Size = new System.Drawing.Size(260, 29);
            this.enemyDropBox.TabIndex = 23;
            this.enemyDropBox.Text = "Manually Select Drops";
            this.toolTip1.SetToolTip(this.enemyDropBox, "When checked, you can select what items get dropped by enemies");
            this.enemyDropBox.UseVisualStyleBackColor = true;
            this.enemyDropBox.CheckedChanged += new System.EventHandler(this.enemyDropBox_CheckedChanged);
            // 
            // pbagDrop
            // 
            this.pbagDrop.AutoSize = true;
            this.pbagDrop.Location = new System.Drawing.Point(15, 20);
            this.pbagDrop.Margin = new System.Windows.Forms.Padding(6);
            this.pbagDrop.Name = "pbagDrop";
            this.pbagDrop.Size = new System.Drawing.Size(317, 29);
            this.pbagDrop.TabIndex = 22;
            this.pbagDrop.Text = "Shuffle Item Drop Frequency";
            this.toolTip1.SetToolTip(this.pbagDrop, "This option will shuffle how often enemies drop pbags and jars");
            this.pbagDrop.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.beamCmbo);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.shieldColor);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.tunicColor);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.spriteCmbo);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.disableMusicBox);
            this.tabPage3.Controls.Add(this.enemyPalette);
            this.tabPage3.Controls.Add(this.beamBox);
            this.tabPage3.Controls.Add(this.fastSpellBox);
            this.tabPage3.Controls.Add(this.jumpNormalbox);
            this.tabPage3.Controls.Add(this.disableLowHealthBeep);
            this.tabPage3.Location = new System.Drawing.Point(8, 39);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1008, 414);
            this.tabPage3.TabIndex = 6;
            this.tabPage3.Text = "Misc.";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // beamCmbo
            // 
            this.beamCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.beamCmbo.FormattingEnabled = true;
            this.beamCmbo.Items.AddRange(new object[] {
            "Default",
            "Fire",
            "Bubble",
            "Rock",
            "Axe",
            "Hammer",
            "Wizzrobe Beam",
            "Random"});
            this.beamCmbo.Location = new System.Drawing.Point(510, 322);
            this.beamCmbo.Name = "beamCmbo";
            this.beamCmbo.Size = new System.Drawing.Size(239, 33);
            this.beamCmbo.TabIndex = 31;
            this.toolTip1.SetToolTip(this.beamCmbo, "Allows you to select what the beam sprite will be");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(505, 282);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(135, 25);
            this.label26.TabIndex = 30;
            this.label26.Text = "Beam Sprite:";
            // 
            // shieldColor
            // 
            this.shieldColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shieldColor.FormattingEnabled = true;
            this.shieldColor.Items.AddRange(new object[] {
            "Default",
            "Green",
            "Dark Green",
            "Aqua",
            "Dark Blue",
            "Purple",
            "Pink",
            "Orange",
            "Red",
            "Turd",
            "Random"});
            this.shieldColor.Location = new System.Drawing.Point(510, 227);
            this.shieldColor.Name = "shieldColor";
            this.shieldColor.Size = new System.Drawing.Size(239, 33);
            this.shieldColor.TabIndex = 29;
            this.toolTip1.SetToolTip(this.shieldColor, "Changes the tunic color for shield");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(505, 187);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(194, 25);
            this.label17.TabIndex = 28;
            this.label17.Text = "Shield Tunic Color:";
            // 
            // tunicColor
            // 
            this.tunicColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tunicColor.FormattingEnabled = true;
            this.tunicColor.Items.AddRange(new object[] {
            "Default",
            "Green",
            "Dark Green",
            "Aqua",
            "Dark Blue",
            "Purple",
            "Pink",
            "Orange",
            "Red",
            "Turd",
            "Random"});
            this.tunicColor.Location = new System.Drawing.Point(510, 133);
            this.tunicColor.Name = "tunicColor";
            this.tunicColor.Size = new System.Drawing.Size(239, 33);
            this.tunicColor.TabIndex = 27;
            this.toolTip1.SetToolTip(this.tunicColor, "Changes the normal tunic color");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(505, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(202, 25);
            this.label16.TabIndex = 26;
            this.label16.Text = "Normal Tunic Color:";
            // 
            // spriteCmbo
            // 
            this.spriteCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spriteCmbo.FormattingEnabled = true;
            this.spriteCmbo.Items.AddRange(new object[] {
            "Link",
            "Zelda",
            "Iron Knuckle",
            "Error",
            "Samus",
            "Simon",
            "Stalfos",
            "Vase Lady",
            "Ruto"});
            this.spriteCmbo.Location = new System.Drawing.Point(510, 40);
            this.spriteCmbo.Name = "spriteCmbo";
            this.spriteCmbo.Size = new System.Drawing.Size(239, 33);
            this.spriteCmbo.TabIndex = 25;
            this.toolTip1.SetToolTip(this.spriteCmbo, "Changes the playable character sprite");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(505, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 25);
            this.label15.TabIndex = 24;
            this.label15.Text = "Character Sprite:";
            // 
            // disableMusicBox
            // 
            this.disableMusicBox.AutoSize = true;
            this.disableMusicBox.Location = new System.Drawing.Point(6, 44);
            this.disableMusicBox.Name = "disableMusicBox";
            this.disableMusicBox.Size = new System.Drawing.Size(179, 29);
            this.disableMusicBox.TabIndex = 23;
            this.disableMusicBox.Text = "Disable Music";
            this.toolTip1.SetToolTip(this.disableMusicBox, "Disables most in game music");
            this.disableMusicBox.UseVisualStyleBackColor = true;
            // 
            // enemyPalette
            // 
            this.enemyPalette.AutoSize = true;
            this.enemyPalette.Location = new System.Drawing.Point(6, 164);
            this.enemyPalette.Margin = new System.Windows.Forms.Padding(6);
            this.enemyPalette.Name = "enemyPalette";
            this.enemyPalette.Size = new System.Drawing.Size(257, 29);
            this.enemyPalette.TabIndex = 22;
            this.enemyPalette.Text = "Shuffle Sprite Palettes";
            this.toolTip1.SetToolTip(this.enemyPalette, "When selected, sprite colors will be shuffled");
            this.enemyPalette.UseVisualStyleBackColor = true;
            // 
            // beamBox
            // 
            this.beamBox.AutoSize = true;
            this.beamBox.Location = new System.Drawing.Point(6, 205);
            this.beamBox.Margin = new System.Windows.Forms.Padding(6);
            this.beamBox.Name = "beamBox";
            this.beamBox.Size = new System.Drawing.Size(275, 29);
            this.beamBox.TabIndex = 19;
            this.beamBox.Text = "Permanent Beam Sword";
            this.toolTip1.SetToolTip(this.beamBox, "Gives Link beam sword regardless of how much health he has");
            this.beamBox.UseVisualStyleBackColor = true;
            // 
            // fastSpellBox
            // 
            this.fastSpellBox.AutoSize = true;
            this.fastSpellBox.Location = new System.Drawing.Point(6, 123);
            this.fastSpellBox.Margin = new System.Windows.Forms.Padding(6);
            this.fastSpellBox.Name = "fastSpellBox";
            this.fastSpellBox.Size = new System.Drawing.Size(219, 29);
            this.fastSpellBox.TabIndex = 4;
            this.fastSpellBox.Text = "Fast Spell Casting";
            this.toolTip1.SetToolTip(this.fastSpellBox, "When checked, you do not have to open the pause menu before casting the selected " +
        "spell");
            this.fastSpellBox.UseVisualStyleBackColor = true;
            // 
            // jumpNormalbox
            // 
            this.jumpNormalbox.AutoSize = true;
            this.jumpNormalbox.Location = new System.Drawing.Point(6, 82);
            this.jumpNormalbox.Margin = new System.Windows.Forms.Padding(6);
            this.jumpNormalbox.Name = "jumpNormalbox";
            this.jumpNormalbox.Size = new System.Drawing.Size(204, 29);
            this.jumpNormalbox.TabIndex = 3;
            this.jumpNormalbox.Text = "Jump Always On";
            this.toolTip1.SetToolTip(this.jumpNormalbox, "The player will jump very high, as if the jump spell is always active");
            this.jumpNormalbox.UseVisualStyleBackColor = true;
            // 
            // disableLowHealthBeep
            // 
            this.disableLowHealthBeep.AutoSize = true;
            this.disableLowHealthBeep.Location = new System.Drawing.Point(6, 6);
            this.disableLowHealthBeep.Margin = new System.Windows.Forms.Padding(6);
            this.disableLowHealthBeep.Name = "disableLowHealthBeep";
            this.disableLowHealthBeep.Size = new System.Drawing.Size(285, 29);
            this.disableLowHealthBeep.TabIndex = 0;
            this.disableLowHealthBeep.Text = "Disable Low Health Beep";
            this.toolTip1.SetToolTip(this.disableLowHealthBeep, "Disables the beeping that happens when the player is low on health");
            this.disableLowHealthBeep.UseVisualStyleBackColor = true;
            // 
            // fileTextBox
            // 
            this.fileTextBox.Location = new System.Drawing.Point(24, 50);
            this.fileTextBox.Margin = new System.Windows.Forms.Padding(6);
            this.fileTextBox.Name = "fileTextBox";
            this.fileTextBox.Size = new System.Drawing.Size(312, 31);
            this.fileTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.fileTextBox, "Select a USA version of the Zelda 2 ROM");
            // 
            // seedTextBox
            // 
            this.seedTextBox.Location = new System.Drawing.Point(24, 123);
            this.seedTextBox.Margin = new System.Windows.Forms.Padding(6);
            this.seedTextBox.Name = "seedTextBox";
            this.seedTextBox.Size = new System.Drawing.Size(312, 31);
            this.seedTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.seedTextBox, "This represents the random values that will be used. A different seed results in " +
        "a different shuffled ROM.");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "ROM File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Seed";
            // 
            // seedBtn
            // 
            this.seedBtn.Location = new System.Drawing.Point(352, 119);
            this.seedBtn.Margin = new System.Windows.Forms.Padding(6);
            this.seedBtn.Name = "seedBtn";
            this.seedBtn.Size = new System.Drawing.Size(150, 44);
            this.seedBtn.TabIndex = 6;
            this.seedBtn.Text = "Create Seed";
            this.seedBtn.UseVisualStyleBackColor = true;
            this.seedBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // fileBtn
            // 
            this.fileBtn.Location = new System.Drawing.Point(352, 46);
            this.fileBtn.Margin = new System.Windows.Forms.Padding(6);
            this.fileBtn.Name = "fileBtn";
            this.fileBtn.Size = new System.Drawing.Size(150, 44);
            this.fileBtn.TabIndex = 7;
            this.fileBtn.Text = "Browse...";
            this.fileBtn.UseVisualStyleBackColor = true;
            this.fileBtn.Click += new System.EventHandler(this.fileBtn_Click);
            // 
            // generateBtn
            // 
            this.generateBtn.Location = new System.Drawing.Point(842, 50);
            this.generateBtn.Margin = new System.Windows.Forms.Padding(6);
            this.generateBtn.Name = "generateBtn";
            this.generateBtn.Size = new System.Drawing.Size(216, 40);
            this.generateBtn.TabIndex = 8;
            this.generateBtn.Text = "Generate ROM";
            this.toolTip1.SetToolTip(this.generateBtn, "Create the ROM");
            this.generateBtn.UseVisualStyleBackColor = true;
            this.generateBtn.Click += new System.EventHandler(this.generateBtn_Click);
            // 
            // flagBox
            // 
            this.flagBox.Location = new System.Drawing.Point(514, 50);
            this.flagBox.Margin = new System.Windows.Forms.Padding(6);
            this.flagBox.Name = "flagBox";
            this.flagBox.Size = new System.Drawing.Size(312, 31);
            this.flagBox.TabIndex = 9;
            this.toolTip1.SetToolTip(this.flagBox, "These flags represent the selected options. They can be copy/pasted.");
            this.flagBox.TextChanged += new System.EventHandler(this.flagBox_TextChanged);
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(842, 119);
            this.updateBtn.Margin = new System.Windows.Forms.Padding(6);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(216, 44);
            this.updateBtn.TabIndex = 10;
            this.updateBtn.Text = "Check for Updates";
            this.toolTip1.SetToolTip(this.updateBtn, "Check for updates");
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(508, 19);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 25);
            this.label9.TabIndex = 11;
            this.label9.Text = "Flags";
            // 
            // wikiBtn
            // 
            this.wikiBtn.Location = new System.Drawing.Point(676, 119);
            this.wikiBtn.Margin = new System.Windows.Forms.Padding(6);
            this.wikiBtn.Name = "wikiBtn";
            this.wikiBtn.Size = new System.Drawing.Size(154, 44);
            this.wikiBtn.TabIndex = 12;
            this.wikiBtn.Text = "Wiki";
            this.toolTip1.SetToolTip(this.wikiBtn, "Visit the website");
            this.wikiBtn.UseVisualStyleBackColor = true;
            this.wikiBtn.Click += new System.EventHandler(this.wikiBtn_Click);
            // 
            // botBtn
            // 
            this.botBtn.Location = new System.Drawing.Point(56, 646);
            this.botBtn.Margin = new System.Windows.Forms.Padding(6);
            this.botBtn.Name = "botBtn";
            this.botBtn.Size = new System.Drawing.Size(150, 44);
            this.botBtn.TabIndex = 18;
            this.botBtn.Text = "Beginner";
            this.toolTip1.SetToolTip(this.botBtn, "This preset is great for people who are looking for a casual experience.");
            this.botBtn.UseVisualStyleBackColor = true;
            this.botBtn.Click += new System.EventHandler(this.botBtn_Click);
            // 
            // megmetBtn
            // 
            this.megmetBtn.Location = new System.Drawing.Point(218, 646);
            this.megmetBtn.Margin = new System.Windows.Forms.Padding(6);
            this.megmetBtn.Name = "megmetBtn";
            this.megmetBtn.Size = new System.Drawing.Size(150, 44);
            this.megmetBtn.TabIndex = 19;
            this.megmetBtn.Text = "Swiss";
            this.toolTip1.SetToolTip(this.megmetBtn, "Flags for the swiss round of the tournament");
            this.megmetBtn.UseVisualStyleBackColor = true;
            this.megmetBtn.Click += new System.EventHandler(this.megmetBtn_Click);
            // 
            // zoraBtn
            // 
            this.zoraBtn.Location = new System.Drawing.Point(380, 646);
            this.zoraBtn.Margin = new System.Windows.Forms.Padding(6);
            this.zoraBtn.Name = "zoraBtn";
            this.zoraBtn.Size = new System.Drawing.Size(150, 44);
            this.zoraBtn.TabIndex = 20;
            this.zoraBtn.Text = "Brackets";
            this.toolTip1.SetToolTip(this.zoraBtn, "Flags for the tournament finals.");
            this.zoraBtn.UseVisualStyleBackColor = true;
            this.zoraBtn.Click += new System.EventHandler(this.zoraBtn_Click);
            // 
            // wizzBtn
            // 
            this.wizzBtn.Location = new System.Drawing.Point(542, 646);
            this.wizzBtn.Margin = new System.Windows.Forms.Padding(6);
            this.wizzBtn.Name = "wizzBtn";
            this.wizzBtn.Size = new System.Drawing.Size(150, 44);
            this.wizzBtn.TabIndex = 21;
            this.wizzBtn.Text = "Finals";
            this.toolTip1.SetToolTip(this.wizzBtn, "If it can be randomized, it is.");
            this.wizzBtn.UseVisualStyleBackColor = true;
            this.wizzBtn.Click += new System.EventHandler(this.wizzBtn_Click);
            // 
            // paraBtn
            // 
            this.paraBtn.Location = new System.Drawing.Point(704, 646);
            this.paraBtn.Margin = new System.Windows.Forms.Padding(6);
            this.paraBtn.Name = "paraBtn";
            this.paraBtn.Size = new System.Drawing.Size(150, 44);
            this.paraBtn.TabIndex = 22;
            this.paraBtn.Text = "Max Rando";
            this.toolTip1.SetToolTip(this.paraBtn, "No gems. Find the triforce as fast as possible!");
            this.paraBtn.UseVisualStyleBackColor = true;
            this.paraBtn.Click += new System.EventHandler(this.paraBtn_Click);
            // 
            // lizBtn
            // 
            this.lizBtn.Location = new System.Drawing.Point(866, 646);
            this.lizBtn.Margin = new System.Windows.Forms.Padding(6);
            this.lizBtn.Name = "lizBtn";
            this.lizBtn.Size = new System.Drawing.Size(150, 44);
            this.lizBtn.TabIndex = 23;
            this.lizBtn.Text = "Four Gems";
            this.toolTip1.SetToolTip(this.lizBtn, "You only have to complete four palaces...but which ones should you pick?");
            this.lizBtn.UseVisualStyleBackColor = true;
            this.lizBtn.Click += new System.EventHandler(this.lizBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(514, 119);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 44);
            this.button1.TabIndex = 24;
            this.button1.Text = "Batch...";
            this.toolTip1.SetToolTip(this.button1, "Visit the website");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // customBox1
            // 
            this.customBox1.Location = new System.Drawing.Point(56, 768);
            this.customBox1.Margin = new System.Windows.Forms.Padding(6);
            this.customBox1.Name = "customBox1";
            this.customBox1.Size = new System.Drawing.Size(312, 31);
            this.customBox1.TabIndex = 25;
            this.toolTip1.SetToolTip(this.customBox1, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customBox2
            // 
            this.customBox2.Location = new System.Drawing.Point(380, 768);
            this.customBox2.Margin = new System.Windows.Forms.Padding(6);
            this.customBox2.Name = "customBox2";
            this.customBox2.Size = new System.Drawing.Size(312, 31);
            this.customBox2.TabIndex = 26;
            this.toolTip1.SetToolTip(this.customBox2, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customSave1
            // 
            this.customSave1.Location = new System.Drawing.Point(56, 817);
            this.customSave1.Margin = new System.Windows.Forms.Padding(6);
            this.customSave1.Name = "customSave1";
            this.customSave1.Size = new System.Drawing.Size(150, 44);
            this.customSave1.TabIndex = 29;
            this.customSave1.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave1, "Saves the current flags to this custom slot");
            this.customSave1.UseVisualStyleBackColor = true;
            this.customSave1.Click += new System.EventHandler(this.customSave1_Click);
            // 
            // customLoad1
            // 
            this.customLoad1.Location = new System.Drawing.Point(220, 817);
            this.customLoad1.Margin = new System.Windows.Forms.Padding(6);
            this.customLoad1.Name = "customLoad1";
            this.customLoad1.Size = new System.Drawing.Size(150, 44);
            this.customLoad1.TabIndex = 30;
            this.customLoad1.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad1, "Loads the flags in this current custom slot");
            this.customLoad1.UseVisualStyleBackColor = true;
            this.customLoad1.Click += new System.EventHandler(this.customLoad1_Click);
            // 
            // customBox3
            // 
            this.customBox3.Location = new System.Drawing.Point(704, 768);
            this.customBox3.Margin = new System.Windows.Forms.Padding(6);
            this.customBox3.Name = "customBox3";
            this.customBox3.Size = new System.Drawing.Size(312, 31);
            this.customBox3.TabIndex = 31;
            this.toolTip1.SetToolTip(this.customBox3, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customSave2
            // 
            this.customSave2.Location = new System.Drawing.Point(382, 817);
            this.customSave2.Margin = new System.Windows.Forms.Padding(6);
            this.customSave2.Name = "customSave2";
            this.customSave2.Size = new System.Drawing.Size(150, 44);
            this.customSave2.TabIndex = 33;
            this.customSave2.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave2, "Saves the current flags to this custom slot");
            this.customSave2.UseVisualStyleBackColor = true;
            this.customSave2.Click += new System.EventHandler(this.customSave2_Click);
            // 
            // customLoad2
            // 
            this.customLoad2.Location = new System.Drawing.Point(544, 817);
            this.customLoad2.Margin = new System.Windows.Forms.Padding(6);
            this.customLoad2.Name = "customLoad2";
            this.customLoad2.Size = new System.Drawing.Size(150, 44);
            this.customLoad2.TabIndex = 34;
            this.customLoad2.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad2, "Loads the flags in this current custom slot");
            this.customLoad2.UseVisualStyleBackColor = true;
            this.customLoad2.Click += new System.EventHandler(this.customLoad2_Click);
            // 
            // customSave3
            // 
            this.customSave3.Location = new System.Drawing.Point(704, 817);
            this.customSave3.Margin = new System.Windows.Forms.Padding(6);
            this.customSave3.Name = "customSave3";
            this.customSave3.Size = new System.Drawing.Size(150, 44);
            this.customSave3.TabIndex = 35;
            this.customSave3.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave3, "Saves the current flags to this custom slot");
            this.customSave3.UseVisualStyleBackColor = true;
            this.customSave3.Click += new System.EventHandler(this.customSave3_Click);
            // 
            // customLoad3
            // 
            this.customLoad3.Location = new System.Drawing.Point(866, 817);
            this.customLoad3.Margin = new System.Windows.Forms.Padding(6);
            this.customLoad3.Name = "customLoad3";
            this.customLoad3.Size = new System.Drawing.Size(150, 44);
            this.customLoad3.TabIndex = 36;
            this.customLoad3.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad3, "Loads the flags in this current custom slot");
            this.customLoad3.UseVisualStyleBackColor = true;
            this.customLoad3.Click += new System.EventHandler(this.customLoad3_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(51, 736);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(180, 25);
            this.label22.TabIndex = 27;
            this.label22.Text = "Custom Flags #1:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(375, 736);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(180, 25);
            this.label23.TabIndex = 28;
            this.label23.Text = "Custom Flags #2:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(699, 736);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(180, 25);
            this.label24.TabIndex = 32;
            this.label24.Text = "Custom Flags #3:";
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Location = new System.Drawing.Point(56, 719);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(960, 2);
            this.label25.TabIndex = 26;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 876);
            this.Controls.Add(this.customLoad3);
            this.Controls.Add(this.customSave3);
            this.Controls.Add(this.customLoad2);
            this.Controls.Add(this.customSave2);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.customBox3);
            this.Controls.Add(this.customLoad1);
            this.Controls.Add(this.customSave1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.customBox2);
            this.Controls.Add(this.customBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lizBtn);
            this.Controls.Add(this.paraBtn);
            this.Controls.Add(this.wizzBtn);
            this.Controls.Add(this.zoraBtn);
            this.Controls.Add(this.megmetBtn);
            this.Controls.Add(this.botBtn);
            this.Controls.Add(this.wikiBtn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.flagBox);
            this.Controls.Add(this.generateBtn);
            this.Controls.Add(this.fileBtn);
            this.Controls.Add(this.seedBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.seedTextBox);
            this.Controls.Add(this.fileTextBox);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Zelda 2 Randomizer";
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.itemGrp.ResumeLayout(false);
            this.itemGrp.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lifeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magicBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attackBar)).EndInit();
            this.expBox.ResumeLayout(false);
            this.expBox.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox fileTextBox;
        private System.Windows.Forms.TextBox seedTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox itemGrp;
        private System.Windows.Forms.CheckBox keyBox;
        private System.Windows.Forms.CheckBox hammerBox;
        private System.Windows.Forms.CheckBox crossBox;
        private System.Windows.Forms.CheckBox fluteBox;
        private System.Windows.Forms.CheckBox bootsBox;
        private System.Windows.Forms.CheckBox raftBox;
        private System.Windows.Forms.CheckBox gloveBox;
        private System.Windows.Forms.CheckBox candleBox;
        private System.Windows.Forms.CheckBox shuffleItemBox;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox thunderBox;
        private System.Windows.Forms.CheckBox spellShuffleBox;
        private System.Windows.Forms.CheckBox shieldBox;
        private System.Windows.Forms.CheckBox jumpBox;
        private System.Windows.Forms.CheckBox lifeBox;
        private System.Windows.Forms.CheckBox fairyBox;
        private System.Windows.Forms.CheckBox fireBox;
        private System.Windows.Forms.CheckBox reflectBox;
        private System.Windows.Forms.CheckBox spellBox;
        private System.Windows.Forms.Button seedBtn;
        private System.Windows.Forms.Button fileBtn;
        private System.Windows.Forms.Button generateBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox heartCmbo;
        private System.Windows.Forms.CheckBox livesBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox techCmbo;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckBox palaceRoomBox;
        private System.Windows.Forms.CheckBox shuffleEnemyHPBox;
        private System.Windows.Forms.GroupBox expBox;
        private System.Windows.Forms.CheckBox lifeExpNeeded;
        private System.Windows.Forms.CheckBox magicExpNeeded;
        private System.Windows.Forms.CheckBox shuffleAtkExp;
        private System.Windows.Forms.CheckBox shuffleAllExp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox numGemsCbo;
        private System.Windows.Forms.CheckBox lifeRefilBox;
        private System.Windows.Forms.CheckBox swordImmuneBox;
        private System.Windows.Forms.CheckBox stealExpAmt;
        private System.Windows.Forms.CheckBox stealExpBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox disableLowHealthBeep;
        private System.Windows.Forms.CheckBox tbirdBox;
        private System.Windows.Forms.CheckBox shuffleEnemyExp;
        private System.Windows.Forms.CheckBox shuffleBossExp;
        private System.Windows.Forms.CheckBox allowPathEnemies;
        private System.Windows.Forms.CheckBox shuffleEncounters;
        private System.Windows.Forms.CheckBox shufflePalaceEnemies;
        private System.Windows.Forms.CheckBox shuffleOverworldEnemies;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox jumpNormalbox;
        private System.Windows.Forms.TextBox flagBox;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox mixEnemies;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button wikiBtn;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.CheckBox mixItemBox;
        private System.Windows.Forms.CheckBox overworldItemBox;
        private System.Windows.Forms.CheckBox palaceItemBox;
        private System.Windows.Forms.CheckBox shuffleSpellLocationsBox;
        private System.Windows.Forms.CheckBox shuffleSmallItemsBox;
        private System.Windows.Forms.CheckBox disableJarBox;
        private System.Windows.Forms.CheckBox palaceKeys;
        private System.Windows.Forms.CheckBox palacePalette;
        private System.Windows.Forms.Button lizBtn;
        private System.Windows.Forms.Button paraBtn;
        private System.Windows.Forms.Button wizzBtn;
        private System.Windows.Forms.Button zoraBtn;
        private System.Windows.Forms.Button megmetBtn;
        private System.Windows.Forms.Button botBtn;
        private System.Windows.Forms.CheckBox palaceSwapBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar attackBar;
        private System.Windows.Forms.Label lifeLabel;
        private System.Windows.Forms.Label magLabel;
        private System.Windows.Forms.Label atkLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TrackBar lifeBar;
        private System.Windows.Forms.TrackBar magicBar;
        private System.Windows.Forms.CheckBox upaBox;
        private System.Windows.Forms.CheckBox gpBox;
        private System.Windows.Forms.CheckBox fastSpellBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox hintBox;
        private System.Windows.Forms.CheckBox kasutoBox;
        private System.Windows.Forms.CheckBox combineFireBox;
        private System.Windows.Forms.CheckBox removeTbird;
        private System.Windows.Forms.CheckBox pbagItemShuffleBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox maxHeartsBox;
        private System.Windows.Forms.CheckBox beamBox;
        private System.Windows.Forms.CheckBox p7Shuffle;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox shuffleDripper;
        private System.Windows.Forms.CheckBox enemyPalette;
        private System.Windows.Forms.ComboBox hpCmbo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox disableMusicBox;
        private System.Windows.Forms.ComboBox hideKasutoBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox spriteCmbo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox tunicColor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox shieldColor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox spellItemBox;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.CheckBox largeKey;
        private System.Windows.Forms.CheckBox large1up;
        private System.Windows.Forms.CheckBox large500;
        private System.Windows.Forms.CheckBox large200;
        private System.Windows.Forms.CheckBox large100;
        private System.Windows.Forms.CheckBox large50;
        private System.Windows.Forms.CheckBox largeRedJar;
        private System.Windows.Forms.CheckBox largeBlueJar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox smallKey;
        private System.Windows.Forms.CheckBox small1up;
        private System.Windows.Forms.CheckBox small500;
        private System.Windows.Forms.CheckBox small200;
        private System.Windows.Forms.CheckBox small100;
        private System.Windows.Forms.CheckBox small50;
        private System.Windows.Forms.CheckBox smallRedJar;
        private System.Windows.Forms.CheckBox smallBlueJar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox enemyDropBox;
        private System.Windows.Forms.CheckBox pbagDrop;
        private System.Windows.Forms.TextBox customBox1;
        private System.Windows.Forms.TextBox customBox2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button customSave1;
        private System.Windows.Forms.Button customLoad1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox customBox3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button customSave2;
        private System.Windows.Forms.Button customLoad2;
        private System.Windows.Forms.Button customSave3;
        private System.Windows.Forms.Button customLoad3;
        private System.Windows.Forms.CheckBox communityBox;
        private System.Windows.Forms.ComboBox beamCmbo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox standardDrops;
        private System.Windows.Forms.CheckBox randoDrops;
        private System.Windows.Forms.CheckBox shufflePbagExp;
    }
}

