﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z2Randomizer
{
    class Palace
    {
        private int num;
        private Room root;
        private SortedDictionary<int, List<Room>> rooms;
        private List<Room> upExits;
        private List<Room> downExits;
        private List<Room> leftExits;
        private List<Room> rightExits;
        private List<Room> allRooms;
        private List<Room> onlyDownExits;
        private List<Room> onlyp7DownExits;
        private int numRooms;
        private int baseAddr;
        private int connAddr;
        private Hyrule hy;
        private readonly List<int> p4downOnly = new List<int> {0x60, 0x78 };
        private readonly int p5block = 0xc8;
        private readonly List<int> p6downOnly = new List<int> {0xa0, 0xb4, 0xc0, 0xdc, 0xd8, 0xe4, 0xac  };
        private readonly int p6weirdRoom = 0xac;
        private readonly int p71up = 0xa4;
        private readonly int p7block = 0xb4;
        private readonly int thunderbird = 0xd4;
        private readonly int darklink = 0xd8;
        private readonly List<int> gems = new List<int> { 0x34, 0x88, 0x38, 0x70, 0xa4, 0xe8, 0xd8 };
        private readonly List<int> p7downOnly = new List<int> { 0xa4, 0xb4, 0xc4, 0x2c };
        private bool needJumpOrFairy;
        private bool needFairy;
        private bool needGlove;
        private bool needDstab;

        internal List<Room> AllRooms
        {
            get
            {
                return allRooms;
            }

            set
            {
                allRooms = value;
            }
        }

        public bool NeedJumpOrFairy { get => needJumpOrFairy; set => needJumpOrFairy = value; }
        public bool NeedFairy { get => needFairy; set => needFairy = value; }
        public bool NeedGlove { get => needGlove; set => needGlove = value; }
        public bool NeedDstab { get => needDstab; set => needDstab = value; }

        public Palace(int number, int b, int c, Hyrule h)
        {
            num = number;
            root = null;
            upExits = new List<Room>();
            downExits = new List<Room>();
            leftExits = new List<Room>();
            rightExits = new List<Room>();
            onlyDownExits = new List<Room>();
            onlyp7DownExits = new List<Room>();
            rooms = new SortedDictionary<int, List<Room>>();
            allRooms = new List<Room>();
            numRooms = 0;
            baseAddr = b;
            connAddr = c;
            hy = h;
            needDstab = false;
            needFairy = false;
            needGlove = false;
            needJumpOrFairy = false;
            loadMaps();
            createTree();
        }

        private void loadMaps()
        {
            List<int> mapsToLoad = new List<int>();
            List<int> finishedMaps = new List<int>();
            mapsToLoad.Add((int)hy.ROMData.getByte(baseAddr + 0x7e) * 4);
            Room tbird = null;
            while (mapsToLoad.Count > 0)
            {
                int addr = connAddr + mapsToLoad[0];
                Byte[] connectBytes = new Byte[4];
                for (int i = 0; i < 4; i++)
                {
                    connectBytes[i] = hy.ROMData.getByte(addr + i);
                    int nextRoom = connectBytes[i] & 0xFC;
                    if (!finishedMaps.Contains(nextRoom) && nextRoom != 0 && nextRoom != 0xFC)
                    {
                        mapsToLoad.Add(connectBytes[i] & 0xFC);
                        finishedMaps.Add(connectBytes[i] & 0xFC);
                    }
                }
                Room r;
                if ((num == 6 && mapsToLoad[0] == p6weirdRoom) || num == 7 && p7downOnly.Contains(mapsToLoad[0]))
                {
                    r = new Room(mapsToLoad[0], connectBytes, addr, true);
                }
                else
                {
                    r = new Room(mapsToLoad[0], connectBytes, addr, false);
                }

                if (num == 5 && mapsToLoad[0] == p5block)
                {
                    Room r2 = new Room(mapsToLoad[0], connectBytes, addr, false);
                    List<Room> l = new List<Room>();
                    l.Add(r2);
                    l.Add(r);
                    r.LeftByte = 0xFC;
                    r2.RightByte = 0xFC;
                    rooms.Add(r.Map, l);
                    allRooms.Add(r);
                    allRooms.Add(r2);
                    mapsToLoad.Remove(mapsToLoad[0]);
                    continue;
                }

                if(num == 7 && r.Map == p7block)
                {
                    Room r2 = new Room(r.Map, connectBytes, addr, true);
                    List<Room> l = new List<Room>();
                    r.RightByte = 0xFC;
                    r.DownByte = 0xFC;
                    r2.LeftByte = 0xFC;
                    l.Add(r2);
                    l.Add(r);
                    rooms.Add(r.Map, l);
                    allRooms.Add(r);
                    allRooms.Add(r2);
                    mapsToLoad.Remove(mapsToLoad[0]);
                    continue;
                }

                if(num == 7 && r.Map == p71up)
                {
                    Room r2 = new Room(mapsToLoad[0], connectBytes, addr, true);
                    List<Room> l = new List<Room>();
                    l.Add(r2);
                    l.Add(r);
                    r.LeftByte = 0xFC;
                    r.DownByte = 0xFC;
                    r2.RightByte = 0xFC;
                    rooms.Add(r.Map, l);
                    allRooms.Add(r);
                    allRooms.Add(r2);
                    mapsToLoad.Remove(mapsToLoad[0]);
                    continue;
                }
                
                if(num == 7 && r.Map == thunderbird)
                {
                    tbird = r;
                }
                if (!rooms.ContainsKey(r.Map))
                {
                    if (root == null)
                    {
                        root = r;
                        r.IsRoot = true;
                    }
                    List<Room> l = new List<Room>();
                    l.Add(r);
                    rooms.Add(r.Map, l);
                    allRooms.Add(r);
                }
                mapsToLoad.Remove(mapsToLoad[0]);
            }
            createTree();
            foreach(Room r in allRooms)
            {
                addRoom(r);
            }

            if(num == 7 && hy.Props.removeTbird)
            {
                tbird.Left.Right = tbird.Right;
                tbird.Left.RightByte = tbird.RightByte;
                tbird.Right.Left = tbird.Left;
                tbird.Right.LeftByte = tbird.LeftByte;
                allRooms.Remove(tbird);
                leftExits.Remove(tbird);
                rightExits.Remove(tbird);
            }
        }

        public void addRoom(Room r)
        {
            if(r.Down != null)
            {
                if ((num == 4 && p4downOnly.Contains(r.Map) || num == 6 && p6downOnly.Contains(r.Map) || num == 7 && p7downOnly.Contains(r.Map)))
                {
                    onlyDownExits.Add(r);
                }
                else
                {
                    downExits.Add(r);
                }
            }

            if (r.Left != null)
            {
                leftExits.Add(r);
            }

            if (r.Right != null)
            {
                if (!(num == 1 && r.Map == 28))
                {
                    rightExits.Add(r);
                }
            }

            if (r.Up != null)
            {
                upExits.Add(r);
            }
            numRooms++;
        }

        public Boolean requiresThunderbird()
        {
            checkSpecialPaths(root, 2);
            return !rooms[darklink][0].BeforeTbird;
        }

        public Boolean hasDeadEnd()
        {
            if(onlyDownExits.Count == 0)
            {
                return false;
            }
            Room end = rooms[gems[num - 1]][0];
            foreach (Room r in onlyDownExits)
            {
                List<Room> reachable = new List<Room>();
                List<Room> roomsToCheck = new List<Room>();
                reachable.Add(r.Down);
                roomsToCheck.Add(r.Down);

                while(roomsToCheck.Count > 0)
                {
                    Room c = roomsToCheck[0];
                    if(c.Left != null && !reachable.Contains(c.Left))
                    {
                        reachable.Add(c.Left);
                        roomsToCheck.Add(c.Left);
                    }
                    if (c.Right != null && !reachable.Contains(c.Right))
                    {
                        reachable.Add(c.Right);
                        roomsToCheck.Add(c.Right);
                    }
                    if (c.Up != null && !reachable.Contains(c.Up))
                    {
                        reachable.Add(c.Up);
                        roomsToCheck.Add(c.Up);
                    }
                    if (c.Down != null && !reachable.Contains(c.Down))
                    {
                        reachable.Add(c.Down);
                        roomsToCheck.Add(c.Down);
                    }
                    roomsToCheck.Remove(c);
                }
                if(!reachable.Contains(root) && !reachable.Contains(end))
                {
                    return true;
                }
            }
            return false;
        }



        private void checkSpecialPaths(Room r, int dir)
        {
            if (!r.BeforeTbird)
            {
                if ((num == 7) && r.Map == thunderbird)
                {
                    r.BeforeTbird = true;
                    return;
                }
                
                r.BeforeTbird = true;
                if (r.Left != null)
                {
                    checkSpecialPaths(r.Left, 3);
                }

                if (r.Right != null)
                {
                    checkSpecialPaths(r.Right, 2);
                }

                if (r.Up != null)
                {
                    checkSpecialPaths(r.Up, 1);
                }

                if (r.Down != null)
                {
                    checkSpecialPaths(r.Down, 0);
                }
            }
        }

        public Boolean allReachable()
        {
            if(canEnterTbirdFromLeft())
            {
                return false;
            }
            checkPaths(root, 2);
            foreach(Room r in allRooms)
            {
                if(!r.IsPlaced)
                {
                    return false;
                }
            }
            return true;
        }
        //0 = up, 1 = down, 2 = left, 3 = right
        private void checkPaths(Room r, int dir)
        {
            if(!r.IsPlaced)
            {
                if((num == 7) && r.Map == thunderbird)
                {
                    if (dir == 3)
                    {
                        r.IsPlaced = false;
                        return;
                    }
                }
                r.IsPlaced = true;
                if (r.Left != null)
                {
                    checkPaths(r.Left, 3);
                }

                if (r.Right != null)
                {
                    checkPaths(r.Right, 2);
                }

                if (r.Up != null)
                {
                    checkPaths(r.Up, 1);
                }

                if (r.Down != null)
                {
                    checkPaths(r.Down, 0);
                }
            }
        }

        private Boolean canEnterTbirdFromLeft()
        {
            List<Room> reachable = new List<Room>();
            List<Room> roomsToCheck = new List<Room>();
            reachable.Add(root.Down);
            roomsToCheck.Add(root.Down);

            while (roomsToCheck.Count > 0)
            {
                Room c = roomsToCheck[0];
                if(c.Left != null && c.Left.Map == thunderbird)
                {
                    return true;
                }
                if (c.Left != null && !reachable.Contains(c.Left))
                {
                    reachable.Add(c.Left);
                    roomsToCheck.Add(c.Left);
                }
                if (c.Right != null && !reachable.Contains(c.Right) && c.Right.Map != thunderbird)
                {
                    reachable.Add(c.Right);
                    roomsToCheck.Add(c.Right);
                }
                if (c.Up != null && !reachable.Contains(c.Up))
                {
                    reachable.Add(c.Up);
                    roomsToCheck.Add(c.Up);
                }
                if (c.Down != null && !reachable.Contains(c.Down))
                {
                    reachable.Add(c.Down);
                    roomsToCheck.Add(c.Down);
                }
                roomsToCheck.Remove(c);
            }
            return false;
        }

        public void shuffleRooms()
        {
            //This method is so ugly and i hate it.
            for (int i = 0; i < upExits.Count; i++)
            {
                int swap = hy.R.Next(i, upExits.Count);
                Room temp = upExits[i].Up;
                Room down1 = upExits[swap].Up;
                temp.Down = upExits[swap];
                down1.Down = upExits[i];
                upExits[i].Up = down1;
                upExits[swap].Up = temp;

                int tempByte = upExits[i].UpByte;
                upExits[i].UpByte = upExits[swap].UpByte;
                upExits[swap].UpByte = tempByte;

                tempByte = temp.DownByte;
                temp.DownByte = down1.DownByte;
                down1.DownByte = tempByte;
            }
            for (int i = 0; i < onlyDownExits.Count; i++)
            {
                int swap = hy.R.Next(i, onlyDownExits.Count);

                Room temp = onlyDownExits[i].Down;
                int tempByte = onlyDownExits[i].DownByte;

                onlyDownExits[i].Down = onlyDownExits[swap].Down;
                onlyDownExits[i].DownByte = onlyDownExits[swap].DownByte;
                onlyDownExits[swap].Down = temp;
                onlyDownExits[swap].DownByte = tempByte;
            }
            
            for (int i = 0; i < downExits.Count; i++)
            {
                int swap = hy.R.Next(i, downExits.Count);
                Room temp = downExits[i].Down;
                Room down1 = downExits[swap].Down;
                temp.Up = downExits[swap];
                down1.Up = downExits[i];
                downExits[i].Down = down1;
                downExits[swap].Down = temp;

                int tempByte = downExits[i].DownByte;
                downExits[i].DownByte = downExits[swap].DownByte;
                downExits[swap].DownByte = tempByte;

                tempByte = temp.UpByte;
                temp.UpByte = down1.UpByte;
                down1.UpByte = tempByte;
            }

            for (int i = 0; i < leftExits.Count; i++)
            {
                int swap = hy.R.Next(i, leftExits.Count);
                Room temp = leftExits[i].Left;
                Room down1 = leftExits[swap].Left;
                temp.Right = leftExits[swap];
                down1.Right = leftExits[i];
                leftExits[i].Left = down1;
                leftExits[swap].Left = temp;

                int tempByte = leftExits[i].LeftByte;
                leftExits[i].LeftByte = leftExits[swap].LeftByte;
                leftExits[swap].LeftByte = tempByte;

                tempByte = temp.RightByte;
                temp.RightByte = down1.RightByte;
                down1.RightByte = tempByte;
            }

            for (int i = 0; i < rightExits.Count; i++)
            {
                int swap = hy.R.Next(i, rightExits.Count);
                Room temp = rightExits[i].Right;
                Room down1 = rightExits[swap].Right;
                temp.Left = rightExits[swap];
                down1.Left = rightExits[i];
                rightExits[i].Right = down1;
                rightExits[swap].Right = temp;

                int tempByte = rightExits[i].RightByte;
                rightExits[i].RightByte = rightExits[swap].RightByte;
                rightExits[swap].RightByte = tempByte;

                tempByte = temp.LeftByte;
                temp.LeftByte = down1.LeftByte;
                down1.LeftByte = tempByte;
            }
            if(num == 6)
            {
                foreach(Room r in onlyDownExits)
                {
                    if(r.Down.Map != 0xBC)
                    {
                        int db = r.DownByte;
                        r.DownByte = (db & 0xFC) + 1;
                    }
                    else
                    {
                        int db = r.DownByte;
                        r.DownByte = (db & 0xFC) + 2;
                    }
                }
            }
        }


        public void updateRom()
        {
            foreach (Room r in allRooms)
            {
                r.updateBytes();
                for (int i = 0; i < 4; i++)
                {
                    if (r.Connections[i] != 0xFC)
                    {
                        hy.ROMData.put(r.MemAddr + i, r.Connections[i]);
                    }
                }
            }
        }

        private void createTree()
        {
            foreach(Room r in allRooms)
            {
                if(r.Left == null && (r.LeftByte < 0xFC && r.LeftByte > 3))
                {
                    List<Room> l = rooms[r.LeftByte & 0xFC];
                    foreach (Room r2 in l)
                    {
                        if((r2.RightByte & 0xFC) == r.Map)
                        {
                            r.Left = r2;
                        }
                    }
                }

                if (r.Right == null && (r.RightByte < 0xFC && r.RightByte > 3))
                {
                    List<Room> l = rooms[r.RightByte & 0xFC];
                    foreach (Room r2 in l)
                    {
                        if ((r2.LeftByte & 0xFC) == r.Map)
                        {
                            r.Right = r2;
                        }
                    }
                }

                if (r.Up == null && (r.UpByte < 0xFC && r.UpByte > 3))
                {
                    List<Room> l = rooms[r.UpByte & 0xFC];
                    foreach (Room r2 in l)
                    {
                        if ((r2.DownByte & 0xFC) == r.Map)
                        {
                            r.Up = r2;
                        }
                    }
                }

                if (r.Down == null && (r.DownByte < 0xFC && r.DownByte > 3))
                {
                    List<Room> l = rooms[r.DownByte & 0xFC];
                    foreach (Room r2 in l)
                    {
                        if (r2.Map == (r.DownByte & 0xFC))
                        {
                            r.Down = r2;
                        }
                    }
                }
                if ((r.UpByte & 0xFC) == 0 && (root.DownByte & 0xFC) == r.Map)
                {
                    r.Up = root;
                }
            }
        }

        public void shorten()
        {
            int target = hy.R.Next(numRooms / 2, (numRooms * 3) / 4) + 1;
            int rooms = numRooms;
            int tries = 0;
            while (rooms > target && tries < 100000)
            {
                int r = hy.R.Next(rooms);
                Room remove = null;
                if(leftExits.Count < rightExits.Count)
                {
                    remove = rightExits[hy.R.Next(rightExits.Count)];
                }

                if(r < leftExits.Count)
                {
                    remove = leftExits[r];
                }

                r -= leftExits.Count;
                if (r < upExits.Count && r >= 0)
                {
                    remove = upExits[r];
                }
                r -= upExits.Count;
                if( r < rightExits.Count && r >= 0)
                {
                    remove = rightExits[r];
                }
                r -= rightExits.Count;
                if(r < downExits.Count && r >= 0)
                {
                    remove = downExits[r];
                }

                if(onlyDownExits.Contains(remove) || remove.Map == thunderbird || remove.Map == darklink)
                {
                    tries++;
                    continue;

                }
                else
                {
                    bool hasRight = remove.Right != null;
                    bool hasLeft = remove.Left != null;
                    bool hasUp = remove.Up != null;
                    bool hasDown = remove.Down != null;

                    int n = 0;
                    n = hasRight ? n+1 : n;
                    n = hasLeft ? n+1 : n;
                    n = hasUp ? n+1 : n;
                    n = hasDown ? n+1 : n;

                    //Console.WriteLine(n);

                    if(n >= 3 || n == 1)
                    {
                        tries++;
                        continue;
                    }

                    if(hasLeft && hasRight && (onlyDownExits[0].Down != remove && onlyDownExits[1].Down != remove && onlyDownExits[2].Down != remove && onlyDownExits[3].Down != remove))
                    {
                        remove.Left.Right = remove.Right;
                        remove.Right.Left = remove.Left;
                        remove.Left.RightByte = remove.RightByte;
                        remove.Right.LeftByte = remove.LeftByte;
                        rooms--;
                        //Console.WriteLine("removed 1 room");
                        leftExits.Remove(remove);
                        rightExits.Remove(remove);
                        allRooms.Remove(remove);
                        tries = 0;
                        continue;
                    }

                    if (hasUp && hasDown)
                    {
                        remove.Up.Down = remove.Down;
                        remove.Down.Up = remove.Up;
                        remove.Up.DownByte = remove.DownByte;
                        remove.Down.UpByte = remove.UpByte;
                        //Console.WriteLine("removed 1 room");
                        rooms--;
                        upExits.Remove(remove);
                        downExits.Remove(remove);
                        allRooms.Remove(remove);
                        tries = 0;
                        continue;
                    }

                    if (hasDown)
                    {
                        if(hasLeft)
                        {
                            int count = 1;
                            count = remove.Left.Up != null ? count + 1 : count;
                            count = remove.Left.Down != null ? count + 1 : count;
                            count = remove.Left.Left != null ? count + 1 : count;
                            if(count >= 3 || count == 1)
                            {
                                tries++;
                                continue;
                            }

                            if(remove.Left.Up == null || remove.Left.Up != root)
                            {
                                tries++;
                                continue;
                            }

                            remove.Left.Up.Down = remove.Down;
                            remove.Left.Up.DownByte = remove.DownByte;
                            remove.Down.Up = remove.Left.Up;
                            remove.Down.UpByte = remove.Left.UpByte;

                            downExits.Remove(remove);
                            leftExits.Remove(remove);
                            rightExits.Remove(remove.Left);
                            upExits.Remove(remove.Left);
                            allRooms.Remove(remove);
                            allRooms.Remove(remove.Left);
                            //Console.WriteLine("removed 2 room");
                            rooms = rooms - 2;
                            tries = 0;
                            continue;
                        }
                        else
                        {
                            int count = 1;
                            count = remove.Right.Up != null ? count + 1 : count;
                            count = remove.Right.Down != null ? count + 1 : count;
                            count = remove.Right.Right != null ? count + 1 : count;
                            if (count >= 3 || count == 1)
                            {
                                tries++;
                                continue;
                            }

                            if (remove.Right.Up == null || remove.Right.Up == root)
                            {
                                tries++;
                                continue;
                            }

                            remove.Right.Up.Down = remove.Down;
                            remove.Right.Up.DownByte = remove.DownByte;
                            remove.Down.Up = remove.Right.Up;
                            remove.Down.UpByte = remove.Right.UpByte;

                            downExits.Remove(remove);
                            rightExits.Remove(remove);
                            leftExits.Remove(remove.Right);
                            upExits.Remove(remove.Right);
                            allRooms.Remove(remove);
                            allRooms.Remove(remove.Right);
                            //Console.WriteLine("removed 2 room");

                            rooms = rooms - 2;
                            tries = 0;
                            continue;
                        }
                    }
                    else
                    {
                        if (hasLeft)
                        {
                            int count = 1;
                            count = remove.Left.Up != null ? count + 1 : count;
                            count = remove.Left.Down != null ? count + 1 : count;
                            count = remove.Left.Left != null ? count + 1 : count;
                            if (count >= 3 || count == 1)
                            {
                                tries++;
                                continue;
                            }

                            if (remove.Left.Down == null || onlyDownExits.Contains(remove.Left))
                            {
                                tries++;
                                continue;
                            }

                            remove.Left.Down.Up = remove.Up;
                            remove.Left.Down.UpByte = remove.UpByte;
                            remove.Up.Down = remove.Left.Down;
                            remove.Up.DownByte = remove.Left.DownByte;

                            upExits.Remove(remove);
                            leftExits.Remove(remove);
                            rightExits.Remove(remove.Left);
                            downExits.Remove(remove.Left);
                            allRooms.Remove(remove);
                            allRooms.Remove(remove.Left);
                            //Console.WriteLine("removed 2 room");

                            rooms = rooms - 2;
                            tries = 0;
                            continue;
                        }
                        else
                        {
                            int count = 1;
                            count = remove.Right.Up != null ? count + 1 : count;
                            count = remove.Right.Down != null  ? count + 1 : count;
                            count = remove.Right.Right != null ? count + 1 : count;
                            if (count >= 3 || count == 1)
                            {
                                tries++;
                                continue;
                            }

                            if (remove.Right.Down == null || onlyDownExits.Contains(remove.Right))
                            {
                                tries++;
                                continue;
                            }

                            remove.Right.Down.Up = remove.Up;
                            remove.Right.Down.UpByte = remove.UpByte;
                            remove.Up.Down = remove.Right.Down;
                            remove.Up.DownByte = remove.Right.DownByte;

                            upExits.Remove(remove);
                            rightExits.Remove(remove);
                            leftExits.Remove(remove.Right);
                            downExits.Remove(remove.Right);
                            allRooms.Remove(remove);
                            allRooms.Remove(remove.Right);
                            //Console.WriteLine("removed 2 room");

                            rooms = rooms - 2;
                            tries = 0;
                            continue;
                        }
                    }




                }
            }
            Console.WriteLine("Target: " + target + " Rooms: " + rooms);
        }

        public void shuffleSmallItems(int world, bool first)
        {
            List<int> addresses = new List<int>();
            List<int> items = new List<int>();
            int startAddr;
            if (first)
            {
                startAddr = 0x8523 - 0x8000 + (world * 0x4000) + 0x10;
            }
            else
            {
                startAddr = 0xA000 - 0x8000 + (world * 0x4000) + 0x10;
            }
            foreach (Room r in allRooms)
            {
                int i = startAddr + (r.Map / 2);
                int low = hy.ROMData.getByte(i);
                int hi = hy.ROMData.getByte(i + 1) * 256;
                int numBytes = hy.ROMData.getByte(hi + low + 16 - 0x8000 + (world * 0x4000));
                for (int j = 4; j < numBytes; j = j + 2)
                {
                    int yPos = hy.ROMData.getByte(hi + low + j + 16 - 0x8000 + (world * 0x4000)) & 0xF0;
                    yPos = yPos >> 4;
                    if (hy.ROMData.getByte(hi + low + j + 1 + 16 - 0x8000 + (world * 0x4000)) == 0x0F && yPos < 13)
                    {
                        int addr = hi + low + j + 2 + 16 - 0x8000 + (world * 0x4000);
                        int item = hy.ROMData.getByte(addr);
                        if (item == 8 || (item > 9 && item < 14) || (item > 15 && item < 19) && !addresses.Contains(addr))
                        {
                            addresses.Add(addr);
                            items.Add(item);
                        }

                        j++;
                    }
                }
            }
            for (int i = 0; i < items.Count; i++)
            {
                int swap = hy.R.Next(i, items.Count);
                int temp = items[swap];
                items[swap] = items[i];
                items[i] = temp;
            }
            for (int i = 0; i < addresses.Count; i++)
            {
                if (hy.Props.shuffleSmallItems)
                {
                    hy.ROMData.put(addresses[i], (Byte)items[i]);
                }

                if(hy.Props.extraKeys && num != 7)
                {
                    hy.ROMData.put(addresses[i], (Byte)0x08);
                }
            }
        }

        public List<int> checkBlocks(List<int> blockers)
        {
            return checkBlocksHelper(new List<int>(), blockers, root);
        }

        private List<int> checkBlocksHelper(List<int> c, List<int> blockers, Room r)
        {
            c.Add(r.Map);
            if(r.Up != null && !blockers.Contains(r.Up.Map) && !c.Contains(r.Up.Map))
            {
                checkBlocksHelper(c, blockers, r.Up);
            }
            if (r.Down != null && !blockers.Contains(r.Down.Map) && !c.Contains(r.Down.Map))
            {
                checkBlocksHelper(c, blockers, r.Down);
            }
            if (r.Left != null && !blockers.Contains(r.Left.Map) && !c.Contains(r.Left.Map))
            {
                checkBlocksHelper(c, blockers, r.Left);
            }
            if (r.Right != null && !blockers.Contains(r.Right.Map) && !c.Contains(r.Right.Map))
            {
                checkBlocksHelper(c, blockers, r.Right);
            }
            return c;
        }
    }
}
